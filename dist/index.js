"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = exports.getProduct = void 0;
const interfaces_1 = require("./lib/definitions/interfaces");
const product_1 = require("./lib/definitions/product");
const types_1 = require("./lib/definitions/types");
const sequence_1 = require("./lib/methods/sequence");
const type_1 = require("./lib/methods/type");
function getProduct(files, parameter, config = fallbackConfig) {
    var _a, _b, _c, _d, _e, _f, _g;
    const product = new product_1.Product(files, parameter, config);
    product.addMessage(1, `Version: 2.5.0`);
    product.addMessage(1, `Input folder name: ${parameter.inputFolderName}`);
    product.addMessage(1, `Input file names:\n  ${files.map((file) => file.name).join("\n  ")}`);
    for (const part of parameter.parts) {
        product.addMessage(1, `Expected product part: ${JSON.stringify(part)}.`);
    }
    product.addMessage(1, `productType: ${parameter.productType}.`);
    product.addMessage(1, `parameter.ignoreSequenceForFlat: ${parameter.ignoreSequenceForFlat}.`);
    product.moveSidecarFiles(config.regex.productionFiles, config.regex.sidecarFiles);
    if (product.getProductionFiles().length === 0) {
        // no production files
        product.addMessage(3, `Missing production files. Make sure that the file names match one of ${config.regex.productionFiles} and not one of ${config.regex.sidecarFiles}.`);
    }
    const amountOfFiles = product.getProductionFiles().length;
    // one part
    if (product.type === interfaces_1.ProductType.flat || product.type === interfaces_1.ProductType.brochure) {
        product.assignAllUnassignedFilesToBody();
        // flat product
        product.addMessage(1, "Job is recognized as flat product.");
        product.addMessage(1, `Expected amount of variants is ${product.parameter.numVariants}.`);
        if (product.hasFilesWithoutSequence()) {
            switch (amountOfFiles) {
                case 1: {
                    const file = product.files[0];
                    file.sequence = 1;
                    product.addMessage(1, "Only one file defined, set sequence to 1.");
                    break;
                }
                case 2: {
                    // all functions only set sequence if not already defined
                    // sets sequence to 1 if it matches defined regex
                    (0, sequence_1.setSequenceFromPairs)(product, config);
                    if (product.getAmountOfFilesWithoutSequence() === 2) {
                        // If sequence cannot be set by name try by pages .. p1, p2
                        (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages);
                    }
                    const filesWithoutSeq = product.getAmountOfFilesWithoutSequence();
                    if (filesWithoutSeq === 1) {
                        // sets sequence to the missing sequence
                        (0, sequence_1.setSequenceFromMissingSequences_Body)(product);
                        if (!product.parameter.ignoreSequenceForFlat)
                            product.addMessage(2, "The sequence of files was not clearly defined.");
                    }
                    else if (filesWithoutSeq === 2) {
                        product.setSequenceFromId(types_1.FILTER_FILES.isBody);
                        if (!product.parameter.ignoreSequenceForFlat) {
                            product.addMessage(3, "The sequence of files couldn't be determined from file names.");
                            return product;
                        }
                    }
                    product.sortFilesBy("sequence");
                    break;
                }
                default: {
                    if (amountOfFiles === product.parameter.numVariants) {
                        // merging order is not relevant
                        product.addMessage(1, `Received as many files as variants for flat product. Merging order can be ingored.`);
                        product.sortFilesBy("id");
                        product.setSequenceFromId();
                    }
                    else if (amountOfFiles === product.parameter.numVariants + 1) {
                        product.addMessage(2, `Received one more file than the number of variants. Assumed that front or back file must be duplicated afterwards.`);
                        (0, type_1.setSubTypeFrontOrBackFromPairs)(product, config);
                        const filesThatMatchFront = product.getProductionFiles().filter((file) => file.subType === interfaces_1.SUB_TYPE.front);
                        const filesThatMatchBack = product.getProductionFiles().filter((file) => file.subType === interfaces_1.SUB_TYPE.back);
                        if (filesThatMatchFront.length === 1) {
                            (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages, [interfaces_1.SUB_TYPE.front]);
                            const filesWithSequence = product.getProductionFiles(types_1.FILTER_FILES.hasSequence);
                            if (product.getAmountOfFilesWithoutSequence() === 1) {
                                const minSequence = Math.min(...filesWithSequence.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
                                filesThatMatchFront[0].sequence = minSequence - 1;
                                product.results.duplicatePageWithVariants = "isFirstPage";
                            }
                        }
                        else if (filesThatMatchBack.length === 1) {
                            (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages, [interfaces_1.SUB_TYPE.back]);
                            const filesWithSequence = product.getProductionFiles(types_1.FILTER_FILES.hasSequence);
                            if (product.getAmountOfFilesWithoutSequence() === 1) {
                                const maxSequence = Math.max(...filesWithSequence.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
                                filesThatMatchBack[0].sequence = maxSequence + 1;
                                product.results.duplicatePageWithVariants = "isLastPage";
                            }
                        }
                        else {
                            product.addMessage(3, `Failed to identify only one file that matches front or that matches back.`);
                        }
                    }
                    else if (amountOfFiles === product.parameter.numVariants * 2) {
                        // each variant needs front and back files
                        product.addMessage(1, `Received twice as many files as variants for flat product. Assumed that each file has matching back file.`);
                        (0, sequence_1.setSequenceForFlatWithVariants)(product, config);
                        product.sortFilesBy("sequence");
                    }
                    else if (amountOfFiles === product.parameter.numVariants * product.parameter.neededBodyPages * 2 && product.type === interfaces_1.ProductType.brochure) {
                        product.addMessage(1, `Assumed that files must be imposed in the post process.`);
                        product.results.imposeSinglePages;
                        (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages);
                        if (product.hasFilesWithoutSequence()) {
                            product.addMessage(3, `Failed to determine sequence of files.`);
                        }
                        return product;
                    }
                    else {
                        // To many files
                        product.addMessage(3, `Received too many files. Received ${amountOfFiles} files, but expected:
              - number of files === number of variants (${product.parameter.numVariants})
              - number of files === number of variants + 1 (${product.parameter.numVariants + 1})
              - number of files === 2 x number of variants (${2 * product.parameter.numVariants})`);
                        return product;
                    }
                }
            }
        }
    }
    else if (product.type === interfaces_1.ProductType.booklet) {
        // multiple pages
        // booklet
        // all files will be assigned to type body
        product.addMessage(1, "Job is recognized as multi page product.");
        if (product.hasFilesWithoutSequence()) {
            const numProductionFiles = product.getProductionFiles().length;
            if (numProductionFiles === 1) {
                product.addMessage(1, `Only one file. Set sequence of ${product.getProductionFiles()[0].loggingName} to 1.`);
                product.files[0].sequence = 1;
                product.files[0].type = types_1.FILE_TYPES.body;
            }
            else {
                product.addMessage(1, "Multiple files. Trying to determine sequence");
                // Try to set sequence from numbers in file names
                (0, sequence_1.setSequenceFromFileName_pages_v2)(product, config.regex.sequence.pages);
                (0, type_1.setTypeForMonth)(product, config);
                if (product.hasFilesWithoutSequence()) {
                    // Sequence cannot be determined by number
                    if (product.hasUnassignedFiles()) {
                        // Get cover files
                        (0, type_1.setTypeFromNameRegex)(product, config.regex.cover, types_1.FILE_TYPES.cover);
                        (0, type_1.setTypeFromPagesOnePart)(product);
                        const filesOfSubTypeMonth = product
                            .getProductionFiles()
                            .filter((file) => file.subType === interfaces_1.SUB_TYPE.monthFront || file.subType === interfaces_1.SUB_TYPE.monthBack || file.subType === interfaces_1.SUB_TYPE.month);
                        if (filesOfSubTypeMonth.length === 0 && product.getAmountOfUnassignedFiles() === 0) {
                            product.addMessage(3, `Expected files of type body, but received only cover files.`);
                            return product;
                        }
                        product.assignAllUnassignedFilesToBody();
                    }
                    if (product.hasFilesWithoutSequence()) {
                        const bodyFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody);
                        if (bodyFiles.length === 1) {
                            bodyFiles[0].sequence = 1;
                        }
                        else {
                            (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages, [], false);
                            (0, sequence_1.setSequenceFromFileName_pagesByMonth)(product, types_1.FILE_TYPES.body, config.regex.sequence.pagesByMonth, [], false);
                        }
                        const filesWithSequence = product.getProductionFiles(types_1.FILTER_FILES.hasSequence);
                        const filesWithoutSequence = product.getProductionFiles(types_1.FILTER_FILES.hasNoSequence);
                        const coverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
                        if (filesWithoutSequence.length === 1 && coverFiles.length === 0 && product.parameter.neededBodyPages === 13) {
                            // Calendar => Assuming that page without sequence is cover
                            product.addMessage(2, `Assumed that file ${filesWithoutSequence[0].loggingName} is of type cover.`);
                            filesWithoutSequence[0].type = types_1.FILE_TYPES.cover;
                            coverFiles.push(...filesWithoutSequence);
                        }
                        else if (filesWithoutSequence.length === 2 && ((_b = (_a = filesWithSequence[0]) === null || _a === void 0 ? void 0 : _a.subType) === null || _b === void 0 ? void 0 : _b.includes("month")) && product.parameter.neededBodyPages === 14) {
                            for (const file of filesWithoutSequence) {
                                if (file.type !== types_1.FILE_TYPES.cover) {
                                    file.type = types_1.FILE_TYPES.cover;
                                    product.addMessage(2, `Assumed that file ${file.loggingName} is of type cover.`);
                                    coverFiles.push(file);
                                }
                            }
                        }
                        if (coverFiles.length === filesWithoutSequence.length) {
                            if (coverFiles.length === 1 && coverFiles[0].numPages > 1) {
                                // cover file needs to be splitted before merge
                                coverFiles[0].sequence = 1;
                                product.results.splitCoverForMergeWithBody = true;
                                product.addMessage(2, `Expected only one part but received body and cover parts. It is assumed that the cover file must be divided before merging.`);
                            }
                            else {
                                // files can be sorted correctly
                                const lowestSequence = Math.min(...filesWithSequence.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
                                const highestSequence = Math.max(...filesWithSequence.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
                                if (coverFiles.length === 1) {
                                    product.addMessage(1, `Received 1 files of type cover. Trying to determine sequence.`);
                                    (0, type_1.setSubTypeFromPairs)(product, config, [], types_1.FILTER_FILES.isCover);
                                    if (coverFiles[0].subType === interfaces_1.SUB_TYPE.front)
                                        coverFiles[0].sequence = lowestSequence - 1;
                                    else if (coverFiles[0].subType === interfaces_1.SUB_TYPE.back)
                                        coverFiles[0].sequence = highestSequence + 1;
                                    const filesWithoutSequence = product.getProductionFiles(types_1.FILTER_FILES.hasNoSequence);
                                    if (filesWithoutSequence.length === 1) {
                                        filesWithoutSequence[0].sequence = lowestSequence - 1;
                                        product.addMessage(2, `The sequence was not clearly defined. It is assumed that ${filesWithoutSequence[0].loggingName} is the first file/page.`);
                                    }
                                }
                                else if (coverFiles.length === 2) {
                                    product.addMessage(1, `Received 2 files of type cover. Trying to determine sequence.`);
                                    (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.cover, config.regex.sequence.pages);
                                    (0, sequence_1.setSequenceFromPairs)(product, config, [], types_1.FILTER_FILES.isCover);
                                    (0, sequence_1.setSequenceFromMissingSequencesCover)(product);
                                    if (product.getAmountOfFilesWithoutSequence() === 0) {
                                        // make sure all cover files are sorted correctly
                                        product.sortFilesBy("sequence", false);
                                        const sortedCoverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
                                        sortedCoverFiles[0].sequence = lowestSequence - 1;
                                        sortedCoverFiles[1].sequence = highestSequence + 1;
                                    }
                                }
                                else if (coverFiles.length === 4) {
                                    product.addMessage(1, `Received 4 files of type cover. Trying to determine sequence.`);
                                    (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.cover, config.regex.sequence.pages);
                                    (0, sequence_1.setSequenceFromMissingSequencesCover)(product);
                                    (0, sequence_1.setSequenceFromPairs)(product, config, [], types_1.FILTER_FILES.isCover);
                                    if (product.getAmountOfFilesWithoutSequence() === 0) {
                                        // make sure all cover files are sorted correctly
                                        product.sortFilesBy("sequence", false);
                                        const sortedCoverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
                                        sortedCoverFiles[0].sequence = lowestSequence - 2;
                                        sortedCoverFiles[1].sequence = lowestSequence - 1;
                                        sortedCoverFiles[2].sequence = highestSequence + 1;
                                        sortedCoverFiles[3].sequence = highestSequence + 2;
                                    }
                                }
                                // Set all cover files to type body for merging
                                coverFiles.map((file) => (file.type = types_1.FILE_TYPES.body));
                            }
                        }
                    }
                }
                else {
                    // Get cover files
                    (0, type_1.setTypeFromNameRegex)(product, config.regex.cover, types_1.FILE_TYPES.cover);
                    (0, type_1.setTypeFromPagesOnePart)(product);
                    product.assignAllUnassignedFilesToBody();
                    const coverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
                    if (coverFiles.length === 1 && coverFiles[0].numPages > 2) {
                        // cover file needs to be splitted before merge
                        coverFiles[0].sequence = 1;
                        product.results.splitCoverForMergeWithBody = true;
                        product.addMessage(2, `Expected only one part but received body and cover parts. It is assumed that the cover file must be divided before merging.`);
                    }
                    else {
                        coverFiles.map((file) => (file.type = types_1.FILE_TYPES.body));
                    }
                }
            }
            if (product.hasFilesWithoutSequence()) {
                product.addMessage(3, "The sequence of files couldn't be determined from file names.");
                return product;
            }
            else {
                product.sortFilesBy("sequence");
            }
        }
    }
    else if (product.type === interfaces_1.ProductType.book || product.type === interfaces_1.ProductType.multipartbooklet) {
        // multi part product
        product.addMessage(1, "Job is recognized as multi part product.");
        switch (amountOfFiles) {
            case 1: {
                // assume cover and body is one file
                product.assignAllUnassignedFilesToBody();
                const file = product.files[0];
                file.sequence = 1;
                file.setPageTypeFromSize((_c = product.parameter.parts.find((part) => part.type === types_1.PART_TYPES.body)) === null || _c === void 0 ? void 0 : _c.width, product.parameter.neededCoverPages);
                product.setPageRangeFromFile(file.id);
                product.results.splitCoverFromSingleFile = true;
                product.addMessage(2, "It is assumed that the cover and body are contained in one file. Therefore, the file must be split in the further process.");
                break;
            }
            case 2: {
                // assume one cover and body file
                if (product.hasUnassignedFiles()) {
                    // try to assign cover from file names
                    (0, type_1.setTypeFromNameRegex)(product, config.regex.cover, types_1.FILE_TYPES.cover);
                    (0, type_1.setTypesFromSize)(product);
                    (0, type_1.setTypeFromPagesCover)(product, [...config.regex.body, ...config.regex.spineFiles]);
                    const coverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
                    if (coverFiles.length === 2) {
                        // two cover files, but we also expected body
                        product.addMessage(3, "Expected files to contain cover and body, but received two cover files.");
                        return product;
                    }
                    if (coverFiles.length === 1) {
                        // one cover file, assume other is body or spine
                        (0, type_1.setTypeFromNameRegex)(product, config.regex.body, types_1.FILE_TYPES.body);
                        if (product.type === interfaces_1.ProductType.book)
                            (0, type_1.setTypeFromNameRegex)(product, config.regex.spineFiles, types_1.FILE_TYPES.body, [], interfaces_1.SUB_TYPE.spine);
                        (0, type_1.setTypeFromPagesBody)(product, [...config.regex.cover, ...config.regex.spineFiles]);
                        const bodyFilesWithoutSpine = product.getProductionFiles(types_1.FILTER_FILES.isBody).filter((file) => file.subType !== interfaces_1.SUB_TYPE.spine);
                        const spineFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody).filter((file) => file.subType === interfaces_1.SUB_TYPE.spine);
                        if (spineFiles.length === 0) {
                            if (bodyFilesWithoutSpine.length === 0) {
                                // no body or spine found, set unassigned file to body
                                if (product.getActualPages(types_1.FILTER_FILES.isUnassigned) !== product.parameter.neededBodyPages) {
                                    const fileName = product.getProductionFiles(types_1.FILTER_FILES.isUnassigned)[0].loggingName;
                                    product.addMessage(2, `The product part types were not clearly defined. Assumed that unassigned file ${fileName} is of type body.`);
                                }
                                product.assignAllUnassignedFilesToBody();
                            }
                            const bodyFile = product.getProductionFiles(types_1.FILTER_FILES.isBody)[0];
                            bodyFile.sequence = 1;
                            coverFiles[0].sequence = 1;
                        }
                        else {
                            if (coverFiles[0].numPages !== product.parameter.allNeededPages) {
                                product.addMessage(3, `Expected product to contain body and cover, but received only cover and spine.`);
                                return product;
                            }
                            else {
                                product.addMessage(2, `Determined ${spineFiles[0].loggingName} as spine. Therefore, the cover files and the spine must be merged in the further process.`);
                                product.addMessage(2, `Assumed that file: ${coverFiles[0].loggingName} which was determined as cover by regex contains the body as well. Therefore set type to body.`);
                                coverFiles[0].type = types_1.FILE_TYPES.body;
                                coverFiles[0].sequence = 1;
                                spineFiles[0].sequence = 2;
                                product.results.spineIsLastPage = true;
                                product.results.splitCoverFromSingleFile = true;
                                const singlePageWidth = (_d = product.parameter.parts.find((part) => part.type === types_1.PART_TYPES.body)) === null || _d === void 0 ? void 0 : _d.width;
                                coverFiles[0].setPageTypeFromSize(singlePageWidth, product.parameter.neededCoverPages);
                                product.setPageRangeFromFile(coverFiles[0].id);
                            }
                        }
                    }
                    if (coverFiles.length === 0) {
                        // no cover found, trying to find body and spine by regex
                        (0, type_1.setTypeFromNameRegex)(product, config.regex.body, types_1.FILE_TYPES.body);
                        if (product.type === interfaces_1.ProductType.book) {
                            (0, type_1.setTypeFromNameRegex)(product, config.regex.spineFiles, types_1.FILE_TYPES.body, [], interfaces_1.SUB_TYPE.spine);
                        }
                        (0, type_1.setTypeFromPagesBody)(product, [...config.regex.cover, ...config.regex.spineFiles]);
                        const bodyFilesWithoutSpine = product.getProductionFiles(types_1.FILTER_FILES.isBody).filter((file) => file.subType !== interfaces_1.SUB_TYPE.spine);
                        const spineFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody).filter((file) => file.subType === interfaces_1.SUB_TYPE.spine);
                        if (spineFiles.length === 0) {
                            if (bodyFilesWithoutSpine.length === 1) {
                                if (product.hasUnassignedFiles() && product.getActualPages(types_1.FILTER_FILES.isUnassigned) !== product.parameter.neededCoverPages) {
                                    const fileName = product.getProductionFiles(types_1.FILTER_FILES.isUnassigned)[0].loggingName;
                                    product.addMessage(2, `The product part types were not clearly defined. Assumed that unassigned file ${fileName} is of type cover.`);
                                }
                                bodyFilesWithoutSpine[0].sequence = 1;
                                product.assignAllUnassignedFilesToCover();
                                const coverFile = product.getProductionFiles(types_1.FILTER_FILES.isCover)[0];
                                coverFile.sequence = 1;
                            }
                            else {
                                // nothing found
                                product.addMessage(3, `The product part types and sequences could not be determined.`);
                                return product;
                            }
                        }
                        else if (spineFiles.length === 1) {
                            product.addMessage(2, "It is assumed that the cover and body are contained in one file. Therefore, the file must be split in the further process.");
                            product.addMessage(2, `Determined ${spineFiles[0].loggingName} as spine. Therefore, the cover files and the spine must be merged in the further process.`);
                            if (bodyFilesWithoutSpine.length === 1) {
                                bodyFilesWithoutSpine[0].sequence = 1;
                                spineFiles[0].sequence = 2;
                                product.results.spineIsLastPage = true;
                                product.results.splitCoverFromSingleFile = true;
                                bodyFilesWithoutSpine[0].setPageTypeFromSize((_e = product.parameter.parts.find((part) => part.type === types_1.PART_TYPES.body)) === null || _e === void 0 ? void 0 : _e.width, product.parameter.neededCoverPages);
                                product.setPageRangeFromFile(bodyFilesWithoutSpine[0].id);
                            }
                            else {
                                const unassignedPart = product.getProductionFiles(types_1.FILTER_FILES.isUnassigned)[0];
                                if (unassignedPart.numPages !== product.parameter.allNeededPages) {
                                    product.addMessage(2, `The product part types were not clearly defined. Assumed that file ${unassignedPart.loggingName} is of type body.`);
                                }
                                unassignedPart.type = types_1.FILE_TYPES.body;
                                unassignedPart.sequence = 1;
                                spineFiles[0].sequence = 2;
                                product.results.spineIsLastPage = true;
                                product.results.splitCoverFromSingleFile = true;
                                unassignedPart.setPageTypeFromSize((_f = product.parameter.parts.find((part) => part.type === types_1.PART_TYPES.body)) === null || _f === void 0 ? void 0 : _f.width, product.parameter.neededCoverPages);
                                product.setPageRangeFromFile(unassignedPart.id);
                            }
                        }
                        else {
                            product.addMessage(3, `Expected files 1 or 0 files of type spine, but received ${spineFiles.length}.`);
                            return product;
                        }
                    }
                }
                product.sortFilesBy("sequence");
                break;
            }
            default: {
                // Try to set sequence from numbers in file names
                (0, sequence_1.setSequenceFromFileName_pages_v2)(product, config.regex.sequence.pages);
                (0, type_1.setTypeForMonth)(product, config);
                if (product.type === interfaces_1.ProductType.book)
                    (0, type_1.setTypeFromNameRegex)(product, config.regex.spineFiles, types_1.FILE_TYPES.cover, [], interfaces_1.SUB_TYPE.spine);
                if (product.hasUnassignedFiles()) {
                    // try to assign cover from file names
                    (0, type_1.setTypeFromNameRegex)(product, config.regex.cover, types_1.FILE_TYPES.cover);
                    if (product.parameter.needsAdditionalPart) {
                        (0, type_1.setTypeFromNameRegex)(product, config.regex.additionalParts, types_1.FILE_TYPES.wildcard);
                    }
                    // only assignes files if not already assign by regex pattern
                    (0, type_1.setTypesFromSize)(product);
                    (0, type_1.setTypeFromPagesCover)(product, [...config.regex.body, ...config.regex.spineFiles]);
                    // check if files where recognized as cover
                    const amountOfCoverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover).filter((file) => file.subType !== interfaces_1.SUB_TYPE.spine).length;
                    const amountOfAdditionalFiles = product.getProductionFiles(types_1.FILTER_FILES.isWildcard).length;
                    const amountSpineFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover).filter((file) => file.subType === interfaces_1.SUB_TYPE.spine).length;
                    if (product.parameter.needsAdditionalPart && amountOfAdditionalFiles === 0)
                        product.addMessage(2, `A file of type wildcard was expected, but the file could not be determined.`);
                    const amountOfUnassignedFiles = product.getAmountOfUnassignedFiles();
                    if (amountSpineFiles <= 1) {
                        if (amountOfCoverFiles > 4) {
                            product.addMessage(3, `Expected a maximum of 4 cover files, but found: ${amountOfCoverFiles}`);
                            return product;
                        }
                        else if (amountOfCoverFiles > 0) {
                            product.addMessage(1, "Files of type cover were found.");
                            const filesOfSubTypeMonth = product
                                .getProductionFiles()
                                .filter((file) => file.subType === interfaces_1.SUB_TYPE.monthFront || file.subType === interfaces_1.SUB_TYPE.monthBack || file.subType === interfaces_1.SUB_TYPE.month);
                            if (filesOfSubTypeMonth.length === 0 && amountOfUnassignedFiles === 0) {
                                // only cover files, but we also expected body
                                product.addMessage(3, "The files were expected to contain cover and body, but only cover files were found.");
                                return product;
                            }
                            else {
                                (0, type_1.setTypeFromNameRegex)(product, config.regex.body, types_1.FILE_TYPES.body);
                                // unassigned files left
                                if (product.hasUnassignedFiles() && product.getActualPages(types_1.FILTER_FILES.isUnassigned) !== product.parameter.neededBodyPages) {
                                    const fileNames = product.getProductionFiles(types_1.FILTER_FILES.isUnassigned).map((file) => file.loggingName);
                                    if (fileNames.length > 0)
                                        product.addMessage(2, `The product part types were not clearly defined. Assumed that unassigned files ${fileNames.join(", ")} are of type body.`);
                                }
                                product.assignAllUnassignedFilesToBody();
                            }
                        }
                        else {
                            product.addMessage(1, "No files of type cover were found. Trying to find body files.");
                            // try to find body by regex
                            (0, type_1.setTypeFromNameRegex)(product, config.regex.body, types_1.FILE_TYPES.body);
                            (0, type_1.setTypeFromPagesBody)(product, [...config.regex.body, ...config.regex.spineFiles]);
                            const amountOfBodyFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody).length;
                            if (amountOfBodyFiles > 0) {
                                if (product.hasUnassignedFiles() && product.getActualPages(types_1.FILTER_FILES.isUnassigned) !== product.parameter.neededCoverPages) {
                                    const fileNames = product.getProductionFiles(types_1.FILTER_FILES.isUnassigned).map((file) => file.loggingName);
                                    product.addMessage(2, `The product part types were not clearly defined. Assumed that unassigned files ${fileNames.join(", ")} are of type cover.`);
                                }
                                if (product.getAmountOfUnassignedFiles() > 4) {
                                    product.addMessage(3, `Expected a maximum of 4 cover files, but found: ${amountOfCoverFiles}`);
                                    return product;
                                }
                                // body found
                                product.assignAllUnassignedFilesToCover();
                            }
                            else {
                                // no type found
                                const numPagesOfUnassignedFiles = product.getActualPages(types_1.FILTER_FILES.isUnassigned);
                                if (amountOfFiles === product.parameter.allNeededPages || numPagesOfUnassignedFiles === product.parameter.allNeededPages) {
                                    // If we have each page as single file
                                    product.addMessage(1, `No type found, but amount of files: ${amountOfFiles} is equal all needed pages: ${product.parameter.allNeededPages}.`);
                                    product.assignAllUnassignedFilesToBody();
                                }
                            }
                        }
                    }
                    else {
                        product.addMessage(3, `Expected 0 or 1 file of type spine, but received ${amountSpineFiles}.`);
                    }
                }
                if (product.hasFilesWithoutSequence()) {
                    const coverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover).filter((file) => file.subType !== interfaces_1.SUB_TYPE.spine);
                    const bodyFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody);
                    const additionalFiles = product.getProductionFiles(types_1.FILTER_FILES.isWildcard);
                    const spineFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover).filter((file) => file.subType === interfaces_1.SUB_TYPE.spine);
                    const amountOfCoverFiles = coverFiles.length;
                    const amountOfBodyFiles = bodyFiles.length;
                    const amountOfAdditionalFiles = additionalFiles.length;
                    const amountSpineFiles = spineFiles.length;
                    if (amountSpineFiles > 1) {
                        product.addMessage(3, `Expected 0 or 1 file of type spine, but received ${amountSpineFiles}.`);
                        return product;
                    }
                    if (amountOfCoverFiles === 1) {
                        coverFiles[0].sequence = 1;
                        product.addMessage(1, `Only one file of type cover. Set sequence of ${coverFiles[0].loggingName} to 1.`);
                    }
                    else if (amountOfCoverFiles === 2) {
                        product.addMessage(1, `Two files of type cover. Trying to determine sequence.`);
                    }
                    else {
                        product.addMessage(1, `Multiple files of type cover. Trying to determine sequence.`);
                    }
                    (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.cover, config.regex.sequence.pages, [interfaces_1.SUB_TYPE.spine]);
                    (0, sequence_1.setSequenceFromMissingSequencesCover)(product);
                    if (amountOfCoverFiles === 4 || amountOfCoverFiles === 2)
                        (0, sequence_1.setSequenceFromPairs)(product, config, [interfaces_1.SUB_TYPE.spine], types_1.FILTER_FILES.isCover);
                    if (product.getAmountOfFilesWithoutSequence() === amountOfBodyFiles + amountOfAdditionalFiles + amountSpineFiles) {
                        if (amountSpineFiles === 1) {
                            product.addMessage(2, `Determined ${spineFiles[0].loggingName} as spine. Therefore, the cover files and the spine must be merged in the further process.`);
                            const highestSequenceOfCover = Math.max(...coverFiles.map((file) => (file.sequence ? file.sequence : 0)));
                            spineFiles[0].sequence = highestSequenceOfCover + 1;
                            // Set type to cover for merging afterwards
                            spineFiles[0].type = types_1.FILE_TYPES.cover;
                            product.results.spineIsLastPage = true;
                            product.addMessage(1, `Set sequence of spine file to ${highestSequenceOfCover + 1}.`);
                        }
                        product.addMessage(1, "Determined the sequence of all cover files.");
                    }
                    else {
                        product.addMessage(3, `Couldn't determine the sequence of all cover files.`);
                        return product;
                    }
                    if (amountOfBodyFiles === 1) {
                        bodyFiles[0].sequence = 1;
                        product.addMessage(1, `Only one file of type body. Set sequence of ${bodyFiles[0].loggingName} to 1.`);
                    }
                    else if (amountOfBodyFiles !== 0) {
                        product.addMessage(1, "Multiple files of type body. Trying to determine sequence.");
                        (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.body, config.regex.sequence.pages, [], false);
                        (0, sequence_1.setSequenceFromFileName_pagesByMonth)(product, types_1.FILE_TYPES.body, config.regex.sequence.pagesByMonth, [], false);
                    }
                    if (amountOfAdditionalFiles === 1) {
                        additionalFiles[0].sequence = 1;
                        product.addMessage(1, `Only one file of type wildcard. Set sequence of ${additionalFiles[0].loggingName} to 1.`);
                    }
                    else if (amountOfAdditionalFiles !== 0) {
                        product.addMessage(1, "Multiple files of type wildcard. Trying to determine sequence.");
                        (0, sequence_1.setSequenceFromFileName_pages)(product, types_1.FILE_TYPES.wildcard, config.regex.sequence.pages);
                    }
                    const filesWithoutSequence = product.getProductionFiles(types_1.FILTER_FILES.hasNoSequence);
                    if (product.type === interfaces_1.ProductType.multipartbooklet && coverFiles.length === 0 && filesWithoutSequence.length === 1 && product.parameter.allNeededPages === 13) {
                        // Calendar => Assuming that page without sequence is cover
                        product.addMessage(2, `Assumed that file ${filesWithoutSequence[0].loggingName} is of type cover.`);
                        filesWithoutSequence[0].type = types_1.FILE_TYPES.cover;
                        filesWithoutSequence[0].sequence = 1;
                    }
                    if (product.hasFilesWithoutSequence()) {
                        product.addMessage(3, `Couldn't determine the sequence of all body files.`);
                        return product;
                    }
                    else {
                        product.addMessage(1, "Determined the sequence of all body files.");
                    }
                }
                else {
                    const filesSortedBySeq = product
                        .getProductionFiles()
                        // @ts-ignore
                        .sort((a, b) => (a["sequence"] > b["sequence"] ? 1 : b["sequence"] > a["sequence"] ? -1 : 0));
                    let indexes = [];
                    if (product.parameter.neededCoverPages === 2) {
                        indexes = [0, filesSortedBySeq.length - 1];
                    }
                    else if (product.parameter.neededCoverPages === 4) {
                        indexes = [0, 1, filesSortedBySeq.length - 2, filesSortedBySeq.length - 1];
                    }
                    const fileNames = [];
                    for (let i = 0; i < filesSortedBySeq.length; i++) {
                        const file = filesSortedBySeq[i];
                        if (file.type === types_1.FILE_TYPES.body && !file.subType.match(/.*month.*/i) && indexes.includes(i)) {
                            fileNames.push(file.loggingName);
                            file.type = types_1.FILE_TYPES.cover;
                        }
                    }
                    if (fileNames.length > 0) {
                        product.addMessage(2, `Set type of ${fileNames.join(", ")} to cover.`);
                    }
                }
                product.sortFilesBy("sequence");
            }
        }
    }
    if (product.type === interfaces_1.ProductType.book) {
        // needs cover as spread
        const coverFiles = product.getProductionFiles(types_1.FILTER_FILES.isCover);
        const bodyFiles = product.getProductionFiles(types_1.FILTER_FILES.isBody);
        let singlePageWidth = (_g = product.parameter.parts.find((part) => part.type === types_1.PART_TYPES.body)) === null || _g === void 0 ? void 0 : _g.width;
        if (!singlePageWidth) {
            singlePageWidth = (0, type_1.mode)(bodyFiles[0].pages.map((page) => page.width));
        }
        if (coverFiles.length === 0 && bodyFiles.length === 1) {
            // no separate cover file => cover pages must be contained in body file
            const coverPages = bodyFiles[0].pages.map((page) => page.type === types_1.FILE_TYPES.cover);
            for (const page of bodyFiles[0].pages) {
                if (page.type === types_1.FILE_TYPES.cover && page.subType !== interfaces_1.SUB_TYPE.spine && page.width) {
                    if (coverPages.length >= 2 && singlePageWidth && !(0, type_1.isWidthDoubleOrMoreThanSinglePage)(page.width, singlePageWidth)) {
                        // single page
                        product.addMessage(2, "Assumed that cover files are delivered as single pages and must be imposed to a spread.");
                        product.results.imposeSinglePages = true;
                    }
                }
            }
            bodyFiles[0].setPageTypeFromSize(singlePageWidth, product.parameter.neededCoverPages);
            bodyFiles[0].setPageRangeFromFile();
        }
        else {
            const coverPages = coverFiles.map((cover) => cover.pages.map((page) => page.type === types_1.FILE_TYPES.cover)).flat().length;
            if (coverPages >= 2) {
                for (const file of coverFiles) {
                    if (file.subType !== interfaces_1.SUB_TYPE.spine && singlePageWidth && !file.isSpread(singlePageWidth)) {
                        product.results.imposeSinglePages = true;
                        file.setPageTypeFromSizeCoverSinglePage(product.parameter.neededCoverPages);
                        file.setPageRangeFromFile();
                    }
                }
                if (product.results.imposeSinglePages)
                    product.addMessage(2, "Assumed that cover files are delivered as single pages and must be imposed to a spread.");
            }
        }
    }
    return product;
}
exports.getProduct = getProduct;
function main(files, parameter, config = fallbackConfig) {
    const product = getProduct(files, parameter, config);
    product.updateFiles();
    product.addFinalMessages();
    return product;
}
exports.main = main;
// fallback configuration
const fallbackConfig = {
    ignoreNumbersAbove: -1,
    regex: {
        removeFromFileNameForLogging: "",
        removeFromFileName: "",
        productionFiles: "(.*.pdf)|(.*.png)|(.*.jpg)|(.*.jpeg)|(.*.tiff)|(.*.tif)|(.*.gif)|(.*.psd)|(.*.psb)|(.*.ai)",
        sidecarFiles: "jobticket",
        ignorePageCountForFiles: "(.*.psd)",
        spineFiles: [],
        cover: [
            "aussen|vorder|front|vorn|recto|titel|außen|outside|forside|couverture|copertina|einband",
            "(?<![a-zA-Z\\d])(us|vs|rs)(?![a-zA-Z\\d])",
            "verso|hinten|rück|rueck|back",
            "u1|u2|u3|u4|umschlag|cover|deckblatt",
            "u1-u4",
            "u2-u3",
        ],
        body: ["inhalt|innen|insides|body|inside|contenu", "(?<![a-zA-Z\\d])(ih|inh)(?![a-zA-Z\\d])"],
        additionalParts: [],
        sequence: {
            pages: [
                {
                    expression: "(?<=(page|pages|seite|seiten|slide)(\\.|\\_|\\-|\\s)?)\\d+",
                    needsEqualPrefix: false,
                    needsEqualPostfix: false,
                },
                {
                    expression: "\\d+(?=(\\.|\\_|\\-|\\s)?(page|pages|seite|seiten|slide))",
                    needsEqualPrefix: false,
                    needsEqualPostfix: false,
                },
                {
                    expression: "(?<=(\\.|\\_|\\-|\\s)(u|p|s|pg)(\\.|\\_|\\-|\\s)?)\\d+(?=(\\.|\\_|\\-|\\s|$).*)",
                    needsEqualPrefix: false,
                    needsEqualPostfix: false,
                },
                {
                    expression: "(?<=(\\.|\\_|\\-|\\s))\\d+(?=(\\.|\\_|\\-|\\s)?(u|p|s|pg)(\\.|\\_|\\-|\\s))",
                    needsEqualPrefix: false,
                    needsEqualPostfix: false,
                },
                {
                    expression: "\\d+",
                    needsEqualPrefix: true,
                    needsEqualPostfix: true,
                },
            ],
            pagesByMonth: [
                {
                    expression: "(?<![a-zA-Z])(jan)(?![a-zA-Z])|(januar|january|jänner|jaenner)",
                    sequence: 1,
                },
                {
                    expression: "(?<![a-zA-Z])(feb)(?![a-zA-Z])|(feburary|februar)",
                    sequence: 2,
                },
                {
                    expression: "(?<![a-zA-Z])(mar|maer|mär)(?![a-zA-Z])|(märz|maerz|marts|march)",
                    sequence: 3,
                },
                {
                    expression: "(?<![a-zA-Z])(apr)(?![a-zA-Z])|(april)",
                    sequence: 4,
                },
                {
                    expression: "(?<![a-zA-Z])(mai|may|maj)(?![a-zA-Z])",
                    sequence: 5,
                },
                {
                    expression: "(?<![a-zA-Z])(jun)(?![a-zA-Z])|(june|juni)",
                    sequence: 6,
                },
                {
                    expression: "(?<![a-zA-Z])(jul)(?![a-zA-Z])|(july|juli)",
                    sequence: 7,
                },
                {
                    expression: "(?<![a-zA-Z])(aug)(?![a-zA-Z])|(august)",
                    sequence: 8,
                },
                {
                    expression: "(?<![a-zA-Z])(sep|sept|sept)(?![a-zA-Z])|(september)",
                    sequence: 9,
                },
                {
                    expression: "(?<![a-zA-Z])(oct|okt)(?![a-zA-Z])|(october|oktober)",
                    sequence: 10,
                },
                {
                    expression: "(?<![a-zA-Z])(nov)(?![a-zA-Z])|(november)",
                    sequence: 11,
                },
                {
                    expression: "(?<![a-zA-Z])(dez|dec)(?![a-zA-Z])|(december|dezember)",
                    sequence: 12,
                },
            ],
            pairs: [
                {
                    front: "yderside",
                    back: "inderside",
                },
                {
                    front: "cara",
                    back: "reverso",
                },
                {
                    front: "prva",
                    back: "druga",
                },
                {
                    front: "prednja",
                    back: "zadnja",
                },
                {
                    front: "_ext_?",
                    back: "_int_?",
                },
                {
                    front: "buitenz",
                    back: "binnenz",
                },
                {
                    front: "vorder",
                    back: "rueck|ru__ck|innen",
                },
                {
                    front: "front",
                    back: "back",
                },
                {
                    front: "außen|aussen",
                    back: "innen",
                },
                {
                    front: "frente",
                    back: "dorse",
                },
                {
                    front: "exterieur",
                    back: "interieur",
                },
                {
                    front: "forside",
                    back: "bagside",
                },
                {
                    front: "outside",
                    back: "inside",
                },
                {
                    front: ".*_a$",
                    back: ".*_b$",
                },
                {
                    front: "(?<![a-zA-Z\\d])(vs)(?![a-zA-Z\\d])|vorder|vorn|titel",
                    back: "(?<![a-zA-Z\\d])(rs)(?![a-zA-Z\\d])|hinten|rück|rueck",
                    inside: "innen|(?<![a-zA-Z0-9])(0?2)(?![a-zA-Z0-9])",
                    outside: "außen|aussen|(?<![a-zA-Z0-9])(0?1)(?![a-zA-Z0-9])",
                },
                {
                    front: "(?<![a-zA-Z0-9])(fc|ifc)(?![a-zA-Z0-9])",
                    back: "(?<![a-zA-Z0-9])(bc|ibc)(?![a-zA-Z0-9])",
                    inside: "(?<![a-zA-Z0-9])(ifc|ibc)(?![a-zA-Z0-9])",
                    outside: "(?<![a-zA-Z0-9])(ofc|obc)(?![a-zA-Z0-9])",
                },
                {
                    front: "recto",
                    back: "verso",
                    inside: "inside|inner|(?<![a-zA-Z0-9])(0?2)(?![a-zA-Z0-9])",
                    outside: "outside|outer|(?<![a-zA-Z0-9])(0?1)(?![a-zA-Z0-9])",
                },
                {
                    front: "front|forside",
                    back: "back",
                    inside: "inside|inner|(?<![a-zA-Z0-9])(0?2)(?![a-zA-Z0-9])",
                    outside: "outside|outer|(?<![a-zA-Z0-9])(0?1)(?![a-zA-Z0-9])",
                },
                {
                    front: "esterno",
                    back: "interno",
                    inside: "",
                    outside: "",
                },
            ],
        },
    },
};
//# sourceMappingURL=index.js.map