import { IFile, IParameter } from "./lib/definitions/interfaces";
import { Product } from "./lib/definitions/product";
export declare function getProduct(files: IFile[], parameter: IParameter, config?: any): Product;
export declare function main(files: IFile[], parameter: IParameter, config?: any): Product;
//# sourceMappingURL=index.d.ts.map