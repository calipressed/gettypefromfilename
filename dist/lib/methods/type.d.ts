import { File } from "../definitions/file";
import { IPair, IRegexPagesMonth, SUB_TYPE } from "../definitions/interfaces";
import { Product } from "../definitions/product";
import { FILE_TYPES, FILTER_FILES } from "../definitions/types";
export declare function setSubTypeFromPairs(product: Product, config: any, excludeSubTypes?: SUB_TYPE[], filter?: FILTER_FILES): void;
export declare function setSubTypeFrontAndBackFromPair(product: Product, files: File[], pair: IPair, strict?: boolean): boolean;
export declare function setSubTypeFrontOrBackFromPairs(product: Product, config: any, filter?: FILTER_FILES): void;
export declare function setSubTypeFrontAndBackInsideOutsideFromPairs(product: Product, files: File[], pair: IPair, strict?: boolean): boolean;
export declare function setSubTypeForMonthsFromPairs(filesThatMatchMonth: File[], pairs: IPair[], product: Product): void;
export declare function setTypeForMonth(product: Product, config: any): void;
export declare function setTypeFromPagesCover(product: Product, excludeRegexes?: string[]): void;
export declare function setTypeFromPagesBody(product: Product, excludeRegexes?: string[]): void;
export declare function setTypeFromPagesOnePart(product: Product, excludeSubTypes?: SUB_TYPE[]): void;
export declare function setTypeFromNameRegex(product: Product, regexes: string[], fileType: FILE_TYPES, excludeSubTypes?: SUB_TYPE[], setSubType?: SUB_TYPE): void;
export declare function getFilesThatMatchRegexes(files: File[], regexes: string[]): File[];
export declare function getFilesThatMatchBackAndMonth(product: Product, regexesMonth?: IRegexPagesMonth[], regexesBack?: string[], filter?: FILTER_FILES): File[];
export declare function getFilesThatMatchFrontAndMonth(product: Product, regexesMonth?: IRegexPagesMonth[], regexesFront?: string[], filter?: FILTER_FILES): File[];
export declare function getFilesThatMatchMonth(product: Product, regexesMonth?: IRegexPagesMonth[], filter?: FILTER_FILES): File[];
export declare function setTypesFromSize(product: Product): void;
export declare function isWidthDoubleOrMoreThanSinglePage(actualWidth: number, singlePageWidth?: number): boolean;
export declare function isWidthHalfOrLessSinglePage(actualWidth: number, singlePageWidth: number): boolean;
export declare function mode(arr: any[]): any;
export declare function isEqual(x: number, y: number, tolerance?: number): boolean;
//# sourceMappingURL=type.d.ts.map