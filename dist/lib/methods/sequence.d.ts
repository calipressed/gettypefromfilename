import { File } from "../definitions/file";
import { IRegexPages, IRegexPagesMonth, SUB_TYPE } from "../definitions/interfaces";
import { Product } from "../definitions/product";
import { FILE_TYPES, FILTER_FILES } from "../definitions/types";
export declare function setSequenceFromPairs(product: Product, config: any, excludeSubTypes?: SUB_TYPE[], filter?: FILTER_FILES): void;
export declare function setSequenceForFlatWithVariants(product: Product, config: any): void;
export declare function setSequenceFromFileName(product: Product, type: FILE_TYPES, regexes: string[], sequence: number, excludeSpine?: boolean): void;
export declare function getSequenceFromFileName(product: Product, type: FILE_TYPES, regexes: string[]): void;
export declare function setSequenceFromMissingSequences_Body(product: Product): void;
export declare function getBodySequence(sequences: number[]): number | null;
export declare function setSequenceFromMissingSequencesCover(product: Product, excludeSpine?: boolean): void;
export declare function getCoverSequence(sequences: number[]): number | null;
export declare function setSequenceFromFileName_pages(product: Product, type: FILE_TYPES, regexArray: IRegexPages[], excludeSubTypes?: SUB_TYPE[], allFilesMustMatch?: boolean): void;
export declare function setSequenceFromFileName_pages_v2(product: Product, regexArray: IRegexPages[], excludeSubTypes?: SUB_TYPE[], type?: FILE_TYPES, allFilesMustMatch?: boolean): void;
export declare function setSequenceFromFileName_pagesByMonth(product: Product, type: FILE_TYPES, regexArray?: IRegexPagesMonth[], excludeSubTypes?: SUB_TYPE[], allFilesMustMatch?: boolean): boolean;
export declare function setSequenceFromFileName_regex(files: File[], regexObj: IRegexPages, ignoreNumbersAbove?: number, allFilesMustMatch?: boolean): boolean;
//# sourceMappingURL=sequence.d.ts.map