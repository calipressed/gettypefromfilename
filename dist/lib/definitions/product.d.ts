import { File } from "./file";
import { IFile, IMessage, IParameter, ProductType } from "./interfaces";
import { Part } from "./part";
import { FILE_TYPES, FILTER_FILES, PART_TYPES } from "./types";
export interface IResults {
    pageRangeBody: string;
    pageRangeCover: string;
    pageRangeSpine: string;
    severity: 1 | 2 | 3;
    splitCoverForMergeWithBody: boolean;
    splitCoverFromSingleFile: boolean;
    spineIsLastPage: boolean;
    imposeSinglePages: boolean;
    hasSidecarFiles: boolean;
    duplicatePageWithVariants: null | "isFirstPage" | "isLastPage";
    lastErrorMessage?: string;
    lastErrorCode?: number;
    lastErrorId?: string;
}
export interface IProductParameter {
    needsAdditionalPart: boolean;
    neededCoverPages: number;
    neededBodyPages: number;
    neededAdditionalPartPages: number;
    allNeededPages: number;
    additionalPartType: string;
    parts: Part[];
    numVariants: number;
    ignoreNumbersAbove: number;
    ignoreSequenceForFlat: boolean;
    mode: "common" | "uploadPortal";
}
export declare class Product {
    files: File[];
    singleFiles: File[];
    sidecarFiles: File[];
    results: IResults;
    parameter: IProductParameter;
    uniqueIDs: string[];
    type: ProductType;
    messages: IMessage[];
    constructor(files: IFile[], parameter: IParameter, config: any);
    private getProductType;
    private init;
    private createInstancesOfFiles;
    private createInstancesOfParts;
    private setIdIfNeeded;
    private checkForCoverAndAdditionalParts;
    moveSidecarFiles(regexProductionFiles: string, regexSidecarFiles?: string): void;
    getProductionFiles(filter?: FILTER_FILES): File[];
    getLastFile(fileType?: FILE_TYPES): File | undefined;
    getFirstFile(fileType?: FILE_TYPES): File | undefined;
    getAmountOfUnassignedFiles(): number;
    getAmountOfFilesWithoutSequence(fileType?: FILE_TYPES): number;
    assignAllUnassignedFilesToBody(): void;
    assignAllUnassignedFilesToCover(): void;
    getAmountOfParts(): number;
    hasFilesWithoutSequence(): boolean;
    hasUnassignedFiles(): boolean;
    getAdditionalPartType(): PART_TYPES | undefined;
    sortFilesBy(key: keyof File, log?: boolean): void;
    setSequenceFromId(filter?: FILTER_FILES): void;
    addMessage(severity: 0 | 1 | 2 | 3, message: string, setLastError?: boolean): void;
    getPart(type: PART_TYPES): Part | undefined;
    getActualPages(filter?: FILTER_FILES): number;
    getAllWidths(): number[];
    addFinalMessages(): void;
    updateFiles(): void;
    normalizeAllFiles(files: IFile[]): void;
    normalizeSequence(files: IFile[], type?: FILE_TYPES): void;
    setPageRangeFromFile(id: string): void;
    toJSON(): {
        files: File[];
        sidecarFiles: File[];
        parts: Part[];
        results: IResults;
    };
}
//# sourceMappingURL=product.d.ts.map