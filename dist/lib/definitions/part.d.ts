import { IPart } from "./interfaces";
import { PART_TYPES } from "./types";
export declare class Part implements IPart {
    type: PART_TYPES;
    pages: number;
    width?: number;
    height?: number;
    constructor(params: IPart);
}
//# sourceMappingURL=part.d.ts.map