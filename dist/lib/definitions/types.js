"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PRODUCT_TYPES = exports.PART_TYPES = exports.FILTER_FILES = exports.FILE_TYPES = void 0;
var FILE_TYPES;
(function (FILE_TYPES) {
    FILE_TYPES["cover"] = "cover";
    FILE_TYPES["body"] = "body";
    FILE_TYPES["sidecar"] = "sidecar";
    FILE_TYPES["wildcard"] = "wildcard";
    FILE_TYPES["product"] = "product";
})(FILE_TYPES = exports.FILE_TYPES || (exports.FILE_TYPES = {}));
var FILTER_FILES;
(function (FILTER_FILES) {
    FILTER_FILES["isUnassigned"] = "isUnassigned";
    FILTER_FILES["isBody"] = "isBody";
    FILTER_FILES["isNotBody"] = "isNotBody";
    FILTER_FILES["isCover"] = "isCover";
    FILTER_FILES["isNotCover"] = "isNotCover";
    FILTER_FILES["isAssigned"] = "isAssigned";
    FILTER_FILES["hasSequence"] = "hasSequence";
    FILTER_FILES["hasNoSequence"] = "hasNoSequence";
    FILTER_FILES["hasPdfProperties"] = "hasPdfProperties";
    FILTER_FILES["hasNoPdfProperties"] = "hasNoPdfProperties";
    FILTER_FILES["isWildcard"] = "isNotBodyOrCover";
})(FILTER_FILES = exports.FILTER_FILES || (exports.FILTER_FILES = {}));
var PART_TYPES;
(function (PART_TYPES) {
    PART_TYPES["cover"] = "cover";
    PART_TYPES["body"] = "body";
    PART_TYPES["jacket"] = "jacket";
    PART_TYPES["wildcard"] = "wildcard";
})(PART_TYPES = exports.PART_TYPES || (exports.PART_TYPES = {}));
var PRODUCT_TYPES;
(function (PRODUCT_TYPES) {
    PRODUCT_TYPES["FlatWork"] = "FlatWork";
    PRODUCT_TYPES["Brochure"] = "Brochure";
    PRODUCT_TYPES["Booklet"] = "Booklet";
    PRODUCT_TYPES["MultipartBooklet"] = "MultipartBooklet";
    PRODUCT_TYPES["Book"] = "Book";
    PRODUCT_TYPES["Cover"] = "Cover";
    PRODUCT_TYPES["Body"] = "Body";
})(PRODUCT_TYPES = exports.PRODUCT_TYPES || (exports.PRODUCT_TYPES = {}));
//# sourceMappingURL=types.js.map