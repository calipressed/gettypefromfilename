"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductType = exports.ProductSubType = exports.Message = exports.SUB_TYPE = void 0;
var SUB_TYPE;
(function (SUB_TYPE) {
    SUB_TYPE["none"] = "none";
    SUB_TYPE["spine"] = "spine";
    SUB_TYPE["monthBack"] = "monthBack";
    SUB_TYPE["monthFront"] = "monthFront";
    SUB_TYPE["month"] = "month";
    SUB_TYPE["cover"] = "cover";
    SUB_TYPE["front"] = "front";
    SUB_TYPE["back"] = "back";
    SUB_TYPE["coverFrontInside"] = "coverFrontInside";
    SUB_TYPE["coverFrontOutside"] = "coverFrontOutside";
    SUB_TYPE["coverBackInside"] = "coverBackInside";
    SUB_TYPE["coverBackOutside"] = "coverBackOutside";
})(SUB_TYPE = exports.SUB_TYPE || (exports.SUB_TYPE = {}));
class Message {
    constructor(severity, message) {
        this.severity = severity;
        this.message = message;
        this.severityString = "info";
        this.log();
    }
    log() {
        switch (this.severity) {
            case 0:
                this.severityString = "debug";
                console.debug(this.message);
                break;
            case 1:
                this.severityString = "info";
                console.log(this.message);
                break;
            case 2:
                this.severityString = "warn";
                console.warn(this.message);
                break;
            case 3:
                this.severityString = "error";
                console.error(this.message);
                break;
        }
    }
}
exports.Message = Message;
var ProductSubType;
(function (ProductSubType) {
    ProductSubType["flatwork"] = "flatwork";
    ProductSubType["booklet"] = "booklet";
    ProductSubType["brochure"] = "brochure";
    ProductSubType["book"] = "book";
    ProductSubType["label"] = "label";
    ProductSubType["cover"] = "cover";
    ProductSubType["multipartbooklet"] = "multipartbooklet";
})(ProductSubType = exports.ProductSubType || (exports.ProductSubType = {}));
var ProductType;
(function (ProductType) {
    ProductType["flat"] = "flat";
    ProductType["booklet"] = "booklet";
    ProductType["multipartbooklet"] = "multipartbooklet";
    ProductType["book"] = "book";
    ProductType["brochure"] = "brochure";
})(ProductType = exports.ProductType || (exports.ProductType = {}));
//# sourceMappingURL=interfaces.js.map