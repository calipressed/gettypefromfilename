"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.File = void 0;
const path_1 = __importDefault(require("path"));
const interfaces_1 = require("./interfaces");
const types_1 = require("./types");
const type_1 = require("../methods/type");
class File {
    constructor(params, config) {
        var _a, _b, _c, _d, _e, _f;
        this.id = (_a = params === null || params === void 0 ? void 0 : params.id) !== null && _a !== void 0 ? _a : "";
        this.filePath = params === null || params === void 0 ? void 0 : params.filePath;
        this.type = (_b = params === null || params === void 0 ? void 0 : params.type) === null || _b === void 0 ? void 0 : _b.toLowerCase();
        this.sequence = params === null || params === void 0 ? void 0 : params.sequence;
        this.pages = (_c = params === null || params === void 0 ? void 0 : params.pages) !== null && _c !== void 0 ? _c : [];
        this.numPages = (_d = params === null || params === void 0 ? void 0 : params.numPages) !== null && _d !== void 0 ? _d : this.pages.length;
        this.maskedOffMatches = [];
        this.oriName = (_e = params === null || params === void 0 ? void 0 : params.name) !== null && _e !== void 0 ? _e : path_1.default.basename((params === null || params === void 0 ? void 0 : params.filePath) || "");
        this.uniqueId = this.getUniqueId(this.oriName, config);
        this.subSequence = this.getSubSequence(params === null || params === void 0 ? void 0 : params.name, config);
        this.name = this.initName(this.oriName, config);
        this.loggingName = this.getLoggingName((_f = config === null || config === void 0 ? void 0 : config.regex) === null || _f === void 0 ? void 0 : _f.removeFromFileNameForLogging);
        this.extension = path_1.default.parse(this.oriName || "").ext.toLowerCase();
        this.subType = interfaces_1.SUB_TYPE.none;
    }
    getName(includeExtension = false) {
        if (includeExtension)
            return `${this.name}${this.extension}`;
        return this.name;
    }
    getLoggingName(removeRegex) {
        let name = this.oriName;
        if (removeRegex) {
            const regex = new RegExp(removeRegex, "g");
            const matches = name.matchAll(regex);
            if (matches) {
                for (const match of matches) {
                    name = name.replace(match[0], "");
                }
            }
        }
        return name;
    }
    initName(oriName, config) {
        var _a, _b;
        let name = path_1.default.parse(oriName || "").name.toLowerCase();
        if (this.uniqueId && ((_a = config === null || config === void 0 ? void 0 : config.upl) === null || _a === void 0 ? void 0 : _a.removeFromSplitFiles)) {
            const regex = new RegExp(config.upl.removeFromSplitFiles, "gi");
            const matches = name.matchAll(regex);
            if (matches) {
                for (const match of matches) {
                    this.maskedOffMatches.push({
                        match: match[0],
                        index: match.index,
                        length: match[0].length,
                    });
                    name = name.replace(match[0], "");
                }
            }
        }
        if ((_b = config === null || config === void 0 ? void 0 : config.regex) === null || _b === void 0 ? void 0 : _b.removeFromFileName) {
            const regex = new RegExp(config === null || config === void 0 ? void 0 : config.regex.removeFromFileName, "gi");
            const matches = name.matchAll(regex);
            if (matches) {
                for (const match of matches) {
                    this.maskedOffMatches.push({
                        match: match[0],
                        index: match.index,
                        length: match[0].length,
                    });
                    name = name.replace(match[0], "");
                }
            }
        }
        return name;
    }
    getUniqueId(oriName, config) {
        var _a;
        const name = path_1.default.parse(oriName || "").name.toLocaleLowerCase();
        if ((config === null || config === void 0 ? void 0 : config.mode) === "uploadPortal" && ((_a = config === null || config === void 0 ? void 0 : config.upl) === null || _a === void 0 ? void 0 : _a.uniqueIdOfSplitFiles)) {
            const regex = new RegExp(config.upl.uniqueIdOfSplitFiles, "gi");
            const match = name.match(regex);
            if (match) {
                return match[0];
            }
        }
        return undefined;
    }
    getSubSequence(oriName, config) {
        var _a, _b, _c;
        const name = path_1.default.parse(oriName || "").name.toLocaleLowerCase();
        if (this.uniqueId && (((_a = config === null || config === void 0 ? void 0 : config.regex) === null || _a === void 0 ? void 0 : _a.subSequence) || ((_b = config === null || config === void 0 ? void 0 : config.upl) === null || _b === void 0 ? void 0 : _b.subSequence))) {
            const regex = new RegExp(config.regex.subSequence || ((_c = config === null || config === void 0 ? void 0 : config.upl) === null || _c === void 0 ? void 0 : _c.subSequence), "gi");
            const match = name.match(regex);
            if (match) {
                return parseInt(match[0]);
            }
        }
        return null;
    }
    isBody() {
        if (this.type === types_1.FILE_TYPES.body) {
            return true;
        }
        return false;
    }
    isCover() {
        if (this.type === types_1.FILE_TYPES.cover) {
            return true;
        }
        return false;
    }
    isAssigned() {
        if (this.type && Object.keys(types_1.FILE_TYPES).includes(this.type)) {
            return true;
        }
        return false;
    }
    hasSequence() {
        if (typeof this.sequence !== "undefined")
            return true;
        return false;
    }
    hasPdfProperties() {
        if (!this.numPages)
            return false;
        if (!this.getWidth() || !this.getHeight())
            return false;
        return true;
    }
    getNameHonoringSequence() {
        if (typeof this.sequence !== "undefined") {
            return setNumberWithZerosInFront(this.sequence, 4) + "_" + this.oriName;
        }
        else {
            return this.oriName;
        }
    }
    setWidth(value, pageRange) {
        for (const _p in this.pages) {
            const page = this.pages[_p];
            const pageNum = parseFloat(_p) + 1;
            if (typeof pageRange === "undefined" || (pageRange === null || pageRange === void 0 ? void 0 : pageRange.includes(pageNum))) {
                page.width = value;
            }
        }
    }
    setHeight(value, pageRange) {
        for (const _p in this.pages) {
            const page = this.pages[_p];
            const pageNum = parseFloat(_p) + 1;
            if (typeof pageRange === "undefined" || (pageRange === null || pageRange === void 0 ? void 0 : pageRange.includes(pageNum))) {
                page.height = value;
            }
        }
    }
    getAllWidths() {
        const widths = [];
        for (const page of this.pages) {
            const width = page.width;
            const TOLERANCE = 1;
            const LOW = width - TOLERANCE;
            const HIGH = width + TOLERANCE;
            let widthExists = false;
            for (const width of widths) {
                if (width > LOW && width < HIGH) {
                    widthExists = true;
                }
            }
            if (!widthExists) {
                // If no width is found add it to widths
                widths.push(width);
            }
        }
        return widths;
    }
    setPageTypeFromSize(bodyWidth, coverPages = 0, isSpread = false) {
        var _a, _b, _c;
        let singlePageWidth = 0;
        if (bodyWidth) {
            singlePageWidth = Math.round(((_a = this.pages.find((page) => isEqual(page.width, bodyWidth))) === null || _a === void 0 ? void 0 : _a.width) || 0);
        }
        else {
            singlePageWidth = mode(this.pages.map((page) => page.width));
        }
        const spreadWidth = (_b = this.pages.find((page) => page.width >= singlePageWidth * 2)) === null || _b === void 0 ? void 0 : _b.width;
        const spineWidth = (_c = this.pages.find((page) => page.width <= singlePageWidth / 2)) === null || _c === void 0 ? void 0 : _c.width;
        console.log(singlePageWidth, spreadWidth, spineWidth);
        for (const page of this.pages) {
            if (isEqual(page.width, singlePageWidth)) {
                page.type = types_1.FILE_TYPES.body;
            }
            else if (spreadWidth && isEqual(page.width, spreadWidth)) {
                page.type = types_1.FILE_TYPES.cover;
            }
            else if (spineWidth && isEqual(page.width, spineWidth)) {
                (page.type = types_1.FILE_TYPES.cover), (page.subType = interfaces_1.SUB_TYPE.spine);
            }
        }
        const coverPagesArr = this.pages.filter((page) => page.type === types_1.FILE_TYPES.cover && page.subType !== interfaces_1.SUB_TYPE.spine);
        const bodyPagesArr = this.pages.filter((page) => page.type === types_1.FILE_TYPES.body);
        if ((coverPages === 2 || (coverPages === 1 && isSpread)) && coverPagesArr.length === 0) {
            bodyPagesArr.at(0).type = types_1.FILE_TYPES.cover;
            bodyPagesArr.at(-1).type = types_1.FILE_TYPES.cover;
            bodyPagesArr.at(0).subType = interfaces_1.SUB_TYPE.coverFrontOutside;
            bodyPagesArr.at(-1).subType = interfaces_1.SUB_TYPE.coverBackOutside;
        }
        else if (coverPages === 4 || (coverPages === 2 && isSpread)) {
            if (coverPagesArr.length === 0 && bodyPagesArr.length >= 4) {
                bodyPagesArr.at(1).type = types_1.FILE_TYPES.cover;
                bodyPagesArr.at(-2).type = types_1.FILE_TYPES.cover;
                bodyPagesArr.at(1).subType = interfaces_1.SUB_TYPE.coverFrontInside;
                bodyPagesArr.at(-2).subType = interfaces_1.SUB_TYPE.coverBackInside;
            }
            if (coverPagesArr.length <= 1 && bodyPagesArr.length >= 2) {
                bodyPagesArr.at(0).type = types_1.FILE_TYPES.cover;
                bodyPagesArr.at(-1).type = types_1.FILE_TYPES.cover;
                bodyPagesArr.at(0).subType = interfaces_1.SUB_TYPE.coverFrontOutside;
                bodyPagesArr.at(-1).subType = interfaces_1.SUB_TYPE.coverBackOutside;
            }
        }
        console.log(this.pages);
        function mode(arr) {
            if (arr.length === 2) {
                return Math.min(...arr);
            }
            return arr.sort((a, b) => arr.filter((v) => v === a).length - arr.filter((v) => v === b).length).pop();
        }
        function isEqual(x, y, tolerance = 3) {
            return x - tolerance < y && y < x + tolerance;
        }
    }
    setPageTypeFromSizeCoverSinglePage(coverPages = 0) {
        // cover delivered as single page
        const allWidths = this.getAllWidths();
        if (allWidths.length > 1) {
            // contains spine?
            const singlePageWidth = Math.max(...allWidths);
            for (const page of this.pages) {
                if (page.width < singlePageWidth / 2) {
                    // only check if page is less than half of single page due to different single page sizes
                    page.type = types_1.FILE_TYPES.cover;
                    page.subType = interfaces_1.SUB_TYPE.spine;
                }
                else {
                    page.type = types_1.FILE_TYPES.cover;
                }
            }
        }
        const coverPagesArr = this.pages.filter((page) => page.type === types_1.FILE_TYPES.cover && page.subType !== interfaces_1.SUB_TYPE.spine);
        const isSpread = coverPagesArr.length >= coverPages * 2;
        if (coverPages === 2 || (coverPages === 1 && isSpread)) {
            coverPagesArr.at(0).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(-1).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(0).subType = interfaces_1.SUB_TYPE.coverFrontOutside;
            coverPagesArr.at(-1).subType = interfaces_1.SUB_TYPE.coverBackOutside;
        }
        else if (coverPages === 4 || (coverPages === 2 && isSpread)) {
            coverPagesArr.at(1).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(-2).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(1).subType = interfaces_1.SUB_TYPE.coverFrontInside;
            coverPagesArr.at(-2).subType = interfaces_1.SUB_TYPE.coverBackInside;
            coverPagesArr.at(0).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(-1).type = types_1.FILE_TYPES.cover;
            coverPagesArr.at(0).subType = interfaces_1.SUB_TYPE.coverFrontOutside;
            coverPagesArr.at(-1).subType = interfaces_1.SUB_TYPE.coverBackOutside;
        }
    }
    getWidth(_page = 1) {
        var _a;
        const width = (_a = this.pages.find((page) => page.page === _page)) === null || _a === void 0 ? void 0 : _a.width;
        if (width)
            return width;
        return 0;
    }
    getHeight(_page = 1) {
        var _a;
        const height = (_a = this.pages.find((page) => page.page === _page)) === null || _a === void 0 ? void 0 : _a.height;
        if (height)
            return height;
        return 0;
    }
    isPageSizeEqual(tolerance = 1) {
        let isEqual = true;
        const width = this.getWidth();
        const height = this.getHeight();
        for (const page of this.pages) {
            if (width - tolerance <= page.width && page.width >= width + tolerance) {
                isEqual = false;
            }
            if (height - tolerance <= page.height && page.height >= height + tolerance) {
                isEqual = false;
            }
        }
        return isEqual;
    }
    addPage(page) {
        this.pages.push(page);
    }
    isSpread(singlePageWidth) {
        if (!singlePageWidth || !this.getWidth())
            return true;
        if ((0, type_1.isWidthDoubleOrMoreThanSinglePage)(this.getWidth(), singlePageWidth)) {
            return true;
        }
        return false;
    }
    setPageRangeFromFile() {
        this.pageRangeSpine = this.pages
            .filter((page) => page.subType === interfaces_1.SUB_TYPE.spine)
            .map((page) => page.page)
            .join(",");
        this.pageRangeCover = this.pages
            .filter((page) => page.type === types_1.FILE_TYPES.cover && page.subType !== interfaces_1.SUB_TYPE.spine)
            .map((page) => page.page)
            .join(",");
        this.pageRangeBody = this.pages
            .filter((page) => page.type === types_1.FILE_TYPES.body)
            .map((page) => page.page)
            .join(",");
    }
}
exports.File = File;
function setNumberWithZerosInFront(num, size) {
    const s = "000000000" + num;
    return s.substring(s.length - size);
}
//# sourceMappingURL=file.js.map