"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Part = void 0;
class Part {
    constructor(params) {
        this.type = params.type.toLowerCase();
        this.pages = params.pages;
        this.width = params.width;
        this.height = params.height;
    }
}
exports.Part = Part;
//# sourceMappingURL=part.js.map