"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const file_1 = require("./file");
const interfaces_1 = require("./interfaces");
const part_1 = require("./part");
const types_1 = require("./types");
class Product {
    constructor(files, parameter, config) {
        var _a, _b, _c;
        this.results = {
            severity: 1,
            splitCoverForMergeWithBody: false,
            splitCoverFromSingleFile: false,
            spineIsLastPage: false,
            hasSidecarFiles: false,
            duplicatePageWithVariants: null,
            imposeSinglePages: false,
            pageRangeBody: "",
            pageRangeCover: "",
            pageRangeSpine: "",
            lastErrorMessage: undefined,
            lastErrorCode: undefined,
            lastErrorId: undefined,
        };
        this.parameter = {
            needsAdditionalPart: false,
            neededCoverPages: 0,
            neededBodyPages: 0,
            neededAdditionalPartPages: 0,
            allNeededPages: 0,
            additionalPartType: "",
            parts: [],
            numVariants: parameter.numVariants || 1,
            ignoreNumbersAbove: (_a = config.ignoreNumbersAbove) !== null && _a !== void 0 ? _a : -1,
            ignoreSequenceForFlat: (_b = parameter.ignoreSequenceForFlat) !== null && _b !== void 0 ? _b : false,
            mode: (_c = config.mode) !== null && _c !== void 0 ? _c : "common",
        };
        this.messages = [];
        this.files = [];
        this.singleFiles = [];
        this.uniqueIDs = [];
        this.sidecarFiles = [];
        this.init(files, parameter.parts, config);
        this.type = this.getProductType(parameter.productType);
    }
    getProductType(type) {
        if (this.parameter.parts.length === 1) {
            if (type) {
                switch (type.toLowerCase()) {
                    case interfaces_1.ProductSubType.booklet:
                        return interfaces_1.ProductType.booklet;
                    case interfaces_1.ProductSubType.flatwork:
                    case interfaces_1.ProductSubType.label:
                        return interfaces_1.ProductType.flat;
                    case interfaces_1.ProductSubType.cover:
                    case interfaces_1.ProductSubType.brochure:
                        return interfaces_1.ProductType.brochure;
                }
            }
            if (this.parameter.numVariants === 1 && this.parameter.allNeededPages > 2) {
                this.addMessage(1, `Assumed that product type is booklet.`);
                return interfaces_1.ProductType.booklet;
            }
            else {
                this.addMessage(1, `Assumed that product type is flatwork.`);
                return interfaces_1.ProductType.flat;
            }
        }
        else {
            if (type) {
                switch (type.toLowerCase()) {
                    case interfaces_1.ProductSubType.booklet:
                    case interfaces_1.ProductSubType.multipartbooklet:
                        return interfaces_1.ProductType.multipartbooklet;
                    case interfaces_1.ProductSubType.book:
                        return interfaces_1.ProductType.book;
                }
            }
            this.addMessage(1, `Assumed that product type is book.`);
            return interfaces_1.ProductType.book;
        }
    }
    init(files, parts, config) {
        this.createInstancesOfFiles(files, config);
        this.createInstancesOfParts(parts);
        this.setIdIfNeeded();
        this.checkForCoverAndAdditionalParts();
    }
    createInstancesOfFiles(files, config) {
        if (this.parameter.mode === "uploadPortal") {
            const filesWithUniqueId = [];
            const filesWithoutUniqueId = [];
            for (const file of files) {
                const newFile = new file_1.File(file, config);
                if (newFile.uniqueId) {
                    filesWithUniqueId.push(newFile);
                }
                else {
                    filesWithoutUniqueId.push(newFile);
                }
            }
            this.uniqueIDs.push(...new Set(filesWithUniqueId.filter((file) => typeof file.uniqueId !== "undefined").map((file) => file.uniqueId)));
            for (const id of this.uniqueIDs) {
                const filesWithSameId = filesWithUniqueId.filter((file) => file.uniqueId === id);
                const originalFile = filesWithSameId[0];
                if (originalFile) {
                    const pages = [];
                    let numPages = 0;
                    for (const file of filesWithSameId) {
                        pages.push(...file.pages);
                        numPages += file.numPages;
                    }
                    this.files.push(new file_1.File(Object.assign(Object.assign({}, originalFile), { name: originalFile.oriName, pages: pages, numPages: numPages }), config));
                }
            }
            for (const file of filesWithoutUniqueId) {
                this.files.push(file);
            }
            this.singleFiles.push(...filesWithUniqueId, ...filesWithoutUniqueId);
        }
        else {
            for (const _f in files) {
                const file = files[_f];
                this.files.push(new file_1.File(file, config));
            }
        }
    }
    createInstancesOfParts(parts) {
        for (const part of parts) {
            this.parameter.parts.push(new part_1.Part(part));
        }
    }
    setIdIfNeeded() {
        for (const _f in this.files) {
            const part = this.files[_f];
            if (!part.id) {
                part.id = `${+_f + 1}`;
            }
        }
        if (this.parameter.mode === "uploadPortal") {
            for (const _f in this.singleFiles) {
                const part = this.singleFiles[_f];
                if (!part.id) {
                    part.id = `${+_f + 1}`;
                }
            }
        }
    }
    checkForCoverAndAdditionalParts() {
        for (const part of this.parameter.parts) {
            const partType = part.type.toLowerCase();
            if (partType === types_1.PART_TYPES.cover) {
                this.parameter.neededCoverPages = part.pages;
            }
            else if (partType === types_1.PART_TYPES.body) {
                this.parameter.neededBodyPages = part.pages;
            }
            else {
                this.parameter.needsAdditionalPart = true;
                this.parameter.additionalPartType = part.type;
                this.parameter.neededAdditionalPartPages = part.pages;
            }
        }
        this.parameter.allNeededPages = this.parameter.neededBodyPages + this.parameter.neededCoverPages + this.parameter.neededAdditionalPartPages;
    }
    moveSidecarFiles(regexProductionFiles, regexSidecarFiles) {
        var _a, _b, _c, _d;
        const removeFileIndexes = [];
        for (let _f = 0; _f < this.files.length; _f++) {
            const file = this.files[_f];
            const matchProd = (_a = file.getName(true)) === null || _a === void 0 ? void 0 : _a.match(new RegExp(regexProductionFiles));
            const matchSidecar = regexSidecarFiles ? (_b = file.getName(true)) === null || _b === void 0 ? void 0 : _b.match(new RegExp(regexSidecarFiles)) : undefined;
            if (!matchProd) {
                if (this.parameter.mode === "uploadPortal") {
                    const singleFiles = this.singleFiles.filter((_file) => _file.getName() === file.getName());
                    for (const singleFile of singleFiles) {
                        singleFile.type = types_1.FILE_TYPES.sidecar;
                        singleFile.sequence = (_c = singleFile.subSequence) !== null && _c !== void 0 ? _c : 0;
                    }
                    if (singleFiles.length > 0) {
                        this.results.hasSidecarFiles = true;
                        removeFileIndexes.push(_f);
                    }
                }
                else {
                    this.results.hasSidecarFiles = true;
                    this.sidecarFiles.push(file);
                    removeFileIndexes.push(_f);
                }
            }
            else if (matchSidecar) {
                if (this.parameter.mode === "uploadPortal") {
                    const singleFiles = this.singleFiles.filter((_file) => _file.getName() === file.getName());
                    for (const singleFile of singleFiles) {
                        singleFile.type = types_1.FILE_TYPES.sidecar;
                        singleFile.sequence = (_d = singleFile.subSequence) !== null && _d !== void 0 ? _d : 0;
                    }
                    if (singleFiles.length > 0) {
                        this.results.hasSidecarFiles = true;
                        removeFileIndexes.push(_f);
                    }
                }
                else {
                    this.results.hasSidecarFiles = true;
                    this.sidecarFiles.push(file);
                    removeFileIndexes.push(_f);
                }
            }
        }
        for (let i = removeFileIndexes.length - 1; i >= 0; i--) {
            this.files.splice(removeFileIndexes[i], 1);
        }
    }
    getProductionFiles(filter) {
        const files = this.files;
        if (filter) {
            switch (filter) {
                case types_1.FILTER_FILES.isUnassigned:
                    return files.filter((file) => !file.isAssigned());
                case types_1.FILTER_FILES.isAssigned:
                    return files.filter((file) => file.isAssigned());
                case types_1.FILTER_FILES.isBody:
                    return files.filter((file) => file.isBody());
                case types_1.FILTER_FILES.isNotBody:
                    return files.filter((file) => !file.isBody());
                case types_1.FILTER_FILES.isCover:
                    return files.filter((file) => file.isCover());
                case types_1.FILTER_FILES.isNotCover:
                    return files.filter((file) => !file.isCover());
                case types_1.FILTER_FILES.hasSequence:
                    return files.filter((file) => file.hasSequence());
                case types_1.FILTER_FILES.hasNoSequence:
                    return files.filter((file) => !file.hasSequence());
                case types_1.FILTER_FILES.hasPdfProperties:
                    return files.filter((file) => file.hasPdfProperties());
                case types_1.FILTER_FILES.hasNoPdfProperties:
                    return files.filter((file) => !file.hasPdfProperties());
                case types_1.FILTER_FILES.isWildcard:
                    return files.filter((file) => !file.isBody() && !file.isCover() && file.isAssigned());
            }
        }
        return files;
    }
    getLastFile(fileType) {
        const files = fileType ? this.getProductionFiles(types_1.FILTER_FILES.hasSequence).filter((file) => file.type === fileType) : this.getProductionFiles(types_1.FILTER_FILES.hasSequence);
        const highestSequence = Math.max(...files.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
        return files.find((file) => file.sequence === highestSequence);
    }
    getFirstFile(fileType) {
        const files = fileType ? this.getProductionFiles(types_1.FILTER_FILES.hasSequence).filter((file) => file.type === fileType) : this.getProductionFiles(types_1.FILTER_FILES.hasSequence);
        const lowestSequence = Math.min(...files.map((file) => { var _a; return (_a = file.sequence) !== null && _a !== void 0 ? _a : 0; }));
        return files.find((file) => file.sequence === lowestSequence);
    }
    getAmountOfUnassignedFiles() {
        return this.getProductionFiles(types_1.FILTER_FILES.isUnassigned).length;
    }
    getAmountOfFilesWithoutSequence(fileType) {
        const files = this.getProductionFiles(types_1.FILTER_FILES.hasNoSequence);
        if (fileType)
            return files.filter((file) => file.type === fileType).length;
        return files.length;
    }
    assignAllUnassignedFilesToBody() {
        const files = this.getProductionFiles(types_1.FILTER_FILES.isUnassigned);
        for (const file of files) {
            file.type = types_1.FILE_TYPES.body;
        }
    }
    assignAllUnassignedFilesToCover() {
        const files = this.getProductionFiles(types_1.FILTER_FILES.isUnassigned);
        for (const file of files) {
            file.type = types_1.FILE_TYPES.cover;
        }
    }
    getAmountOfParts() {
        return this.parameter.parts.length;
    }
    hasFilesWithoutSequence() {
        if (this.getAmountOfFilesWithoutSequence() > 0)
            return true;
        return false;
    }
    hasUnassignedFiles() {
        if (this.getAmountOfUnassignedFiles() > 0)
            return true;
        return false;
    }
    getAdditionalPartType() {
        var _a;
        return (_a = this.parameter.parts.find((part) => part.type !== types_1.PART_TYPES.body && part.type !== types_1.PART_TYPES.cover)) === null || _a === void 0 ? void 0 : _a.type;
    }
    sortFilesBy(key, log = true) {
        if (log)
            this.addMessage(1, `Files are sorted by ${key}.`);
        const coverFiles = this.getProductionFiles(types_1.FILTER_FILES.isCover);
        const bodyFiles = this.getProductionFiles(types_1.FILTER_FILES.isBody);
        const otherFIles = this.getProductionFiles(types_1.FILTER_FILES.isWildcard);
        const unassignedFiles = this.getProductionFiles(types_1.FILTER_FILES.isUnassigned);
        // @ts-ignore
        const filteredCoverFiles = coverFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
        // @ts-ignore
        const filteredBodyFiles = bodyFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
        // @ts-ignore
        const filteredOtherFiles = otherFIles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
        // @ts-ignore
        const filteredUnassignedFiles = unassignedFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
        this.files = [...filteredCoverFiles, ...filteredBodyFiles, ...filteredOtherFiles, ...filteredUnassignedFiles];
    }
    setSequenceFromId(filter) {
        let files;
        if (filter) {
            files = this.getProductionFiles(filter);
        }
        else {
            files = this.getProductionFiles();
        }
        const key = "id";
        const sortedFiles = files.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
        for (const _f in sortedFiles) {
            const file = sortedFiles[_f];
            file.sequence = parseInt(_f) + 1;
        }
    }
    addMessage(severity, message, setLastError = true) {
        if (severity > this.results.severity && severity !== 0) {
            this.results.severity = severity;
        }
        if (setLastError && severity >= this.results.severity) {
            if (severity === 1) {
                this.results.lastErrorMessage = message;
                this.results.lastErrorId = "setOrderInfo";
                this.results.lastErrorCode = -123;
            }
            else if (severity === 2) {
                this.results.lastErrorMessage = message;
                this.results.lastErrorId = "setOrderWarning";
                this.results.lastErrorCode = -124;
            }
            else if (severity === 3) {
                this.results.lastErrorMessage = message;
                this.results.lastErrorId = "setOrderError";
                this.results.lastErrorCode = -125;
            }
        }
        const _message = new interfaces_1.Message(severity, message);
        this.messages.push(_message);
    }
    getPart(type) {
        // If part type is wild card return part that is not cover or body
        if (type === types_1.PART_TYPES.wildcard) {
            return this.parameter.parts.find((part) => part.type !== types_1.PART_TYPES.body && part.type !== types_1.PART_TYPES.cover);
        }
        return this.parameter.parts.find((part) => part.type === type);
    }
    getActualPages(filter) {
        let files = this.getProductionFiles();
        if (filter)
            files = this.getProductionFiles(filter);
        return files.reduce((acc, file) => acc + (file.numPages || 0), 0);
    }
    getAllWidths() {
        const files = this.getProductionFiles();
        const widths = [];
        for (const file of files) {
            const width = file.getWidth();
            const TOLERANCE = 3;
            const LOW = width - TOLERANCE;
            const HIGH = width + TOLERANCE;
            let widthExists = false;
            for (const width of widths) {
                if (width > LOW && width < HIGH)
                    widthExists = true;
            }
            if (!widthExists) {
                // If no width is found add it to widths
                widths.push(width);
            }
        }
        return widths;
    }
    addFinalMessages() {
        const allProductionFiles = this.files;
        const allSidecarFiles = this.sidecarFiles;
        if (allSidecarFiles.length > 0) {
            this.addMessage(1, `The following files were determined as sidecar files:\n  ${allSidecarFiles.map((file) => `fileName: ${file.oriName}`).join("\n  ")}`, false);
        }
        const filesWithoutTypeOrSequence = allProductionFiles.filter((file) => typeof file.type === "undefined" || typeof file.sequence === "undefined");
        if (this.results.severity !== 3)
            this.addMessage(1, `The product part type and the order of all files were set as follows:\n  ${allProductionFiles
                .map((file) => `file name: ${file.oriName}, type: ${file.type}, sequence: ${file.sequence}`)
                .join("\n  ")}`, false);
        if (this.results.severity !== 3 && filesWithoutTypeOrSequence.length === 0) {
            this.addMessage(1, `The order of all files was successfully determined.`);
        }
        else if (this.results.severity !== 3) {
            this.addMessage(3, `The order of files could not be determined.`);
        }
    }
    updateFiles() {
        if (this.parameter.mode === "uploadPortal") {
            // make sure the files are sorted
            this.sortFilesBy("sequence");
            let startSeq = 0;
            for (const uniqueId of this.uniqueIDs) {
                const singleFilesWithSameId = this.singleFiles.filter((file) => file.uniqueId === uniqueId);
                const originalFile = this.files.find((file) => file.uniqueId === uniqueId);
                if (originalFile) {
                    for (let i = 1; i < singleFilesWithSameId.length + 1; i++) {
                        const singleFile = singleFilesWithSameId[i - 1];
                        singleFile.type = originalFile.type;
                        if (typeof originalFile.sequence !== "undefined" && typeof singleFile.subSequence !== "object") {
                            singleFile.sequence = startSeq + singleFile.subSequence;
                        }
                    }
                }
                startSeq = startSeq + singleFilesWithSameId.length;
            }
            this.normalizeAllFiles(this.singleFiles);
            this.files = this.singleFiles.filter((file) => file.type !== types_1.FILE_TYPES.sidecar);
            this.sidecarFiles = this.singleFiles.filter((file) => file.type === types_1.FILE_TYPES.sidecar);
            this.sortFilesBy("sequence");
        }
        else {
            this.normalizeAllFiles(this.files);
            this.addMessage(1, `Updating sequence of files for further processing.`);
        }
    }
    normalizeAllFiles(files) {
        this.normalizeSequence(files, types_1.FILE_TYPES.body);
        this.normalizeSequence(files, types_1.FILE_TYPES.cover);
        this.normalizeSequence(files, types_1.FILE_TYPES.wildcard);
    }
    normalizeSequence(files, type) {
        const filesOfSameType = files.filter((file) => file.type === type).sort((a, b) => { var _a, _b, _c, _d; return (((_a = a.sequence) !== null && _a !== void 0 ? _a : 0) > ((_b = b.sequence) !== null && _b !== void 0 ? _b : 0) ? 1 : ((_c = b.sequence) !== null && _c !== void 0 ? _c : 0) > ((_d = a.sequence) !== null && _d !== void 0 ? _d : 0) ? -1 : 0); });
        const start = 2;
        for (let i = 0; i < filesOfSameType.length; i++) {
            const file = filesOfSameType[i];
            if (typeof file.sequence !== "undefined") {
                file.sequence = start + i;
            }
        }
    }
    setPageRangeFromFile(id) {
        const file = this.files.find((file) => file.id === id);
        if (file) {
            this.results.pageRangeSpine = file.pages
                .filter((page) => page.subType === interfaces_1.SUB_TYPE.spine)
                .map((page) => page.page)
                .join(",");
            this.results.pageRangeCover = file.pages
                .filter((page) => page.type === types_1.FILE_TYPES.cover && page.subType !== interfaces_1.SUB_TYPE.spine)
                .map((page) => page.page)
                .join(",");
            this.results.pageRangeBody = file.pages
                .filter((page) => page.type === types_1.FILE_TYPES.body)
                .map((page) => page.page)
                .join(",");
        }
    }
    toJSON() {
        const json = {
            files: this.files,
            sidecarFiles: this.sidecarFiles,
            parts: this.parameter.parts,
            results: this.results,
        };
        return json;
    }
}
exports.Product = Product;
//# sourceMappingURL=product.js.map