import { IFile, SUB_TYPE, IPage } from "./interfaces";
import { FILE_TYPES } from "./types";
export declare class File implements IFile {
    id: string;
    filePath?: string;
    type?: FILE_TYPES;
    subType: SUB_TYPE;
    sequence?: number;
    numPages: number;
    pages: IPage[];
    name: string;
    extension: string;
    oriName: string;
    nameWithoutRegexMatch?: string;
    removeFromFileRegex?: string;
    maskedOffMatches: any[];
    subSequence: number | null;
    loggingName: string;
    uniqueId?: string;
    pageRangeCover?: string;
    pageRangeSpine?: string;
    pageRangeBody?: string;
    constructor(params: IFile, config: any);
    getName(includeExtension?: boolean): string;
    getLoggingName(removeRegex?: string): string;
    initName(oriName: string, config: any): string;
    getUniqueId(oriName: string, config: any): string | undefined;
    getSubSequence(oriName: string, config: any): number | null;
    isBody(): boolean;
    isCover(): boolean;
    isAssigned(): boolean;
    hasSequence(): boolean;
    hasPdfProperties(): boolean;
    getNameHonoringSequence(): string;
    setWidth(value: number, pageRange?: number[]): void;
    setHeight(value: number, pageRange?: number[]): void;
    getAllWidths(): number[];
    setPageTypeFromSize(bodyWidth?: number, coverPages?: number, isSpread?: boolean): void;
    setPageTypeFromSizeCoverSinglePage(coverPages?: number): void;
    getWidth(_page?: number): number;
    getHeight(_page?: number): number;
    isPageSizeEqual(tolerance?: number): boolean;
    addPage(page: IPage): void;
    isSpread(singlePageWidth?: number): boolean;
    setPageRangeFromFile(): void;
}
//# sourceMappingURL=file.d.ts.map