import { File } from "../definitions/file";
import { IRegexPages, IRegexPagesMonth, SUB_TYPE } from "../definitions/interfaces";
import { Product } from "../definitions/product";
import { FILE_TYPES, FILTER_FILES } from "../definitions/types";
import { setSubTypeFromPairs } from "./type";

export function setSequenceFromPairs(product: Product, config: any, excludeSubTypes: SUB_TYPE[] = [], filter?: FILTER_FILES) {
  const files = product.getProductionFiles(filter).filter((file) => typeof file.sequence === "undefined" && !excludeSubTypes.includes(file.subType));

  if (files.length === 4) product.addMessage(1, `Trying to determine front inside, front outside, back inside and back outside files.`);
  if (files.length === 2) product.addMessage(1, `Trying to determine front and back files.`);

  setSubTypeFromPairs(product, config, excludeSubTypes, filter);

  const filesThatMatchFront = files.filter((file) => file.subType === SUB_TYPE.front);
  const filesThatMatchBack = files.filter((file) => file.subType === SUB_TYPE.back);

  const frontFileThatMatchesInside = files.filter((file) => file.subType === SUB_TYPE.coverFrontInside);
  const frontFileThatMatchesOutside = files.filter((file) => file.subType === SUB_TYPE.coverFrontOutside);
  const backFileThatMatchesInside = files.filter((file) => file.subType === SUB_TYPE.coverBackInside);
  const backFileThatMatchesOutside = files.filter((file) => file.subType === SUB_TYPE.coverBackOutside);

  if (frontFileThatMatchesInside.length === 1 && frontFileThatMatchesOutside.length === 1 && backFileThatMatchesInside.length === 1 && backFileThatMatchesOutside.length === 1) {
    product.addMessage(1, `Determined the sub type of the following cover files:`);
    frontFileThatMatchesOutside[0].sequence = 1;
    frontFileThatMatchesInside[0].sequence = 2;
    backFileThatMatchesInside[0].sequence = 3;
    backFileThatMatchesOutside[0].sequence = 4;
  } else if (files.length === 2 && filesThatMatchFront.length === 1 && filesThatMatchBack.length === 1) {
    filesThatMatchFront[0].sequence = 1;
    filesThatMatchBack[0].sequence = 2;
  }
}

export function setSequenceForFlatWithVariants(product: Product, config: any): void {
  const files = product.getProductionFiles();
  const numFiles = files.length;

  setSubTypeFromPairs(product, config);

  const filesThatMatchFront = files.filter((file) => file.subType === SUB_TYPE.front);
  const filesThatMatchBack = files.filter((file) => file.subType === SUB_TYPE.back);

  if (filesThatMatchFront.length === filesThatMatchBack.length && filesThatMatchFront.length + filesThatMatchBack.length === numFiles) {
    let sequence = 1;
    for (const file of filesThatMatchFront) {
      const matchingBackFile = filesThatMatchBack.find((_file) => _file.nameWithoutRegexMatch === file.nameWithoutRegexMatch);
      if (matchingBackFile && matchingBackFile.nameWithoutRegexMatch === file.nameWithoutRegexMatch) {
        file.sequence = sequence;
        matchingBackFile.sequence = sequence + 1;
        sequence = sequence + 2;
      } else {
        product.addMessage(3, `Couldn't find matching back file for ${file.loggingName}`);
      }
    }
  } else {
    product.addMessage(3, `Couldn't determine sequence of files: ${files.map((file) => file.loggingName).join(", ")}.`);
  }
}

export function setSequenceFromFileName(product: Product, type: FILE_TYPES, regexes: string[], sequence: number, excludeSpine = true): void {
  const filesWithoutSequence = product.getProductionFiles(FILTER_FILES.hasNoSequence).filter((file) => file.type === type);

  for (const file of filesWithoutSequence) {
    if (excludeSpine && file.subType !== SUB_TYPE.spine) {
      for (const regex of regexes) {
        const match = file.name?.match(new RegExp(regex));
        if (match) {
          file.sequence = sequence;
          product.addMessage(1, `File name: ${file.name} matched regex: ${regex}. Set sequence to ${sequence}.`);
          break;
        }
      }
    } else {
      for (const regex of regexes) {
        const match = file.name?.match(new RegExp(regex));
        if (match) {
          file.sequence = sequence;
          product.addMessage(1, `File name: ${file.name} matched regex: ${regex}. Set sequence to ${sequence}.`);
          break;
        }
      }
    }
  }
}

export function getSequenceFromFileName(product: Product, type: FILE_TYPES, regexes: string[]): void {
  const filesWithoutSequence = product.getProductionFiles(FILTER_FILES.hasNoSequence).filter((file) => file.type === type);

  for (const file of filesWithoutSequence) {
    for (const regex of regexes) {
      const match = file.name?.match(new RegExp(regex));
      if (match) {
        file.sequence = parseFloat(match[0]);
        product.addMessage(1, `File name: ${file.name} matched regex: ${regex}. Set sequence to ${match[0]}.`);
        break;
      }
    }
  }
}

export function setSequenceFromMissingSequences_Body(product: Product): void {
  const filesWithoutSequence = product.getProductionFiles(FILTER_FILES.hasNoSequence);
  const filesLeft = filesWithoutSequence.filter((file) => file.type === FILE_TYPES.body);

  if (filesLeft.length === 1) {
    const file = filesLeft[0];

    const sequencesOfSameType = product
      .getProductionFiles(FILTER_FILES.isBody)
      .filter((file) => file.hasSequence())
      .map((file) => file.sequence);

    if (sequencesOfSameType) {
      // @ts-ignore
      const sequence = getBodySequence(sequencesOfSameType);

      if (sequence) {
        file.sequence = sequence;
        product.addMessage(1, `One file left with undefined sequence. Therefore set sequence of file: ${file.loggingName} to the missing sequence of ${sequence}.`);
      } else {
        product.addMessage(3, `Failed to determine sequence of the file: ${file.loggingName}.`);
      }
    }
  }
}

export function getBodySequence(sequences: number[]): number | null {
  let seq;
  if (sequences.length === 1) {
    const neededSeq = [1, 2];
    seq = neededSeq.filter((element) => !sequences.includes(element));
  } else {
    return null;
  }
  return seq[0];
}

export function setSequenceFromMissingSequencesCover(product: Product, excludeSpine = true): void {
  const filesWithoutSequence = product.getProductionFiles(FILTER_FILES.hasNoSequence);
  const fileLeft = excludeSpine
    ? filesWithoutSequence.filter((file) => file.type === FILE_TYPES.cover && file.subType !== SUB_TYPE.spine)
    : filesWithoutSequence.filter((file) => file.type === FILE_TYPES.cover);

  if (fileLeft.length === 1) {
    const file = fileLeft[0];

    const sequencesOfSameType = product
      .getProductionFiles(FILTER_FILES.isCover)
      .filter((file) => file.hasSequence())
      .map((file) => file.sequence);

    if (sequencesOfSameType) {
      // @ts-ignore
      const sequence = getCoverSequence(sequencesOfSameType);

      if (sequence) {
        file.sequence = sequence;
        product.addMessage(1, `One file left with undefined sequence. Therefore set sequence of file: ${file.loggingName} to the missing sequence of ${sequence}.`);
      } else {
        product.addMessage(3, `Failed to determine sequence of the file: ${file.loggingName}.`);
      }
    }
  }
}

export function getCoverSequence(sequences: number[]): number | null {
  let seq;
  if (sequences.length === 1) {
    const neededSeq = [1, 2];
    seq = neededSeq.filter((element) => !sequences.includes(element));
  } else if (sequences.length === 3) {
    const neededSeq = [1, 2, 3, 4];
    seq = neededSeq.filter((element) => !sequences.includes(element));
  } else if (sequences.length === 4) {
    const neededSeq = [1, 2, 3, 4, 5];
    seq = neededSeq.filter((element) => !sequences.includes(element));
  } else {
    return null;
  }
  return seq[0];
}

export function setSequenceFromFileName_pages(product: Product, type: FILE_TYPES, regexArray: IRegexPages[], excludeSubTypes: SUB_TYPE[] = [], allFilesMustMatch?: boolean): void {
  const filesWithoutSequence = product
    .getProductionFiles(FILTER_FILES.hasNoSequence)
    .filter((file) => excludeSubTypes.indexOf(file.subType) === -1 && file.type === type && file.subType !== SUB_TYPE.spine);

  if (filesWithoutSequence.length > 0) {
    for (const regexObj of regexArray) {
      if (setSequenceFromFileName_regex(filesWithoutSequence, regexObj, product.parameter.ignoreNumbersAbove, allFilesMustMatch)) return;
    }
  }
}

export function setSequenceFromFileName_pages_v2(
  product: Product,
  regexArray: IRegexPages[],
  excludeSubTypes: SUB_TYPE[] = [],
  type?: FILE_TYPES,
  allFilesMustMatch?: boolean,
): void {
  const filesWithoutSequence = product
    .getProductionFiles(FILTER_FILES.hasNoSequence)
    .filter((file) => excludeSubTypes.indexOf(file.subType) === -1 && file.subType !== SUB_TYPE.spine);

  const filteredFiles = filesWithoutSequence.filter((file) => file.type === type);
  if (filteredFiles.length > 0) {
    for (const regexObj of regexArray) {
      if (setSequenceFromFileName_regex(filteredFiles, regexObj, product.parameter.ignoreNumbersAbove, allFilesMustMatch)) return;
    }
  }
}

export function setSequenceFromFileName_pagesByMonth(
  product: Product,
  type: FILE_TYPES,
  regexArray: IRegexPagesMonth[] = [],
  excludeSubTypes: SUB_TYPE[] = [],
  allFilesMustMatch = true,
): boolean {
  const filesWithoutSequence = product.getProductionFiles(FILTER_FILES.hasNoSequence).filter((file) => !excludeSubTypes.includes(file.subType) && file.type === type);

  const filesThatMatchCoverAndMonth = product
    .getProductionFiles(FILTER_FILES.hasNoSequence)
    .filter((file) => file.subType === SUB_TYPE.monthBack)
    .map((file) => file.getName());

  const filesThatMatch: any[] = [];

  let isDescending = false;
  let isAscending = false;

  if (filesWithoutSequence.length > 1) {
    for (const regexObj of regexArray) {
      for (const file of filesWithoutSequence) {
        const match = file.name.match(new RegExp(regexObj.expression));
        if (match && typeof match?.index !== "undefined") {
          let sequence = filesThatMatchCoverAndMonth.length > 0 ? regexObj.sequence * 2 - 1 : regexObj.sequence;

          if (filesThatMatchCoverAndMonth.indexOf(file.name) > -1) {
            sequence += 1;
            // prefixesBack.push(file.name.substring(0, match.index));
          } else {
            // prefixesFront.push(file.name.substring(0, match.index));
          }

          filesThatMatch.push({
            fileId: file.id,
            fileName: file.name,
            sequence: sequence,
          });
        }
      }
    }
  }

  const hasEqualAmountOfBackAndFrontFiles = filesThatMatchCoverAndMonth.length === 0 || filesThatMatch.length / 2 === filesThatMatchCoverAndMonth.length;

  if (hasEqualAmountOfBackAndFrontFiles && (!allFilesMustMatch || filesThatMatch.length === filesWithoutSequence.length)) {
    const sortedMatches = filesThatMatch.sort(compare);

    isDescending = isArrayDescending(sortedMatches.map((matchObj) => matchObj.sequence));
    isAscending = isArrayAscending(sortedMatches.map((matchObj) => matchObj.sequence));

    if (isDescending || isAscending) {
      for (const matchObj of sortedMatches) {
        const file = filesWithoutSequence.find((file) => file.id === matchObj.fileId);
        if (file) {
          file.sequence = matchObj.sequence;
        }
      }
      return true;
    }
  }

  return false;
}

export function setSequenceFromFileName_regex(files: File[], regexObj: IRegexPages, ignoreNumbersAbove = -1, allFilesMustMatch = true): boolean {
  // TODO: Make it optional that amount of files must be equal the amount of matches
  const filesThatMatch: any[] = [];
  const regExp = new RegExp(regexObj.expression, "g");

  let isDescending = false;
  let isAscending = false;

  const regExpArray = [];

  for (const file of files) {
    const fileName = file.getName() || "";
    const matches = [...fileName.matchAll(regExp)];

    if (matches) {
      for (const _m in matches) {
        const match = matches[_m];
        if (typeof match.index !== "undefined") {
          const filesThatMatchObj: any = {
            fileId: file.id,
            fileName: fileName,
            regExp: regExp,
            matchGroup: _m,
            match: match[0],
            prefix: fileName.substring(0, match.index),
            suffix: fileName.substring(match.index + match[0].length),
          };
          filesThatMatch.push(filesThatMatchObj);

          regExpArray.push({
            regExp: regExp,
            matchGroup: _m,
            match: match[0],
            prefix: fileName.substring(0, match.index),
            suffix: fileName.substring(match.index + match[0].length),
          });
        }
      }
    }
  }

  for (const obj of [...new Set(regExpArray)]) {
    const filesOfSameGroup = filesThatMatch.filter((file) => file.matchGroup === obj.matchGroup);

    const filesWithSameSuffix = filesOfSameGroup.filter((file) => file.suffix === obj.suffix);
    const filesWithSamePrefix = filesOfSameGroup.filter((file) => file.prefix === obj.prefix);
    const fileWithSameSuffixAndPrefix = filesOfSameGroup.filter((file) => file.suffix === obj.suffix && file.prefix === obj.prefix);

    const hasSameSuffix = filesOfSameGroup.length === filesWithSameSuffix.length ? true : false;
    const hasSamePrefix = filesOfSameGroup.length === filesWithSamePrefix.length ? true : false;
    const hasSameSuffixAndPrefix = filesOfSameGroup.length === fileWithSameSuffixAndPrefix.length ? true : false;

    if (
      (regexObj.needsEqualPrefix && hasSamePrefix) ||
      (regexObj.needsEqualPostfix && hasSameSuffix) ||
      (regexObj.needsEqualPrefixAndPostfix && hasSameSuffixAndPrefix) ||
      (!regexObj.needsEqualPrefix && !regexObj.needsEqualPostfix && !regexObj.needsEqualPrefixAndPostfix)
    ) {
      if (!allFilesMustMatch || filesOfSameGroup.length === files.length) {
        const sequences = [];

        for (const file of filesOfSameGroup) {
          const sequence: number = parseInt(file.match.match(/\d+/g));

          if (ignoreNumbersAbove !== (0 || -1) && sequence > ignoreNumbersAbove) {
            return false;
          }

          sequences.push({
            fileId: file.fileId,
            sequence: sequence,
          });
        }

        const sortedSequences = sequences.sort(compare);
        const allFilesMatchRegex = files.length === sortedSequences.length;

        isDescending = isArrayDescending(sortedSequences.map((sequence) => sequence.sequence));
        isAscending = isArrayAscending(sortedSequences.map((sequence) => sequence.sequence));

        if ((!allFilesMustMatch && (isDescending || isAscending)) || (allFilesMatchRegex && (isDescending || isAscending))) {
          for (const seqObj of sortedSequences) {
            const file = files.find((file) => file.id === seqObj.fileId);
            if (file) {
              file.sequence = seqObj.sequence;
            }
          }
          return true;
        }
      }
    }
  }

  return false;
}

function compare(a: any, b: any) {
  if (a.sequence < b.sequence) {
    return -1;
  }
  if (a.sequence > b.sequence) {
    return 1;
  }
  return 0;
}

function isArrayAscending(array: any[]) {
  let retval = false;
  for (let i = 0; i < array.length; i++) {
    if (array[i] < array[i + 1]) {
      retval = true;
    } else {
      if (typeof array[i + 1] !== "undefined") {
        return false;
      }
    }
  }
  return retval;
}

function isArrayDescending(array: any[]) {
  let retval = false;
  for (let i = 0; i < array.length; i++) {
    if (array[i] > array[i + 1]) {
      retval = true;
    } else {
      if (typeof array[i + 1] !== "undefined") {
        return false;
      }
    }
  }
  return retval;
}
