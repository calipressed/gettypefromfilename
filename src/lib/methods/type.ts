import { File } from "../definitions/file";
import { IFile, IPair, IRegexPagesMonth, ProductType, SUB_TYPE } from "../definitions/interfaces";
import { Product } from "../definitions/product";
import { FILE_TYPES, FILTER_FILES } from "../definitions/types";

export function setSubTypeFromPairs(product: Product, config: any, excludeSubTypes: SUB_TYPE[] = [], filter?: FILTER_FILES) {
  const files = product.getProductionFiles(filter).filter((file) => typeof file.sequence === "undefined" && !excludeSubTypes.includes(file.subType));
  const pairs = config.regex.sequence.pairs;

  // first run through pairs in strict mode (only expect front/back pairs)
  for (const pair of pairs) {
    if (product.type === ProductType.flat || product.type === ProductType.brochure) {
      if (setSubTypeFrontAndBackFromPair(product, files, pair)) return;
    } else {
      if (files.length === 2) {
        if (setSubTypeFrontAndBackFromPair(product, files, pair)) return;
      } else if (files.length === 4) {
        if (setSubTypeFrontAndBackInsideOutsideFromPairs(product, files, pair)) return;
      }
    }
  }

  // second run through pairs (allow assuming front or back files)
  for (const pair of pairs) {
    if (product.type === ProductType.flat) {
      if (setSubTypeFrontAndBackFromPair(product, files, pair, false)) return;
    } else {
      if (files.length === 2) {
        if (setSubTypeFrontAndBackFromPair(product, files, pair, false)) return;
      } else if (files.length === 4) {
        if (setSubTypeFrontAndBackInsideOutsideFromPairs(product, files, pair, false)) return;
      }
    }
  }
}

export function setSubTypeFrontAndBackFromPair(product: Product, files: File[], pair: IPair, strict = true) {
  const filesThatMatchFront = [];
  const filesThatMatchBack = [];

  for (const file of files) {
    const matchFront = file.name?.match(new RegExp(pair.front));
    const matchBack = file.name?.match(new RegExp(pair.back));

    if (matchFront) {
      file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.front), "");
      filesThatMatchFront.push(file);
    }

    if (matchBack) {
      file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.back), "");
      filesThatMatchBack.push(file);
    }
  }

  if (strict && filesThatMatchFront.length === files.length / 2 && filesThatMatchBack.length === files.length / 2) {
    filesThatMatchFront.map((file) => (file.subType = SUB_TYPE.front));
    filesThatMatchBack.map((file) => (file.subType = SUB_TYPE.back));

    const filesThatMatchFrontNames = filesThatMatchFront.map((file) => file.loggingName);
    const filesThatMatchBackNames = filesThatMatchBack.map((file) => file.loggingName);

    product.addMessage(1, `Determined: ${filesThatMatchFrontNames} as front files.`);
    product.addMessage(1, `Determined: ${filesThatMatchBackNames} as back files.`);
    return true;
  } else if (!strict && files.length === 2) {
    if (filesThatMatchFront.length === 1 && filesThatMatchBack.length === 0) {
      filesThatMatchFront[0].subType = SUB_TYPE.front;
      const otherFile = files.find((file) => file.subType !== SUB_TYPE.front);
      if (otherFile) {
        product.addMessage(1, `Determined file: ${filesThatMatchFront[0]?.loggingName} as front.`);
        otherFile.subType = SUB_TYPE.back;
        product.addMessage(2, `Assumed that file ${otherFile?.loggingName} is back.`);
        return true;
      }
    } else if (filesThatMatchFront.length === 0 && filesThatMatchBack.length === 1) {
      filesThatMatchBack[0].subType = SUB_TYPE.back;
      const otherFile = files.find((file) => file.subType !== SUB_TYPE.back);
      if (otherFile) {
        product.addMessage(1, `Determined file: ${filesThatMatchBack[0]?.loggingName} as back.`);
        otherFile.subType = SUB_TYPE.front;
        product.addMessage(2, `Assumed that file ${otherFile?.loggingName} is front.`);
        return true;
      }
    }
  }

  return false;
}

export function setSubTypeFrontOrBackFromPairs(product: Product, config: any, filter?: FILTER_FILES) {
  const files = product.getProductionFiles(filter).filter((file) => typeof file.sequence === "undefined");
  const pairs = config.regex.sequence.pairs;

  for (const pair of pairs) {
    // strict mode
    const filesThatMatchFront = [];
    const filesThatMatchBack = [];

    for (const file of files) {
      const matchFront = file.name?.match(new RegExp(pair.front));
      const matchBack = file.name?.match(new RegExp(pair.back));

      if (matchFront) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.front), "");
        filesThatMatchFront.push(file);
      }

      if (matchBack) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.back), "");
        filesThatMatchBack.push(file);
      }
    }

    if (filesThatMatchFront.length + filesThatMatchBack.length === files.length) {
      filesThatMatchFront.map((file) => (file.subType = SUB_TYPE.front));
      filesThatMatchBack.map((file) => (file.subType = SUB_TYPE.back));
      if (filesThatMatchFront.length >= 1) product.addMessage(1, `Determined files: ${filesThatMatchFront.map((file) => file.loggingName).join(", ")} as front.`);
      if (filesThatMatchBack.length >= 1) product.addMessage(1, `Determined files: ${filesThatMatchBack.map((file) => file.loggingName).join(", ")} as back.`);
      return;
    }
  }

  for (const pair of pairs) {
    const filesThatMatchFront = [];
    const filesThatMatchBack = [];

    for (const file of files) {
      const matchFront = file.name?.match(new RegExp(pair.front));
      const matchBack = file.name?.match(new RegExp(pair.back));

      if (matchFront) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.front), "");
        filesThatMatchFront.push(file);
      }

      if (matchBack) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.back), "");
        filesThatMatchBack.push(file);
      }
    }

    if (filesThatMatchFront.length > 0) {
      filesThatMatchFront.map((file) => (file.subType = SUB_TYPE.front));
      product.addMessage(1, `Determined files: ${filesThatMatchFront.map((file) => file.loggingName).join(", ")} as front pages.`);

      const filesThatDoNotMatch = files.filter((file) => file.subType !== SUB_TYPE.front);
      filesThatDoNotMatch.map((file) => (file.subType = SUB_TYPE.back));

      if (filesThatDoNotMatch.length > 0) product.addMessage(2, `Assumed that files: ${filesThatDoNotMatch.map((file) => file.loggingName).join(", ")} are the back pages.`);
      return;
    } else if (filesThatMatchBack.length > 0) {
      filesThatMatchBack.map((file) => (file.subType = SUB_TYPE.back));
      product.addMessage(1, `Determined files: ${filesThatMatchBack.map((file) => file.loggingName).join(", ")} as back pages.`);

      const filesThatDoNotMatch = files.filter((file) => file.subType !== SUB_TYPE.back);
      filesThatDoNotMatch.map((file) => (file.subType = SUB_TYPE.front));

      if (filesThatDoNotMatch.length > 0) product.addMessage(2, `Assumed that files: ${filesThatDoNotMatch.map((file) => file.loggingName).join(", ")} are the front pages.`);
    }
  }
}

export function setSubTypeFrontAndBackInsideOutsideFromPairs(product: Product, files: File[], pair: IPair, strict = true) {
  const filesThatMatchFront: File[] = [];
  const filesThatMatchBack: File[] = [];
  const filesThatMatchNone: File[] = [];
  const filesThatMatchOnlyInside: File[] = [];
  const filesThatMatchOnlyOutside: File[] = [];
  const filesThatMatchFrontInside: File[] = [];
  const filesThatMatchFrontOutside: File[] = [];
  const filesThatMatchBackInside: File[] = [];
  const filesThatMatchBackOutside: File[] = [];

  if (pair.inside && pair.outside) {
    for (const file of files) {
      const matchFront = file.name?.match(new RegExp(pair.front));
      const matchBack = file.name?.match(new RegExp(pair.back));
      const matchInside = pair.inside ? file.getName().match(new RegExp(pair.inside, "g")) : null;
      const matchOutside = pair.outside ? file.getName().match(new RegExp(pair.outside, "g")) : null;

      if (matchFront) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.front), "");
        filesThatMatchFront.push(file);
        if (matchInside) filesThatMatchFrontInside.push(file);
        else if (matchOutside) filesThatMatchFrontOutside.push(file);
      } else if (matchBack) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.back), "");
        filesThatMatchBack.push(file);
        if (matchInside) filesThatMatchBackInside.push(file);
        else if (matchOutside) filesThatMatchBackOutside.push(file);
      } else {
        filesThatMatchNone.push(file);
        if (matchInside) filesThatMatchOnlyInside.push(file);
        else if (matchOutside) filesThatMatchOnlyOutside.push(file);
      }
    }

    if (!strict) {
      if (filesThatMatchFront.length === 2 && filesThatMatchBack.length === 0) {
        filesThatMatchBack.push(...filesThatMatchNone);
        product.addMessage(2, `Assumed that files: ${filesThatMatchBack.map((file) => file.loggingName)} are back pages.`);
        if (filesThatMatchOnlyInside.length === 1) filesThatMatchBackInside.push(...filesThatMatchOnlyInside);
        if (filesThatMatchOnlyOutside.length === 1) filesThatMatchBackOutside.push(...filesThatMatchOnlyOutside);
      } else if (filesThatMatchBack.length === 2 && filesThatMatchFront.length === 0) {
        filesThatMatchFront.push(...filesThatMatchNone);
        product.addMessage(2, `Assumed that files: ${filesThatMatchFront.map((file) => file.loggingName)} are front pages.`);
        if (filesThatMatchOnlyInside.length === 1) filesThatMatchFrontInside.push(...filesThatMatchOnlyInside);
        if (filesThatMatchOnlyOutside.length === 1) filesThatMatchFrontOutside.push(...filesThatMatchOnlyOutside);
      }
      if (filesThatMatchFrontInside.length === 1 && filesThatMatchFrontOutside.length === 0) {
        const outsideFile = filesThatMatchFront.find((file) => file.name !== filesThatMatchFrontInside[0].name);
        if (outsideFile) {
          product.addMessage(2, `Assumed that file ${outsideFile.loggingName} is the outside of the front.`);
          filesThatMatchFrontOutside.push(outsideFile);
        }
      } else if (filesThatMatchFrontInside.length === 0 && filesThatMatchFrontOutside.length === 1) {
        const insideFile = filesThatMatchFront.find((file) => file.name !== filesThatMatchFrontOutside[0].name);
        if (insideFile) {
          product.addMessage(2, `Assumed that file ${insideFile.loggingName} is the inside of the front.`);
          filesThatMatchFrontInside.push(insideFile);
        }
      }

      if (filesThatMatchBackInside.length === 1 && filesThatMatchBackOutside.length === 0) {
        const outsideFile = filesThatMatchBack.find((file) => file.name !== filesThatMatchBackInside[0].name);
        if (outsideFile) {
          product.addMessage(2, `Assumed that file ${outsideFile.loggingName} is the outside of the back.`);
          filesThatMatchBackOutside.push(outsideFile);
        }
      } else if (filesThatMatchBackInside.length === 0 && filesThatMatchBackOutside.length === 1) {
        const insideFile = filesThatMatchBack.find((file) => file.name !== filesThatMatchBackOutside[0].name);
        if (insideFile) {
          product.addMessage(2, `Assumed that file ${insideFile.loggingName} is the inside of the back.`);
          filesThatMatchBackInside.push(insideFile);
        }
      }
    }

    if (filesThatMatchFrontInside.length === 1 && filesThatMatchFrontOutside.length === 1 && filesThatMatchBackInside.length === 1 && filesThatMatchBackOutside.length === 1) {
      filesThatMatchFrontInside.map((file) => (file.subType = SUB_TYPE.coverFrontInside));
      filesThatMatchFrontOutside.map((file) => (file.subType = SUB_TYPE.coverFrontOutside));
      filesThatMatchBackInside.map((file) => (file.subType = SUB_TYPE.coverBackInside));
      filesThatMatchBackOutside.map((file) => (file.subType = SUB_TYPE.coverBackOutside));

      product.addMessage(1, `${filesThatMatchFrontInside[0]?.loggingName} is determined as cover front inside.`);
      product.addMessage(1, `${filesThatMatchFrontOutside[0]?.loggingName} is determined as cover front outside.`);
      product.addMessage(1, `${filesThatMatchBackInside[0]?.loggingName} is determined as cover back inside.`);
      product.addMessage(1, `${filesThatMatchBackOutside[0]?.loggingName} is determined as cover back outside.`);
      return true;
    }
  }

  return false;
}

export function setSubTypeForMonthsFromPairs(filesThatMatchMonth: File[], pairs: IPair[], product: Product) {
  for (const pair of pairs) {
    const filesThatMatchBack = [];
    const filesThatMatchFront = [];

    for (const file of filesThatMatchMonth) {
      const matchFront = file.name?.match(new RegExp(pair.front));
      const matchBack = file.name?.match(new RegExp(pair.back));

      if (matchFront) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.front), "");
        filesThatMatchFront.push(file);
      }

      if (matchBack) {
        file.nameWithoutRegexMatch = file.name?.replace(new RegExp(pair.back), "");
        filesThatMatchBack.push(file);
      }
    }
    if (filesThatMatchFront.length === filesThatMatchMonth.length / 2 && filesThatMatchBack.length === filesThatMatchMonth.length / 2) {
      filesThatMatchFront.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthFront)));
      filesThatMatchBack.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthBack)));
      break;
    } else if (filesThatMatchFront.length === filesThatMatchMonth.length / 2) {
      filesThatMatchFront.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthFront)));
      filesThatMatchBack.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthBack)));

      const filesThatDoNotMatchFront = filesThatMatchMonth.filter((file) => file.subType !== SUB_TYPE.monthBack && file.subType !== SUB_TYPE.monthFront);
      filesThatDoNotMatchFront.map((file) => (file.subType = SUB_TYPE.monthBack));

      product.addMessage(2, `Assumed that the files: ${filesThatDoNotMatchFront.map((file) => file.name).join(", ")} are the back files of the calendar.`);
      break;
    } else if (filesThatMatchBack.length === filesThatMatchMonth.length / 2) {
      filesThatMatchBack.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthBack)));
      filesThatMatchFront.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.monthFront)));

      const filesThatDoNotMatchBack = filesThatMatchMonth.filter((file) => file.subType !== SUB_TYPE.monthBack && file.subType !== SUB_TYPE.monthFront);
      filesThatDoNotMatchBack.map((file) => (file.subType = SUB_TYPE.monthFront));

      product.addMessage(2, `Assumed that the files: ${filesThatDoNotMatchBack.map((file) => file.name).join(", ")} are the front files of the calendar.`);
      break;
    }
  }
}

export function setTypeForMonth(product: Product, config: any) {
  const filesThatMatchMonth = getFilesThatMatchMonth(product, config.regex.sequence.pagesByMonth);
  filesThatMatchMonth.map((file) => ((file.type = FILE_TYPES.body), (file.subType = SUB_TYPE.month)));

  setSubTypeForMonthsFromPairs(filesThatMatchMonth, config.regex.sequence.pairs, product);
}

export function setTypeFromPagesCover(product: Product, excludeRegexes: string[] = []) {
  const allFiles = product.getProductionFiles();

  const alreadyAssignedBodyFilesPages = product.getProductionFiles(FILTER_FILES.isBody).reduce(function (acc, file) {
    return acc + file.numPages;
  }, 0);

  const alreadyAssignedCoverFilesPages = product.getProductionFiles(FILTER_FILES.isCover).reduce(function (acc, file) {
    return acc + file.numPages;
  }, 0);

  const possibleCoverFiles = allFiles.filter(
    (file) =>
      file.numPages + alreadyAssignedCoverFilesPages === product.parameter.neededCoverPages && file.numPages + alreadyAssignedBodyFilesPages !== product.parameter.neededBodyPages,
  );

  // Only set type to cover if one file found and it's not already assigned
  // The file name must not match any exclude regex
  if (possibleCoverFiles.length === 1 && !possibleCoverFiles[0].isAssigned()) {
    if (excludeRegexes.length > 0) {
      let setType = true;
      for (const regex of excludeRegexes) {
        const match = possibleCoverFiles[0].name.match(new RegExp(regex));
        if (match) {
          setType = false;
        }
      }
      if (setType) {
        possibleCoverFiles[0].type = FILE_TYPES.cover;
        product.addMessage(1, `Assumed that file ${possibleCoverFiles[0].loggingName} with ${possibleCoverFiles[0].numPages} pages is of type cover.`);
      }
    } else {
      possibleCoverFiles[0].type = FILE_TYPES.cover;
      product.addMessage(1, `Assumed that file ${possibleCoverFiles[0].loggingName} with ${possibleCoverFiles[0].numPages} pages is of type cover.`);
    }
  }
}

export function setTypeFromPagesBody(product: Product, excludeRegexes: string[] = []) {
  const allFiles = product.getProductionFiles();

  const alreadyAssignedBodyFilesPages = product.getProductionFiles(FILTER_FILES.isBody).reduce(function (acc, file) {
    return acc + file.numPages;
  }, 0);

  const alreadyAssignedCoverFilesPages = product.getProductionFiles(FILTER_FILES.isCover).reduce(function (acc, file) {
    return acc + file.numPages;
  }, 0);

  const possibleBodyFiles = allFiles.filter(
    (file) =>
      file.numPages + alreadyAssignedBodyFilesPages === product.parameter.neededBodyPages && file.numPages + alreadyAssignedCoverFilesPages !== product.parameter.neededCoverPages,
  );

  // Only set type to cover if one file found and it's not already assigned
  // The file name must not match any exclude regex
  if (possibleBodyFiles.length === 1 && !possibleBodyFiles[0].isAssigned()) {
    if (excludeRegexes.length > 0) {
      let setType = true;
      for (const regex of excludeRegexes) {
        const match = possibleBodyFiles[0].name.match(new RegExp(regex));
        if (match) {
          setType = false;
        }
      }
      if (setType) {
        possibleBodyFiles[0].type = FILE_TYPES.body;
        product.addMessage(1, `Assumed that file ${possibleBodyFiles[0].loggingName} with ${possibleBodyFiles[0].numPages} pages is of type body.`);
      }
    } else {
      possibleBodyFiles[0].type = FILE_TYPES.body;
      product.addMessage(1, `Assumed that file ${possibleBodyFiles[0].loggingName} with ${possibleBodyFiles[0].numPages} pages is of type body.`);
    }
  }
}

export function setTypeFromPagesOnePart(product: Product, excludeSubTypes: SUB_TYPE[] = []) {
  const allFiles = excludeSubTypes.length > 1 ? product.getProductionFiles().filter((file) => excludeSubTypes.indexOf(file.subType) === -1) : product.getProductionFiles();

  // Search all files if there is only one with the specified amount of pages
  const coverFiles = allFiles.filter((file) => file.numPages === 2 || file.numPages === 4);

  // Only set type to cover if one file found and it's not already assigned
  if (coverFiles.length === 1 && !coverFiles[0].isAssigned()) {
    coverFiles[0].type = FILE_TYPES.cover;
  }
}

export function setTypeFromNameRegex(product: Product, regexes: string[], fileType: FILE_TYPES, excludeSubTypes: SUB_TYPE[] = [], setSubType?: SUB_TYPE) {
  const unassignedFiles = product.getProductionFiles(FILTER_FILES.isUnassigned);

  for (const file of unassignedFiles) {
    if (excludeSubTypes.indexOf(file.subType) === -1) {
      for (const regex of regexes) {
        const match = file.name?.match(new RegExp(regex));
        if (match) {
          // Set to cover if name matches regex
          file.type = fileType;
          if (setSubType) file.subType = setSubType;
          product.addMessage(1, `File name: ${file.name} matches regex: ${regex}. Set type to ${fileType}.`);
          break;
        }
      }
    }
  }
}

export function getFilesThatMatchRegexes(files: File[], regexes: string[]) {
  const filesThatMatch = [];

  for (const file of files) {
    for (const regex of regexes) {
      const match = file.name?.match(new RegExp(regex));
      if (match) {
        filesThatMatch.push(file);
      }
    }
  }
  return filesThatMatch;
}

export function getFilesThatMatchBackAndMonth(product: Product, regexesMonth: IRegexPagesMonth[] = [], regexesBack: string[] = [], filter?: FILTER_FILES): File[] {
  const files = filter ? product.getProductionFiles(filter) : product.getProductionFiles();

  const filesThatMatchMonthAndBack: File[] = [];
  const filesThatMatchOnlyBack: File[] = [];

  for (const file of files) {
    for (const regexMonthObj of regexesMonth) {
      for (const regexBack of regexesBack) {
        const matchMonth = file.name?.match(new RegExp(regexMonthObj.expression));
        const matchBack = file.name?.match(new RegExp(regexBack));
        if (matchMonth) {
          if (matchBack) {
            filesThatMatchMonthAndBack.push(file);
          }
        } else if (matchBack) {
          filesThatMatchOnlyBack.push(file);
        }
      }
    }
  }

  return filesThatMatchMonthAndBack;
}

export function getFilesThatMatchFrontAndMonth(product: Product, regexesMonth: IRegexPagesMonth[] = [], regexesFront: string[] = [], filter?: FILTER_FILES): File[] {
  const files = filter ? product.getProductionFiles(filter) : product.getProductionFiles();

  const filesThatMatchMonthAndFront: File[] = [];
  const filesThatMatchOnlyFront: File[] = [];

  for (const file of files) {
    for (const regexMonthObj of regexesMonth) {
      for (const regexFront of regexesFront) {
        const matchMonth = file.name?.match(new RegExp(regexMonthObj.expression));
        const matchFront = file.name?.match(new RegExp(regexFront));
        if (matchMonth) {
          if (matchFront) {
            filesThatMatchMonthAndFront.push(file);
          }
        } else if (matchFront) {
          filesThatMatchOnlyFront.push(file);
        }
      }
    }
  }

  return filesThatMatchMonthAndFront;
}

export function getFilesThatMatchMonth(product: Product, regexesMonth: IRegexPagesMonth[] = [], filter?: FILTER_FILES): File[] {
  const files = filter ? product.getProductionFiles(filter) : product.getProductionFiles();
  const filesThatMatchMonth: File[] = [];

  for (const file of files) {
    for (const regexMonthObj of regexesMonth) {
      const matchMonth = file.name?.match(new RegExp(regexMonthObj.expression));
      if (matchMonth) {
        filesThatMatchMonth.push(file);
      }
    }
  }

  return filesThatMatchMonth;
}

export function setTypesFromSize(product: Product) {
  const unassignedFiles = product.getProductionFiles(FILTER_FILES.isUnassigned);
  const widths = product.getAllWidths();

  const coverFiles = product.getProductionFiles(FILTER_FILES.isCover);
  let numCoverPages = coverFiles.length > 0 ? coverFiles.map((file) => file.numPages).reduce((sum, numPages) => sum + numPages) : 0;

  const wildcardFiles = product.getProductionFiles(FILTER_FILES.isWildcard);
  let numWildcardPages = wildcardFiles.length > 0 ? wildcardFiles.map((file) => file.numPages).reduce((sum, numPages) => sum + numPages) : 0;

  const smallestWidth = widths.reduce((prev, curr) => (prev < curr ? prev : curr));
  const greatestWidth = widths.reduce((prev, curr) => (prev > curr ? prev : curr));
  const middleWidth = widths.find((width) => width !== smallestWidth && width !== greatestWidth);

  switch (widths.length) {
    case 1:
      break;
    case 2: {
      if (numCoverPages < product.parameter.neededCoverPages) {
        // assumend only cover and body files
        for (const file of unassignedFiles) {
          const actualWidth = file.getWidth();
          if (numCoverPages < product.parameter.neededCoverPages && isWidthDoubleOrMoreThanSinglePage(actualWidth, smallestWidth)) {
            if (file.numPages <= 4) {
              product.addMessage(1, `File: ${file.name} has more than two times the width of a single page. Therefore the part type is set to cover.`);
              file.type = FILE_TYPES.cover;
              numCoverPages = numCoverPages + file.numPages;
            }
          }
        }
      }
      break;
    }
    case 3: {
      for (const file of unassignedFiles) {
        const actualWidth = file.getWidth();

        if (middleWidth) {
          if (numCoverPages < product.parameter.neededCoverPages && isWidthDoubleOrMoreThanSinglePage(actualWidth, middleWidth)) {
            if (file.numPages <= 4) {
              file.type = FILE_TYPES.cover;
              product.addMessage(1, `Due to the width of the file: ${file.name} it is set to cover.`);
              numCoverPages = numCoverPages + 1;
            }
          } else if (
            product.parameter.needsAdditionalPart &&
            isWidthHalfOrLessSinglePage(actualWidth, middleWidth) &&
            numWildcardPages < product.parameter.neededAdditionalPartPages
          ) {
            if (file.numPages <= 4) {
              file.type = FILE_TYPES.wildcard;
              product.addMessage(1, `Due to the width of the file: ${file.name} it is set to wildcard.`);
              numWildcardPages = numWildcardPages + file.numPages;
            }
          }
        }
      }
      break;
    }
    default:
      product.addMessage(1, "Trying to determine type by width of files.");
      break;
  }
}

export function isWidthDoubleOrMoreThanSinglePage(actualWidth: number, singlePageWidth?: number) {
  if (singlePageWidth && actualWidth >= singlePageWidth * 2) {
    return true;
  }
  return false;
}

export function isWidthHalfOrLessSinglePage(actualWidth: number, singlePageWidth: number) {
  if (actualWidth <= singlePageWidth / 2) {
    return true;
  }
  return false;
}

export function mode(arr: any[]) {
  if (arr.length === 2) {
    return Math.min(...arr);
  }
  return arr.sort((a, b) => arr.filter((v) => v === a).length - arr.filter((v) => v === b).length).pop();
}

export function isEqual(x: number, y: number, tolerance = 3) {
  return x - tolerance < y && y < x + tolerance;
}
