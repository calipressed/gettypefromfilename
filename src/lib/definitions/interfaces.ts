import { FILE_TYPES, PART_TYPES } from "./types";

export interface IPair {
  front: string;
  back: string;
  inside?: string;
  outside?: string;
}

export interface IRegexPagesMonth {
  expression: string;
  sequence: number;
}

export interface IRegexPages {
  expression: string;
  needsEqualPrefix?: boolean;
  needsEqualPostfix?: boolean;
  needsEqualPrefixAndPostfix?: boolean;
}

export interface IFile {
  id?: string;
  filePath?: string;
  type?: FILE_TYPES;
  sequence?: number;
  numPages?: number;
  pages?: IPage[];
  isProductionPart?: boolean;
  name: string;
}

export enum SUB_TYPE {
  none = "none",
  spine = "spine",
  monthBack = "monthBack",
  monthFront = "monthFront",
  month = "month",
  cover = "cover",
  front = "front",
  back = "back",
  coverFrontInside = "coverFrontInside",
  coverFrontOutside = "coverFrontOutside",
  coverBackInside = "coverBackInside",
  coverBackOutside = "coverBackOutside",
}

export interface IPage {
  subType?: SUB_TYPE;
  type?: FILE_TYPES;
  width: number;
  height: number;
  page: number;
}

export interface IMessage {
  severity: 0 | 1 | 2 | 3;
  severityString: "debug" | "info" | "warn" | "error";
  message: string;
}

export class Message implements IMessage {
  severity: 0 | 1 | 2 | 3;
  severityString: "debug" | "info" | "warn" | "error";
  message: string;

  constructor(severity: 0 | 1 | 2 | 3, message: string) {
    this.severity = severity;
    this.message = message;
    this.severityString = "info";
    this.log();
  }

  log() {
    switch (this.severity) {
      case 0:
        this.severityString = "debug";
        console.debug(this.message);
        break;
      case 1:
        this.severityString = "info";
        console.log(this.message);
        break;
      case 2:
        this.severityString = "warn";
        console.warn(this.message);
        break;
      case 3:
        this.severityString = "error";
        console.error(this.message);
        break;
    }
  }
}

export enum ProductSubType {
  flatwork = "flatwork",
  booklet = "booklet",
  brochure = "brochure",
  book = "book",
  label = "label",
  cover = "cover",
  multipartbooklet = "multipartbooklet",
}

export enum ProductType {
  flat = "flat",
  booklet = "booklet",
  multipartbooklet = "multipartbooklet",
  book = "book",
  brochure = "brochure",
}

export interface IParameter {
  parts: IPart[];
  productType?: string;
  ignoreSequenceForFlat?: boolean;
  numVariants?: number;
  inputFolderName?: string;
}

export interface IPart {
  type: PART_TYPES;
  pages: number;
  width?: number;
  height?: number;
  spreadType?: "Spread" | "SinglePage";
}
