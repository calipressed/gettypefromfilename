import { File } from "./file";
import { IFile, IMessage, IParameter, IPart, Message, ProductSubType, ProductType, SUB_TYPE } from "./interfaces";
import { Part } from "./part";
import { FILE_TYPES, FILTER_FILES, PART_TYPES } from "./types";

export interface IResults {
  pageRangeBody: string;
  pageRangeCover: string;
  pageRangeSpine: string;
  severity: 1 | 2 | 3;
  splitCoverForMergeWithBody: boolean;
  splitCoverFromSingleFile: boolean;
  spineIsLastPage: boolean;
  imposeSinglePages: boolean;
  hasSidecarFiles: boolean;
  duplicatePageWithVariants: null | "isFirstPage" | "isLastPage";
  lastErrorMessage?: string;
  lastErrorCode?: number;
  lastErrorId?: string;
}

export interface IProductParameter {
  needsAdditionalPart: boolean;
  neededCoverPages: number;
  neededBodyPages: number;
  neededAdditionalPartPages: number;
  allNeededPages: number;
  additionalPartType: string;
  parts: Part[];
  numVariants: number;
  ignoreNumbersAbove: number;
  ignoreSequenceForFlat: boolean;
  mode: "common" | "uploadPortal";
}

export class Product {
  files: File[];
  singleFiles: File[];
  sidecarFiles: File[];
  results: IResults;
  parameter: IProductParameter;
  uniqueIDs: string[];
  type: ProductType;
  messages: IMessage[];

  constructor(files: IFile[], parameter: IParameter, config: any) {
    this.results = {
      severity: 1,
      splitCoverForMergeWithBody: false,
      splitCoverFromSingleFile: false,
      spineIsLastPage: false,
      hasSidecarFiles: false,
      duplicatePageWithVariants: null,
      imposeSinglePages: false,
      pageRangeBody: "",
      pageRangeCover: "",
      pageRangeSpine: "",
      lastErrorMessage: undefined,
      lastErrorCode: undefined,
      lastErrorId: undefined,
    };
    this.parameter = {
      needsAdditionalPart: false,
      neededCoverPages: 0,
      neededBodyPages: 0,
      neededAdditionalPartPages: 0,
      allNeededPages: 0,
      additionalPartType: "",
      parts: [],
      numVariants: parameter.numVariants || 1,
      ignoreNumbersAbove: config.ignoreNumbersAbove ?? -1,
      ignoreSequenceForFlat: parameter.ignoreSequenceForFlat ?? false,
      mode: config.mode ?? "common",
    };
    this.messages = [];
    this.files = [];
    this.singleFiles = [];
    this.uniqueIDs = [];
    this.sidecarFiles = [];
    this.init(files, parameter.parts, config);
    this.type = this.getProductType(parameter.productType);
  }

  private getProductType(type?: string): ProductType {
    if (this.parameter.parts.length === 1) {
      if (type) {
        switch (type.toLowerCase()) {
          case ProductSubType.booklet:
            return ProductType.booklet;
          case ProductSubType.flatwork:
          case ProductSubType.label:
            return ProductType.flat;
          case ProductSubType.cover:
          case ProductSubType.brochure:
            return ProductType.brochure;
        }
      }

      if (this.parameter.numVariants === 1 && this.parameter.allNeededPages > 2) {
        this.addMessage(1, `Assumed that product type is booklet.`);
        return ProductType.booklet;
      } else {
        this.addMessage(1, `Assumed that product type is flatwork.`);
        return ProductType.flat;
      }
    } else {
      if (type) {
        switch (type.toLowerCase()) {
          case ProductSubType.booklet:
          case ProductSubType.multipartbooklet:
            return ProductType.multipartbooklet;
          case ProductSubType.book:
            return ProductType.book;
        }
      }
      this.addMessage(1, `Assumed that product type is book.`);
      return ProductType.book;
    }
  }

  private init(files: IFile[], parts: IPart[], config: any) {
    this.createInstancesOfFiles(files, config);
    this.createInstancesOfParts(parts);
    this.setIdIfNeeded();
    this.checkForCoverAndAdditionalParts();
  }

  private createInstancesOfFiles(files: IFile[], config: any) {
    if (this.parameter.mode === "uploadPortal") {
      const filesWithUniqueId: File[] = [];
      const filesWithoutUniqueId: File[] = [];

      for (const file of files) {
        const newFile = new File(file, config);
        if (newFile.uniqueId) {
          filesWithUniqueId.push(newFile);
        } else {
          filesWithoutUniqueId.push(newFile);
        }
      }

      this.uniqueIDs.push(...new Set(filesWithUniqueId.filter((file) => typeof file.uniqueId !== "undefined").map((file) => file.uniqueId) as string[]));

      for (const id of this.uniqueIDs) {
        const filesWithSameId = filesWithUniqueId.filter((file: File) => file.uniqueId === id);
        const originalFile = filesWithSameId[0];

        if (originalFile) {
          const pages = [];
          let numPages = 0;
          for (const file of filesWithSameId) {
            pages.push(...file.pages);
            numPages += file.numPages;
          }
          this.files.push(new File({ ...originalFile, name: originalFile.oriName, pages: pages, numPages: numPages }, config));
        }
      }

      for (const file of filesWithoutUniqueId) {
        this.files.push(file);
      }

      this.singleFiles.push(...filesWithUniqueId, ...filesWithoutUniqueId);
    } else {
      for (const _f in files) {
        const file = files[_f];
        this.files.push(new File(file, config));
      }
    }
  }

  private createInstancesOfParts(parts: IPart[]) {
    for (const part of parts) {
      this.parameter.parts.push(new Part(part));
    }
  }

  private setIdIfNeeded() {
    for (const _f in this.files) {
      const part = this.files[_f];
      if (!part.id) {
        part.id = `${+_f + 1}`;
      }
    }
    if (this.parameter.mode === "uploadPortal") {
      for (const _f in this.singleFiles) {
        const part = this.singleFiles[_f];
        if (!part.id) {
          part.id = `${+_f + 1}`;
        }
      }
    }
  }

  private checkForCoverAndAdditionalParts() {
    for (const part of this.parameter.parts) {
      const partType = part.type.toLowerCase();
      if (partType === PART_TYPES.cover) {
        this.parameter.neededCoverPages = part.pages;
      } else if (partType === PART_TYPES.body) {
        this.parameter.neededBodyPages = part.pages;
      } else {
        this.parameter.needsAdditionalPart = true;
        this.parameter.additionalPartType = part.type;
        this.parameter.neededAdditionalPartPages = part.pages;
      }
    }
    this.parameter.allNeededPages = this.parameter.neededBodyPages + this.parameter.neededCoverPages + this.parameter.neededAdditionalPartPages;
  }

  moveSidecarFiles(regexProductionFiles: string, regexSidecarFiles?: string) {
    const removeFileIndexes: number[] = [];
    for (let _f = 0; _f < this.files.length; _f++) {
      const file = this.files[_f];
      const matchProd = file.getName(true)?.match(new RegExp(regexProductionFiles));
      const matchSidecar = regexSidecarFiles ? file.getName(true)?.match(new RegExp(regexSidecarFiles)) : undefined;

      if (!matchProd) {
        if (this.parameter.mode === "uploadPortal") {
          const singleFiles = this.singleFiles.filter((_file) => _file.getName() === file.getName());
          for (const singleFile of singleFiles) {
            singleFile.type = FILE_TYPES.sidecar;
            singleFile.sequence = singleFile.subSequence ?? 0;
          }
          if (singleFiles.length > 0) {
            this.results.hasSidecarFiles = true;
            removeFileIndexes.push(_f);
          }
        } else {
          this.results.hasSidecarFiles = true;
          this.sidecarFiles.push(file);
          removeFileIndexes.push(_f);
        }
      } else if (matchSidecar) {
        if (this.parameter.mode === "uploadPortal") {
          const singleFiles = this.singleFiles.filter((_file) => _file.getName() === file.getName());
          for (const singleFile of singleFiles) {
            singleFile.type = FILE_TYPES.sidecar;
            singleFile.sequence = singleFile.subSequence ?? 0;
          }
          if (singleFiles.length > 0) {
            this.results.hasSidecarFiles = true;
            removeFileIndexes.push(_f);
          }
        } else {
          this.results.hasSidecarFiles = true;
          this.sidecarFiles.push(file);
          removeFileIndexes.push(_f);
        }
      }
    }
    for (let i = removeFileIndexes.length - 1; i >= 0; i--) {
      this.files.splice(removeFileIndexes[i], 1);
    }
  }

  getProductionFiles(filter?: FILTER_FILES) {
    const files = this.files;
    if (filter) {
      switch (filter) {
        case FILTER_FILES.isUnassigned:
          return files.filter((file) => !file.isAssigned());
        case FILTER_FILES.isAssigned:
          return files.filter((file) => file.isAssigned());
        case FILTER_FILES.isBody:
          return files.filter((file) => file.isBody());
        case FILTER_FILES.isNotBody:
          return files.filter((file) => !file.isBody());
        case FILTER_FILES.isCover:
          return files.filter((file) => file.isCover());
        case FILTER_FILES.isNotCover:
          return files.filter((file) => !file.isCover());
        case FILTER_FILES.hasSequence:
          return files.filter((file) => file.hasSequence());
        case FILTER_FILES.hasNoSequence:
          return files.filter((file) => !file.hasSequence());
        case FILTER_FILES.hasPdfProperties:
          return files.filter((file) => file.hasPdfProperties());
        case FILTER_FILES.hasNoPdfProperties:
          return files.filter((file) => !file.hasPdfProperties());
        case FILTER_FILES.isWildcard:
          return files.filter((file) => !file.isBody() && !file.isCover() && file.isAssigned());
      }
    }
    return files;
  }

  getLastFile(fileType?: FILE_TYPES) {
    const files = fileType ? this.getProductionFiles(FILTER_FILES.hasSequence).filter((file) => file.type === fileType) : this.getProductionFiles(FILTER_FILES.hasSequence);
    const highestSequence = Math.max(...files.map((file) => file.sequence ?? 0));

    return files.find((file) => file.sequence === highestSequence);
  }

  getFirstFile(fileType?: FILE_TYPES) {
    const files = fileType ? this.getProductionFiles(FILTER_FILES.hasSequence).filter((file) => file.type === fileType) : this.getProductionFiles(FILTER_FILES.hasSequence);
    const lowestSequence = Math.min(...files.map((file) => file.sequence ?? 0));

    return files.find((file) => file.sequence === lowestSequence);
  }

  getAmountOfUnassignedFiles() {
    return this.getProductionFiles(FILTER_FILES.isUnassigned).length;
  }

  getAmountOfFilesWithoutSequence(fileType?: FILE_TYPES) {
    const files = this.getProductionFiles(FILTER_FILES.hasNoSequence);
    if (fileType) return files.filter((file) => file.type === fileType).length;
    return files.length;
  }

  assignAllUnassignedFilesToBody() {
    const files = this.getProductionFiles(FILTER_FILES.isUnassigned);
    for (const file of files) {
      file.type = FILE_TYPES.body;
    }
  }

  assignAllUnassignedFilesToCover() {
    const files = this.getProductionFiles(FILTER_FILES.isUnassigned);
    for (const file of files) {
      file.type = FILE_TYPES.cover;
    }
  }

  getAmountOfParts() {
    return this.parameter.parts.length;
  }

  hasFilesWithoutSequence() {
    if (this.getAmountOfFilesWithoutSequence() > 0) return true;
    return false;
  }

  hasUnassignedFiles() {
    if (this.getAmountOfUnassignedFiles() > 0) return true;
    return false;
  }

  getAdditionalPartType() {
    return this.parameter.parts.find((part) => part.type !== PART_TYPES.body && part.type !== PART_TYPES.cover)?.type;
  }

  sortFilesBy(key: keyof File, log = true) {
    if (log) this.addMessage(1, `Files are sorted by ${key}.`);
    const coverFiles = this.getProductionFiles(FILTER_FILES.isCover);
    const bodyFiles = this.getProductionFiles(FILTER_FILES.isBody);
    const otherFIles = this.getProductionFiles(FILTER_FILES.isWildcard);
    const unassignedFiles = this.getProductionFiles(FILTER_FILES.isUnassigned);

    // @ts-ignore
    const filteredCoverFiles = coverFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));

    // @ts-ignore
    const filteredBodyFiles = bodyFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));
    // @ts-ignore
    const filteredOtherFiles = otherFIles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));

    // @ts-ignore
    const filteredUnassignedFiles = unassignedFiles.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));

    this.files = [...filteredCoverFiles, ...filteredBodyFiles, ...filteredOtherFiles, ...filteredUnassignedFiles];
  }

  setSequenceFromId(filter?: FILTER_FILES) {
    let files;
    if (filter) {
      files = this.getProductionFiles(filter);
    } else {
      files = this.getProductionFiles();
    }

    const key = "id";

    const sortedFiles = files.sort((a, b) => (parseFloat(a[key]) > parseFloat(b[key]) ? 1 : parseFloat(b[key]) > parseFloat(a[key]) ? -1 : 0));

    for (const _f in sortedFiles) {
      const file = sortedFiles[_f];
      file.sequence = parseInt(_f) + 1;
    }
  }

  addMessage(severity: 0 | 1 | 2 | 3, message: string, setLastError = true) {
    if (severity > this.results.severity && severity !== 0) {
      this.results.severity = severity;
    }

    if (setLastError && severity >= this.results.severity) {
      if (severity === 1) {
        this.results.lastErrorMessage = message;
        this.results.lastErrorId = "setOrderInfo";
        this.results.lastErrorCode = -123;
      } else if (severity === 2) {
        this.results.lastErrorMessage = message;
        this.results.lastErrorId = "setOrderWarning";
        this.results.lastErrorCode = -124;
      } else if (severity === 3) {
        this.results.lastErrorMessage = message;
        this.results.lastErrorId = "setOrderError";
        this.results.lastErrorCode = -125;
      }
    }

    const _message = new Message(severity, message);
    this.messages.push(_message);
  }

  getPart(type: PART_TYPES) {
    // If part type is wild card return part that is not cover or body
    if (type === PART_TYPES.wildcard) {
      return this.parameter.parts.find((part) => part.type !== PART_TYPES.body && part.type !== PART_TYPES.cover);
    }
    return this.parameter.parts.find((part) => part.type === type);
  }

  getActualPages(filter?: FILTER_FILES) {
    let files = this.getProductionFiles();
    if (filter) files = this.getProductionFiles(filter);
    return files.reduce((acc, file) => acc + (file.numPages || 0), 0);
  }

  getAllWidths() {
    const files = this.getProductionFiles();
    const widths: number[] = [];

    for (const file of files) {
      const width = file.getWidth();

      const TOLERANCE = 3;
      const LOW = width - TOLERANCE;
      const HIGH = width + TOLERANCE;

      let widthExists = false;
      for (const width of widths) {
        if (width > LOW && width < HIGH) widthExists = true;
      }

      if (!widthExists) {
        // If no width is found add it to widths
        widths.push(width);
      }
    }

    return widths;
  }

  addFinalMessages() {
    const allProductionFiles = this.files;
    const allSidecarFiles = this.sidecarFiles;

    if (allSidecarFiles.length > 0) {
      this.addMessage(1, `The following files were determined as sidecar files:\n  ${allSidecarFiles.map((file) => `fileName: ${file.oriName}`).join("\n  ")}`, false);
    }

    const filesWithoutTypeOrSequence = allProductionFiles.filter((file) => typeof file.type === "undefined" || typeof file.sequence === "undefined");

    if (this.results.severity !== 3)
      this.addMessage(
        1,
        `The product part type and the order of all files were set as follows:\n  ${allProductionFiles
          .map((file) => `file name: ${file.oriName}, type: ${file.type}, sequence: ${file.sequence}`)
          .join("\n  ")}`,
        false,
      );

    if (this.results.severity !== 3 && filesWithoutTypeOrSequence.length === 0) {
      this.addMessage(1, `The order of all files was successfully determined.`);
    } else if (this.results.severity !== 3) {
      this.addMessage(3, `The order of files could not be determined.`);
    }
  }

  updateFiles() {
    if (this.parameter.mode === "uploadPortal") {
      // make sure the files are sorted
      this.sortFilesBy("sequence");

      let startSeq = 0;

      for (const uniqueId of this.uniqueIDs) {
        const singleFilesWithSameId = this.singleFiles.filter((file) => file.uniqueId === uniqueId);
        const originalFile = this.files.find((file) => file.uniqueId === uniqueId);

        if (originalFile) {
          for (let i = 1; i < singleFilesWithSameId.length + 1; i++) {
            const singleFile = singleFilesWithSameId[i - 1];
            singleFile.type = originalFile.type;

            if (typeof originalFile.sequence !== "undefined" && typeof singleFile.subSequence !== "object") {
              singleFile.sequence = startSeq + singleFile.subSequence;
            }
          }
        }

        startSeq = startSeq + singleFilesWithSameId.length;
      }
      this.normalizeAllFiles(this.singleFiles);
      this.files = this.singleFiles.filter((file) => file.type !== FILE_TYPES.sidecar);
      this.sidecarFiles = this.singleFiles.filter((file) => file.type === FILE_TYPES.sidecar);
      this.sortFilesBy("sequence");
    } else {
      this.normalizeAllFiles(this.files);
      this.addMessage(1, `Updating sequence of files for further processing.`);
    }
  }

  normalizeAllFiles(files: IFile[]) {
    this.normalizeSequence(files, FILE_TYPES.body);
    this.normalizeSequence(files, FILE_TYPES.cover);
    this.normalizeSequence(files, FILE_TYPES.wildcard);
  }

  normalizeSequence(files: IFile[], type?: FILE_TYPES) {
    const filesOfSameType = files.filter((file) => file.type === type).sort((a, b) => ((a.sequence ?? 0) > (b.sequence ?? 0) ? 1 : (b.sequence ?? 0) > (a.sequence ?? 0) ? -1 : 0));

    const start = 2;
    for (let i = 0; i < filesOfSameType.length; i++) {
      const file = filesOfSameType[i];

      if (typeof file.sequence !== "undefined") {
        file.sequence = start + i;
      }
    }
  }

  setPageRangeFromFile(id: string) {
    const file = this.files.find((file) => file.id === id);
    if (file) {
      this.results.pageRangeSpine = file.pages
        .filter((page) => page.subType === SUB_TYPE.spine)
        .map((page) => page.page)
        .join(",");

      this.results.pageRangeCover = file.pages
        .filter((page) => page.type === FILE_TYPES.cover && page.subType !== SUB_TYPE.spine)
        .map((page) => page.page)
        .join(",");

      this.results.pageRangeBody = file.pages
        .filter((page) => page.type === FILE_TYPES.body)
        .map((page) => page.page)
        .join(",");
    }
  }

  toJSON() {
    const json = {
      files: this.files,
      sidecarFiles: this.sidecarFiles,
      parts: this.parameter.parts,
      results: this.results,
    };
    return json;
  }
}
