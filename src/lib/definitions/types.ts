export enum FILE_TYPES {
  cover = "cover",
  body = "body",
  sidecar = "sidecar",
  wildcard = "wildcard",
  product = "product",
}

export enum FILTER_FILES {
  isUnassigned = "isUnassigned",
  isBody = "isBody",
  isNotBody = "isNotBody",
  isCover = "isCover",
  isNotCover = "isNotCover",
  isAssigned = "isAssigned",
  hasSequence = "hasSequence",
  hasNoSequence = "hasNoSequence",
  hasPdfProperties = "hasPdfProperties",
  hasNoPdfProperties = "hasNoPdfProperties",
  isWildcard = "isNotBodyOrCover",
}

export enum PART_TYPES {
  cover = "cover",
  body = "body",
  jacket = "jacket",
  wildcard = "wildcard",
}

export enum PRODUCT_TYPES {
  FlatWork = "FlatWork",
  Brochure = "Brochure",
  Booklet = "Booklet",
  MultipartBooklet = "MultipartBooklet",
  Book = "Book",
  Cover = "Cover",
  Body = "Body",
}
