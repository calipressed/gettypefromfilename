import path from "path";
import { IFile, SUB_TYPE, IPage } from "./interfaces";
import { FILE_TYPES, PART_TYPES } from "./types";
import { isWidthHalfOrLessSinglePage, isWidthDoubleOrMoreThanSinglePage } from "../methods/type";
import { truncate } from "fs-extra";

export class File implements IFile {
  id: string;
  filePath?: string;
  type?: FILE_TYPES;
  subType: SUB_TYPE;
  sequence?: number;
  numPages: number;
  pages: IPage[];
  name: string;
  extension: string;
  oriName: string;
  nameWithoutRegexMatch?: string;
  removeFromFileRegex?: string;
  maskedOffMatches: any[];
  subSequence: number | null;
  loggingName: string;
  uniqueId?: string;
  pageRangeCover?: string;
  pageRangeSpine?: string;
  pageRangeBody?: string;

  constructor(params: IFile, config: any) {
    this.id = params?.id ?? "";
    this.filePath = params?.filePath;
    this.type = params?.type?.toLowerCase() as FILE_TYPES;
    this.sequence = params?.sequence;
    this.pages = params?.pages ?? [];
    this.numPages = params?.numPages ?? this.pages.length;
    this.maskedOffMatches = [];
    this.oriName = params?.name ?? path.basename(params?.filePath || "");
    this.uniqueId = this.getUniqueId(this.oriName, config);
    this.subSequence = this.getSubSequence(params?.name, config);
    this.name = this.initName(this.oriName, config);
    this.loggingName = this.getLoggingName(config?.regex?.removeFromFileNameForLogging);
    this.extension = path.parse(this.oriName || "").ext.toLowerCase();
    this.subType = SUB_TYPE.none;
  }

  getName(includeExtension = false) {
    if (includeExtension) return `${this.name}${this.extension}`;
    return this.name;
  }

  getLoggingName(removeRegex?: string) {
    let name = this.oriName;
    if (removeRegex) {
      const regex = new RegExp(removeRegex, "g");
      const matches = name.matchAll(regex);

      if (matches) {
        for (const match of matches) {
          name = name.replace(match[0], "");
        }
      }
    }
    return name;
  }

  initName(oriName: string, config: any) {
    let name = path.parse(oriName || "").name.toLowerCase();

    if (this.uniqueId && config?.upl?.removeFromSplitFiles) {
      const regex = new RegExp(config.upl.removeFromSplitFiles, "gi");
      const matches = name.matchAll(regex);

      if (matches) {
        for (const match of matches) {
          this.maskedOffMatches.push({
            match: match[0],
            index: match.index,
            length: match[0].length,
          });
          name = name.replace(match[0], "");
        }
      }
    }

    if (config?.regex?.removeFromFileName) {
      const regex = new RegExp(config?.regex.removeFromFileName, "gi");
      const matches = name.matchAll(regex);

      if (matches) {
        for (const match of matches) {
          this.maskedOffMatches.push({
            match: match[0],
            index: match.index,
            length: match[0].length,
          });
          name = name.replace(match[0], "");
        }
      }
    }

    return name;
  }

  getUniqueId(oriName: string, config: any) {
    const name = path.parse(oriName || "").name.toLocaleLowerCase();
    if (config?.mode === "uploadPortal" && config?.upl?.uniqueIdOfSplitFiles) {
      const regex = new RegExp(config.upl.uniqueIdOfSplitFiles, "gi");
      const match = name.match(regex);
      if (match) {
        return match[0];
      }
    }
    return undefined;
  }

  getSubSequence(oriName: string, config: any) {
    const name = path.parse(oriName || "").name.toLocaleLowerCase();
    if (this.uniqueId && (config?.regex?.subSequence || config?.upl?.subSequence)) {
      const regex = new RegExp(config.regex.subSequence || config?.upl?.subSequence, "gi");
      const match = name.match(regex);
      if (match) {
        return parseInt(match[0]);
      }
    }
    return null;
  }

  isBody() {
    if (this.type === FILE_TYPES.body) {
      return true;
    }
    return false;
  }

  isCover() {
    if (this.type === FILE_TYPES.cover) {
      return true;
    }
    return false;
  }

  isAssigned() {
    if (this.type && Object.keys(FILE_TYPES).includes(this.type)) {
      return true;
    }
    return false;
  }

  hasSequence() {
    if (typeof this.sequence !== "undefined") return true;
    return false;
  }

  hasPdfProperties() {
    if (!this.numPages) return false;
    if (!this.getWidth() || !this.getHeight()) return false;
    return true;
  }

  getNameHonoringSequence() {
    if (typeof this.sequence !== "undefined") {
      return setNumberWithZerosInFront(this.sequence, 4) + "_" + this.oriName;
    } else {
      return this.oriName;
    }
  }

  setWidth(value: number, pageRange?: number[]) {
    for (const _p in this.pages) {
      const page = this.pages[_p];
      const pageNum = parseFloat(_p) + 1;

      if (typeof pageRange === "undefined" || pageRange?.includes(pageNum)) {
        page.width = value;
      }
    }
  }

  setHeight(value: number, pageRange?: number[]) {
    for (const _p in this.pages) {
      const page = this.pages[_p];
      const pageNum = parseFloat(_p) + 1;

      if (typeof pageRange === "undefined" || pageRange?.includes(pageNum)) {
        page.height = value;
      }
    }
  }

  getAllWidths() {
    const widths: number[] = [];

    for (const page of this.pages) {
      const width = page.width;

      const TOLERANCE = 1;
      const LOW = width - TOLERANCE;
      const HIGH = width + TOLERANCE;

      let widthExists = false;
      for (const width of widths) {
        if (width > LOW && width < HIGH) {
          widthExists = true;
        }
      }

      if (!widthExists) {
        // If no width is found add it to widths
        widths.push(width);
      }
    }

    return widths;
  }

  setPageTypeFromSize(bodyWidth?: number, coverPages = 0, isSpread = false) {
    let singlePageWidth = 0;

    if (bodyWidth) {
      singlePageWidth = Math.round(this.pages.find((page) => isEqual(page.width, bodyWidth))?.width || 0);
    } else {
      singlePageWidth = mode(this.pages.map((page) => page.width));
    }

    const spreadWidth = this.pages.find((page) => page.width >= singlePageWidth * 2)?.width;
    const spineWidth = this.pages.find((page) => page.width <= singlePageWidth / 2)?.width;

    console.log(singlePageWidth, spreadWidth, spineWidth);

    for (const page of this.pages) {
      if (isEqual(page.width, singlePageWidth)) {
        page.type = FILE_TYPES.body;
      } else if (spreadWidth && isEqual(page.width, spreadWidth)) {
        page.type = FILE_TYPES.cover;
      } else if (spineWidth && isEqual(page.width, spineWidth)) {
        (page.type = FILE_TYPES.cover), (page.subType = SUB_TYPE.spine);
      }
    }

    const coverPagesArr = this.pages.filter((page) => page.type === FILE_TYPES.cover && page.subType !== SUB_TYPE.spine);
    const bodyPagesArr = this.pages.filter((page) => page.type === FILE_TYPES.body);

    if ((coverPages === 2 || (coverPages === 1 && isSpread)) && coverPagesArr.length === 0) {
      bodyPagesArr.at(0)!.type = FILE_TYPES.cover;
      bodyPagesArr.at(-1)!.type = FILE_TYPES.cover;
      bodyPagesArr.at(0)!.subType = SUB_TYPE.coverFrontOutside;
      bodyPagesArr.at(-1)!.subType = SUB_TYPE.coverBackOutside;
    } else if (coverPages === 4 || (coverPages === 2 && isSpread)) {
      if (coverPagesArr.length === 0 && bodyPagesArr.length >= 4) {
        bodyPagesArr.at(1)!.type = FILE_TYPES.cover;
        bodyPagesArr.at(-2)!.type = FILE_TYPES.cover;
        bodyPagesArr.at(1)!.subType = SUB_TYPE.coverFrontInside;
        bodyPagesArr.at(-2)!.subType = SUB_TYPE.coverBackInside;
      }
      if (coverPagesArr.length <= 1 && bodyPagesArr.length >= 2) {
        bodyPagesArr.at(0)!.type = FILE_TYPES.cover;
        bodyPagesArr.at(-1)!.type = FILE_TYPES.cover;
        bodyPagesArr.at(0)!.subType = SUB_TYPE.coverFrontOutside;
        bodyPagesArr.at(-1)!.subType = SUB_TYPE.coverBackOutside;
      }
    }

    console.log(this.pages);

    function mode(arr: any[]) {
      if (arr.length === 2) {
        return Math.min(...arr);
      }
      return arr.sort((a, b) => arr.filter((v) => v === a).length - arr.filter((v) => v === b).length).pop();
    }

    function isEqual(x: number, y: number, tolerance = 3) {
      return x - tolerance < y && y < x + tolerance;
    }
  }

  setPageTypeFromSizeCoverSinglePage(coverPages = 0) {
    // cover delivered as single page

    const allWidths = this.getAllWidths();

    if (allWidths.length > 1) {
      // contains spine?
      const singlePageWidth = Math.max(...allWidths);

      for (const page of this.pages) {
        if (page.width < singlePageWidth / 2) {
          // only check if page is less than half of single page due to different single page sizes
          page.type = FILE_TYPES.cover;
          page.subType = SUB_TYPE.spine;
        } else {
          page.type = FILE_TYPES.cover;
        }
      }
    }

    const coverPagesArr = this.pages.filter((page) => page.type === FILE_TYPES.cover && page.subType !== SUB_TYPE.spine);
    const isSpread = coverPagesArr.length >= coverPages * 2;

    if (coverPages === 2 || (coverPages === 1 && isSpread)) {
      coverPagesArr.at(0)!.type = FILE_TYPES.cover;
      coverPagesArr.at(-1)!.type = FILE_TYPES.cover;
      coverPagesArr.at(0)!.subType = SUB_TYPE.coverFrontOutside;
      coverPagesArr.at(-1)!.subType = SUB_TYPE.coverBackOutside;
    } else if (coverPages === 4 || (coverPages === 2 && isSpread)) {
      coverPagesArr.at(1)!.type = FILE_TYPES.cover;
      coverPagesArr.at(-2)!.type = FILE_TYPES.cover;
      coverPagesArr.at(1)!.subType = SUB_TYPE.coverFrontInside;
      coverPagesArr.at(-2)!.subType = SUB_TYPE.coverBackInside;
      coverPagesArr.at(0)!.type = FILE_TYPES.cover;
      coverPagesArr.at(-1)!.type = FILE_TYPES.cover;
      coverPagesArr.at(0)!.subType = SUB_TYPE.coverFrontOutside;
      coverPagesArr.at(-1)!.subType = SUB_TYPE.coverBackOutside;
    }
  }

  getWidth(_page = 1) {
    const width = this.pages.find((page) => page.page === _page)?.width;
    if (width) return width;
    return 0;
  }

  getHeight(_page = 1) {
    const height = this.pages.find((page) => page.page === _page)?.height;
    if (height) return height;
    return 0;
  }

  isPageSizeEqual(tolerance = 1) {
    let isEqual = true;
    const width = this.getWidth();
    const height = this.getHeight();

    for (const page of this.pages) {
      if (width - tolerance <= page.width && page.width >= width + tolerance) {
        isEqual = false;
      }
      if (height - tolerance <= page.height && page.height >= height + tolerance) {
        isEqual = false;
      }
    }
    return isEqual;
  }

  addPage(page: IPage) {
    this.pages.push(page);
  }

  isSpread(singlePageWidth?: number) {
    if (!singlePageWidth || !this.getWidth()) return true;
    if (isWidthDoubleOrMoreThanSinglePage(this.getWidth(), singlePageWidth)) {
      return true;
    }
    return false;
  }

  setPageRangeFromFile() {
    this.pageRangeSpine = this.pages
      .filter((page) => page.subType === SUB_TYPE.spine)
      .map((page) => page.page)
      .join(",");

    this.pageRangeCover = this.pages
      .filter((page) => page.type === FILE_TYPES.cover && page.subType !== SUB_TYPE.spine)
      .map((page) => page.page)
      .join(",");

    this.pageRangeBody = this.pages
      .filter((page) => page.type === FILE_TYPES.body)
      .map((page) => page.page)
      .join(",");
  }
}

function setNumberWithZerosInFront(num: number, size: number): string {
  const s = "000000000" + num;
  return s.substring(s.length - size);
}
