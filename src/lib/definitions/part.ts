import { IPart } from "./interfaces";
import { PART_TYPES } from "./types";

export class Part implements IPart {
  type: PART_TYPES;
  pages: number;
  width?: number;
  height?: number;

  constructor(params: IPart) {
    this.type = params.type.toLowerCase() as PART_TYPES;
    this.pages = params.pages;
    this.width = params.width;
    this.height = params.height;
  }
}
