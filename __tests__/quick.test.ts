import inputTest from "./resources/input_test.json";
import config from "../src/resources/config.json";
import { getProduct, main } from "../src";
import { IParameter } from "../src/lib/definitions/interfaces";

const configPCUSA = { ...config };

configPCUSA.regex.removeFromFileName = "\\d{4,}$";
configPCUSA.regex.removeFromFileNameForLogging = "";
configPCUSA.regex.productionFiles = "(.*.pdf)|(.*.png)|(.*.jpg)|(.*.jpeg)|(.*.tiff)|(.*.tif)|(.*.gif)|(.*.psd)|(.*.psb)|(.*.ai)";
configPCUSA.regex.sidecarFiles = "jobticket";
configPCUSA.regex.ignorePageCountForFiles = "(.*.psd)";
// @ts-ignore
configPCUSA.regex.spineFiles = ["spine"];
// @ts-ignore
configPCUSA.regex.additionalParts = ["jacket"];

describe("Quicktest", () => {
  test("Should have all sequences and product part types", () => {
    const result = getProduct(inputTest["3_files_cover_body_spine"].files, inputTest["3_files_cover_body_spine"].parameter as IParameter, configPCUSA);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("Should not throw an error", () => {
    const result = main(inputTest["subType_error"].files, inputTest["subType_error"].parameter as IParameter, configPCUSA);
    expect(result).toMatchSnapshot();
  });

  test("cover_spread", () => {
    const result = main(inputTest["cover_spread"].files as any, inputTest["cover_spread"].parameter as IParameter, configPCUSA);
    expect(result).toMatchSnapshot();
  });

  test("front_back", () => {
    const result = main(inputTest["front_back"].files as any, inputTest["front_back"].parameter as IParameter, config);
    expect(result).toMatchSnapshot();
  });
});
