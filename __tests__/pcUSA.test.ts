import inputProd from "./resources/input_production_examples.json";
import configProd from "../src/resources/config.json";
import { main } from "../src";
import { IParameter } from "../src/lib/definitions/interfaces";

configProd.regex.removeFromFileName = "\\d{4,}$";
configProd.regex.productionFiles = "(.*.pdf)|(.*.png)|(.*.jpg)|(.*.jpeg)|(.*.tiff)|(.*.tif)|(.*.gif)|(.*.psd)|(.*.psb)|(.*.ai)";
configProd.regex.sidecarFiles = "jobticket";
configProd.regex.ignorePageCountForFiles = "(.*.psd)";
// @ts-ignore
configProd.regex.spineFiles = ["spine"];
// @ts-ignore
configProd.regex.additionalParts = ["jacket"];
configProd.regex.sequence.pairs.push(
  ...[
    {
      front: "image",
      back: "grid",
    },
    {
      front: "top",
      back: "bottom",
    },
  ],
);

describe("Multipart => ", () => {
  test("3_files names are numbers, body and cover", () => {
    const result = main(inputProd["3_files_numbers_cover_body"].files, inputProd["3_files_numbers_cover_body"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("16_files names are numbers, body and cover", () => {
    const result = main(inputProd["16_files_cover_body"].files, inputProd["16_files_cover_body"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("8_files", () => {
    const result = main(inputProd["8_files"].files, inputProd["8_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("8_files_cover_as_spread", () => {
    const result = main(inputProd["8_files_cover_as_spread"].files, inputProd["8_files_cover_as_spread"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("8_files_book", () => {
    const result = main(inputProd["8_files_book"].files, inputProd["8_files_book"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("1_file_book", () => {
    const result = main(inputProd["1_file_book"].files, inputProd["1_file_book"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("128_files_sequenceFromName", () => {
    const result = main(inputProd["128_files_sequenceFromName"].files, inputProd["128_files_sequenceFromName"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("2_files_book_with_spine", () => {
    const result = main(inputProd["2_files_book_with_spine"].files, inputProd["2_files_book_with_spine"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("multiple_files_book_with_spine", () => {
    const result = main(inputProd["multiple_files_book_with_spine"].files, inputProd["multiple_files_book_with_spine"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("26_files_sequence_by_month", () => {
    const result = main(inputProd["26_files_sequence_by_month"].files, inputProd["26_files_sequence_by_month"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("14_files_sequence_by_month", () => {
    const result = main(inputProd["14_files_sequence_by_month"].files, inputProd["14_files_sequence_by_month"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("17_files_sequence_by_month", () => {
    // sequence shall not be defined
    const result = main(inputProd["17_files_sequence_by_month"].files, inputProd["17_files_sequence_by_month"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("1_file_book_less_pages", () => {
    // sequence shall not be defined
    const result = main(inputProd["1_file_book_less_pages"].files, inputProd["1_file_book_less_pages"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("2_files_cover_body", () => {
    const result = main(inputProd["2_files_cover_body"].files, inputProd["2_files_cover_body"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("4_files_cover_front_back_spine", () => {
    const result = main(inputProd["4_files_cover_front_back_spine"].files, inputProd["4_files_cover_front_back_spine"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("notebook", () => {
    const result = main(inputProd["notebook"].files, inputProd["notebook"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_image_grid", () => {
    const result = main(inputProd["26_files_calendar_image_grid"].files, inputProd["26_files_calendar_image_grid"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_top_bottom", () => {
    const result = main(inputProd["26_files_calendar_top_bottom"].files, inputProd["26_files_calendar_top_bottom"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});

describe("Flat => ", () => {
  test("6_files_3_variants_flat", () => {
    const result = main(inputProd["6_files_3_variants_flat"].files, inputProd["6_files_3_variants_flat"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("2_files_postcard", () => {
    const result = main(inputProd["2_files_postcard"].files, inputProd["2_files_postcard"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("10_variants_brochure", () => {
    const result = main(inputProd["10_variants_brochure"].files, inputProd["10_variants_brochure"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("10_files_5_variants_flat", () => {
    const result = main(inputProd["10_files_5_variants_flat"].files, inputProd["10_files_5_variants_flat"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("6_files_2_variants_flat_to_many_files", () => {
    const result = main(inputProd["6_files_2_variants_flat_to_many_files"].files, inputProd["6_files_2_variants_flat_to_many_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });
});

describe("Mulitpage", () => {
  test("3_files", () => {
    const result = main(inputProd["3_files"].files, inputProd["3_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("3_files only numbers as names", () => {
    const result = main(inputProd["3_files_numbers"].files, inputProd["3_files_numbers"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("16_files", () => {
    const result = main(inputProd["16_files"].files, inputProd["16_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("16_files_booklet", () => {
    const result = main(inputProd["16_files_booklet"].files, inputProd["16_files_booklet"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("3_files_num_between", () => {
    const result = main(inputProd["3_files_num_between"].files, inputProd["3_files_num_between"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("3_files_no_sequence", () => {
    const result = main(inputProd["3_files_no_sequence"].files, inputProd["3_files_no_sequence"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("12_files_sequence_by_month", () => {
    const result = main(inputProd["12_files_with_cover_files"].files, inputProd["12_files_with_cover_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });
});
