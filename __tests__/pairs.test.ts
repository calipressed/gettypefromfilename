import { main } from "../src";
import inputProd from "./resources/input_pairs.json";
import configProd from "../src/resources/config.json";
import { IParameter } from "../src/lib/definitions/interfaces";

describe("Pairs => ", () => {
  test("3_files_front_back_should_work", () => {
    const result = main(inputProd["3_files_front_back_should_work"].files, inputProd["3_files_front_back_should_work"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files_front_verso_should_work_but_with_warning", () => {
    const result = main(
      inputProd["3_files_front_verso_should_work_but_with_warning"].files,
      inputProd["3_files_front_verso_should_work_but_with_warning"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files_recto_verso_should_work", () => {
    const result = main(inputProd["3_files_recto_verso_should_work"].files, inputProd["3_files_recto_verso_should_work"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_front_back_inside_outside_should_work", () => {
    const result = main(
      inputProd["5_files_front_back_inside_outside_should_work"].files,
      inputProd["5_files_front_back_inside_outside_should_work"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_front_verso_inside_outside_should_work_with_warning", () => {
    const result = main(
      inputProd["5_files_front_verso_inside_outside_should_work_with_warning"].files,
      inputProd["5_files_front_verso_inside_outside_should_work_with_warning"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_front_verso_inside_outside_should_work_with_warning_2", () => {
    const result = main(
      inputProd["5_files_front_verso_inside_outside_should_work_with_warning_2"].files,
      inputProd["5_files_front_verso_inside_outside_should_work_with_warning_2"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_front_verso_inside_outside_should_not_work", () => {
    const result = main(
      inputProd["5_files_front_verso_inside_outside_should_not_work"].files,
      inputProd["5_files_front_verso_inside_outside_should_not_work"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBeGreaterThan(0);
    expect(result).toMatchSnapshot();
  });
});
