import input from "./resources/input_booklet.json";
import config from "../src/resources/config.json";
import { main } from "../src";
import { IParameter } from "../src/lib/definitions/interfaces";

config.regex.removeFromFileName = "\\d{4,}$";

describe("Booklet => ", () => {
  test("12_files_calendar", () => {
    const result = main(input["12_files_calendar"].files, input["12_files_calendar"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("13_files_calendar_with_cover_front_1", () => {
    const result = main(input["13_files_calendar_with_cover_front_1"].files, input["13_files_calendar_with_cover_front_1"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("14_files_calendar_with_cover_front_and_back", () => {
    const result = main(input["14_files_calendar_with_cover_front_and_back"].files, input["14_files_calendar_with_cover_front_and_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_front_and_back_with_cover", () => {
    const result = main(input["26_files_calendar_front_and_back_with_cover"].files, input["26_files_calendar_front_and_back_with_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_front_and_back_with_cover_numbers", () => {
    const result = main(
      input["26_files_calendar_front_and_back_with_cover_numbers"].files,
      input["26_files_calendar_front_and_back_with_cover_numbers"].parameter as IParameter,
      config,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("8_files_with_two_cover_numbers", () => {
    const result = main(input["8_files_with_two_cover_numbers"].files, input["8_files_with_two_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("6_files_with_two_cover_numbers", () => {
    const result = main(input["6_files_with_two_cover_numbers"].files, input["6_files_with_two_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_with_one_cover_numbers", () => {
    const result = main(input["5_files_with_one_cover_numbers"].files, input["5_files_with_one_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("6_files_with_cover_front_back", () => {
    const result = main(input["6_files_with_cover_front_back"].files, input["6_files_with_cover_front_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("6_files", () => {
    const result = main(input["6_files"].files, input["6_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_with_one_cover", () => {
    const result = main(input["2_files_with_one_cover"].files, input["2_files_with_one_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("8_files_with_4_cover_files", () => {
    const result = main(input["8_files_with_4_cover_files"].files, input["8_files_with_4_cover_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("13_files_calendar", () => {
    const result = main(input["13_files_calendar"].files, input["13_files_calendar"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("13_files_calendar_months", () => {
    const result = main(input["13_files_calendar_months"].files, input["13_files_calendar_months"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("13_files_calendar_multipart", () => {
    const result = main(input["13_files_calendar_multipart"].files, input["13_files_calendar_multipart"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("13_files_calendar_months_multipart", () => {
    const result = main(input["13_files_calendar_months_multipart"].files, input["13_files_calendar_months_multipart"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("14_files_calendar_last_page", () => {
    const result = main(input["14_files_calendar_last_page"].files, input["14_files_calendar_last_page"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("36_files_first_and_last_cover", () => {
    const result = main(input["36_files_first_and_last_cover"].files, input["36_files_first_and_last_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("8_files_only_sequence", () => {
    const result = main(input["8_files_only_sequence"].files, input["8_files_only_sequence"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_front_back", () => {
    const result = main(input["2_files_front_back"].files, input["2_files_front_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(2);
    expect(result).toMatchSnapshot();
  });

  test("2_files_body_cover", () => {
    const result = main(input["2_files_body_cover"].files, input["2_files_body_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("8_files", () => {
    const result = main(input["8_files"].files, input["8_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});
