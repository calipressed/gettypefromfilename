import input from "./resources/input_book.json";
import { main } from "../src/index";
import { IParameter } from "../src/lib/definitions/interfaces";
import config from "../src/resources/config.json";

config.regex.removeFromFileName = "\\d{4,}$";
// @ts-ignore
config.regex.additionalParts = ["jacket"];

describe("Book => ", () => {
  test("1_file + sidecar", () => {
    const result = main(input["1_file"].files, input["1_file"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files + sidecar", () => {
    const result = main(input["2_files"].files, input["2_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files + sidecar", () => {
    const result = main(input["3_files"].files, input["3_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files + one sequence missing + sidecar", () => {
    const result = main(input["3_files_one_sequence"].files, input["3_files_one_sequence"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files + sidecar", () => {
    const result = main(input["5_files"].files, input["5_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("10_files + sidecar", () => {
    const result = main(input["10_files"].files, input["10_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files + sidecar", () => {
    const result = main(input["12_files"].files, input["12_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_additionalPart + sidecar", () => {
    const result = main(input["5_files_additionalPart"].files, input["5_files_additionalPart"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files_with_3_parts", () => {
    const result = main(input["3_files_with_3_parts"].files, input["3_files_with_3_parts"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_sequenceFromName", () => {
    const result = main(input["5_files_sequenceFromName"].files, input["5_files_sequenceFromName"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result.results.lastErrorMessage).toMatchSnapshot();
  });

  test("8_files_with_4_cover_files", () => {
    const result = main(input["8_files_with_4_cover_files"].files, input["8_files_with_4_cover_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_2_cover_last", () => {
    const result = main(input["12_files_with_2_cover_last"].files, input["12_files_with_2_cover_last"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_2_cover_first", () => {
    const result = main(input["12_files_with_2_cover_first"].files, input["12_files_with_2_cover_first"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_4_cover_last", () => {
    const result = main(input["12_files_with_4_cover_last"].files, input["12_files_with_4_cover_last"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_4_cover_first", () => {
    const result = main(input["12_files_with_4_cover_first"].files, input["12_files_with_4_cover_first"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_4_cover_first_and_last", () => {
    const result = main(input["12_files_with_4_cover_first_and_last"].files, input["12_files_with_4_cover_first_and_last"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover", () => {
    const result = main(input["1_file_cover"].files, input["1_file_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_2_pages", () => {
    const result = main(input["1_file_cover_2_pages"].files, input["1_file_cover_2_pages"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_2_pages_with_spine", () => {
    const result = main(input["1_file_cover_2_pages_with_spine"].files, input["1_file_cover_2_pages_with_spine"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_with_pages", () => {
    const result = main(input["1_file_cover_with_pages"].files, input["1_file_cover_with_pages"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_2_pages_with_spine_with_pages", () => {
    const result = main(input["1_file_cover_2_pages_with_spine_with_pages"].files, input["1_file_cover_2_pages_with_spine_with_pages"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_single_pages", () => {
    const result = main(input["1_file_cover_single_pages"].files, input["1_file_cover_single_pages"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("1_file_cover_spread_and_single", () => {
    const result = main(input["1_file_cover_spread_and_single"].files, input["1_file_cover_spread_and_single"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});
