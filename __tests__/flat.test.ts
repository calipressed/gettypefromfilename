import input from "./resources/input_flat.json";
import { main } from "../src/index";
import { IParameter } from "../src/lib/definitions/interfaces";
import config from "../src/resources/config.json";

config.regex.removeFromFileName = "\\d{4,}$";

describe("Flat => ", () => {
  test("1_file + sidecar", () => {
    const result = main(input["1_file"].files, input["1_file"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files", () => {
    const result = main(input["2_files"].files, input["2_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_only_one_sequence", () => {
    const result = main(input["2_files_only_one_sequence"].files, input["2_files_only_one_sequence"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_no_sequence", () => {
    const result = main(input["2_files_no_sequence"].files, input["2_files_no_sequence"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files - should have error message", () => {
    const result = main(input["3_files"].files, input["3_files"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(3);
    expect(result.messages).toMatchSnapshot();
  });

  test("2_files_sidecar_via_regex", () => {
    const result = main(input["2_files_sidecar_via_regex"].files, input["2_files_sidecar_via_regex"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("4_files_2_variants_brochure", () => {
    const result = main(input["4_files_2_variants_brochure"].files, input["4_files_2_variants_brochure"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("4_files_4_variants_brochure", () => {
    const result = main(input["4_files_4_variants_brochure"].files, input["4_files_4_variants_brochure"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_4_variants_brochure_front", () => {
    const result = main(input["5_files_4_variants_brochure_front"].files, input["5_files_4_variants_brochure_front"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_4_variants_brochure_back", () => {
    const result = main(input["5_files_4_variants_brochure_back"].files, input["5_files_4_variants_brochure_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_4_variants_brochure_none", () => {
    const result = main(input["5_files_4_variants_brochure_none"].files, input["5_files_4_variants_brochure_none"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(5);
    expect(result).toMatchSnapshot();
  });

  test("5_files_4_variants_brochure_back_and_front", () => {
    const result = main(input["5_files_4_variants_brochure_back_and_front"].files, input["5_files_4_variants_brochure_back_and_front"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_extension_capitalLetters", () => {
    const result = main(input["2_files_extension_capitalLetters"].files, input["2_files_extension_capitalLetters"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_visitengarten", () => {
    const result = main(input["2_files_visitengarten"].files, input["2_files_visitengarten"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_to_be_imposed", () => {
    const result = main(input["2_files_to_be_imposed"].files, input["2_files_to_be_imposed"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_flat", () => {
    const result = main(input["2_files_flat"].files, input["2_files_flat"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});
