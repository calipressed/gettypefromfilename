import input from "./resources/input_multipartbooklet.json";
import config from "../src/resources/config.json";
import { main } from "../src";
import { IParameter } from "../src/lib/definitions/interfaces";

describe("MultipartBooklet => ", () => {
  test("13_files_calendar_with_cover_front_1", () => {
    const result = main(input["13_files_calendar_with_cover_front_1"].files, input["13_files_calendar_with_cover_front_1"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("14_files_calendar_with_cover_front_and_back", () => {
    const result = main(input["14_files_calendar_with_cover_front_and_back"].files, input["14_files_calendar_with_cover_front_and_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_front_and_back_with_cover", () => {
    const result = main(input["26_files_calendar_front_and_back_with_cover"].files, input["26_files_calendar_front_and_back_with_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("26_files_calendar_front_and_back_with_cover_numbers", () => {
    const result = main(
      input["26_files_calendar_front_and_back_with_cover_numbers"].files,
      input["26_files_calendar_front_and_back_with_cover_numbers"].parameter as IParameter,
      config,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("8_files_with_two_cover_numbers", () => {
    const result = main(input["8_files_with_two_cover_numbers"].files, input["8_files_with_two_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("6_files_with_two_cover_numbers", () => {
    const result = main(input["6_files_with_two_cover_numbers"].files, input["6_files_with_two_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("5_files_with_one_cover_numbers", () => {
    const result = main(input["5_files_with_one_cover_numbers"].files, input["5_files_with_one_cover_numbers"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("6_files_with_cover_front_back", () => {
    const result = main(input["6_files_with_cover_front_back"].files, input["6_files_with_cover_front_back"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_assign_body_by_pages_infer_cover", () => {
    const result = main(input["2_files_assign_body_by_pages_infer_cover"].files, input["2_files_assign_body_by_pages_infer_cover"].parameter as IParameter, config);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});
