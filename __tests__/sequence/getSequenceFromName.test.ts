import { IFile, IParameter } from "../../src/lib/definitions/interfaces";
import { Product } from "../../src/lib/definitions/product";
import { FILE_TYPES } from "../../src/lib/definitions/types";
import { setSequenceFromFileName_pages } from "../../src/lib/methods/sequence";
import input from "./resources/input.json";
import config from "../../src/resources/config.json";

config.regex.removeFromFileName = "\\d{4,}$";

describe("getSequenceFromName => ", () => {
  test("case 1 - Page before number but separareted from name", () => {
    const _case = "case1";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 2 - Page after number but separareted from name", () => {
    const _case = "case2";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 3 - number at the beginning", () => {
    const _case = "case3";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 4 - number at the end", () => {
    const _case = "case4";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 5 - number at the beginning, should not determine sequence due to unequal postfix", () => {
    const _case = "case5";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBeGreaterThan(0);
  });

  test("case 6 - number at the end, should not determine sequence due to unequal prefix", () => {
    const _case = "case6";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBeGreaterThan(0);
  });

  test("case 7 - Page before number not separated from name", () => {
    const _case = "case7";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 8 - Page before after not separated from name", () => {
    const _case = "case8";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 9 - p before after but separated from name", () => {
    const _case = "case9";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 10 - p before before but separated from name", () => {
    const _case = "case10";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });

  test("case 11 - p before before but separated from name", () => {
    const _case = "case11";
    const files = input[_case].fileNames.map((name) => {
      return {
        name: name,
        type: input[_case].type,
      };
    }) as IFile[];

    const params = {
      ignoreSequenceForFlat: false,
      productType: "Booklet",
      parts: [
        {
          type: "body",
          pages: files.length,
          width: 210,
          height: 297,
        },
      ],
    } as IParameter;

    const product = new Product(files, params, config);
    setSequenceFromFileName_pages(product, FILE_TYPES.body, [input[_case].regex]);

    const fileWithSequences = product.files
      .filter((file) => typeof file.sequence !== "undefined")
      .map((file) => {
        return {
          name: file.name,
          sequence: file.sequence,
        };
      });
    const undefinedSequences = product.files.filter((file) => typeof file.sequence === "undefined");

    expect(fileWithSequences).toMatchSnapshot();
    expect(undefinedSequences.length).toBe(0);
  });
});
