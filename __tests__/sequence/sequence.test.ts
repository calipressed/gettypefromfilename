import { setSequenceFromPairs } from "../../src/lib/methods/sequence";
import input from "./resources/input_sequence.json";
import config from "../../src/resources/config.json";
import { File } from "../../src/lib/definitions/file";
import { FILE_TYPES } from "../../src/lib/definitions/types";
import { Product } from "../../src/lib/definitions/product";
import { IParameter } from "../../src/lib/definitions/interfaces";

config.regex.removeFromFileName = "\\d{4,}$";

describe("setSequenceForFourCoverFiles", () => {
  test("4_cover_files ", () => {
    const files: File[] = [];

    for (const file of input["4_cover_files"].files) {
      const _file = new File(file, config);
      _file.type = FILE_TYPES.cover;
      files.push(_file);
    }

    const product = new Product(files, input["4_cover_files"].parameter as IParameter, config);

    setSequenceFromPairs(product, config);
    expect(product.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(product.files).toMatchSnapshot();
  });

  test("4_cover_files_front_outside_missing", () => {
    const files: File[] = [];

    for (const file of input["4_cover_files_front_outside_missing"].files) {
      const _file = new File(file, config);
      _file.type = FILE_TYPES.cover;
      files.push(_file);
    }

    const product = new Product(files, input["4_cover_files_front_outside_missing"].parameter as IParameter, config);

    setSequenceFromPairs(product, config);
    expect(product.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(product.files).toMatchSnapshot();
  });

  test("4_cover_files_front_inside_missing", () => {
    const files: File[] = [];

    for (const file of input["4_cover_files_front_inside_missing"].files) {
      const _file = new File(file, config);
      _file.type = FILE_TYPES.cover;
      files.push(_file);
    }

    const product = new Product(files, input["4_cover_files_front_inside_missing"].parameter as IParameter, config);

    setSequenceFromPairs(product, config);
    expect(product.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(product.files).toMatchSnapshot();
  });

  test("4_cover_files_FC", () => {
    const files: File[] = [];

    for (const file of input["4_cover_files_FC"].files) {
      const _file = new File(file, config);
      _file.type = FILE_TYPES.cover;
      files.push(_file);
    }

    const product = new Product(files, input["4_cover_files_FC"].parameter as IParameter, config);

    setSequenceFromPairs(product, config);
    expect(product.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(product.files).toMatchSnapshot();
  });
});
