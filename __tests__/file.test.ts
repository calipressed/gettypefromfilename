import { File } from "../src/lib/definitions/file";

describe("File", () => {
  test("file naming with sequence", () => {
    const result = new File({
      name: "Test.pdf",
    });

    result.sequence = 11;
    expect(result.getNameHonoringSequence()).toMatchInlineSnapshot(`"0011_Test.pdf"`);
  });

  test("File set width", () => {
    const file = new File({
      name: "Test.pdf",
      numPages: 2,
      pages: [
        {
          height: 210,
          width: 297,
          page: 1,
        },
        {
          height: 210,
          width: 297,
          page: 2,
        },
      ],
    });

    file.setWidth(420, [2]);
    expect(file).toMatchInlineSnapshot(`
File {
  "extension": ".pdf",
  "filePath": undefined,
  "id": "",
  "loggingName": "Test.pdf",
  "maskedOffMatches": [],
  "name": "test",
  "numPages": 2,
  "oriName": "Test.pdf",
  "pages": [
    {
      "height": 210,
      "page": 1,
      "width": 297,
    },
    {
      "height": 210,
      "page": 2,
      "width": 420,
    },
  ],
  "sequence": undefined,
  "subSequence": null,
  "subType": "none",
  "type": undefined,
  "uniqueId": undefined,
}
`);
  });

  test("File get height", () => {
    const file = new File({
      name: "Test.pdf",
      numPages: 2,
      pages: [
        {
          height: 210,
          width: 297,
          page: 1,
        },
        {
          height: 220,
          width: 297,
          page: 2,
        },
      ],
    });

    expect(file.getHeight(1)).toMatchInlineSnapshot(`210`);
  });

  test("Check if size is equal", () => {
    const file = new File({
      name: "Test.pdf",
      numPages: 2,
      pages: [
        {
          height: 210,
          width: 297,
          page: 1,
        },
        {
          height: 210,
          width: 297,
          page: 2,
        },
      ],
    });

    expect(file.isPageSizeEqual()).toBeTruthy();
  });

  test("Check if size is equal - negative", () => {
    const file = new File({
      name: "Test.pdf",
      numPages: 2,
      pages: [
        {
          height: 210,
          width: 297,
          page: 1,
        },
        {
          height: 220,
          width: 297,
          page: 2,
        },
      ],
    });

    expect(file.isPageSizeEqual()).toBeFalsy();
  });
});
