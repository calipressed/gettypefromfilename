import inputProd from "./resources/input_uploadportal.json";
import configProd from "../src/resources/config.json";
import { main } from "../src";
import { IParameter } from "../src/lib/definitions/interfaces";

// @ts-ignore
configProd.mode = "uploadPortal";
configProd.ignoreNumbersAbove = -1;

// @ts-ignore
configProd.regex.subSequence = "[0-9]{4}$";
configProd.regex.removeFromFileNameForLogging = "^[a-zA-z0-9]+-";
configProd.regex.removeFromFileName = "(^[a-zA-z0-9]+-)";
configProd.regex.productionFiles = "(.*.pdf)|(.*.png)|(.*.jpg)|(.*.jpeg)|(.*.tiff)|(.*.tif)|(.*.gif)|(.*.psd)|(.*.psb)|(.*.ai)";
configProd.regex.sidecarFiles = "(.*.xml)|(sidecar)";
configProd.regex.ignorePageCountForFiles = "(.*.psd)";
configProd.regex.spineFiles = ["spine"];
configProd.regex.additionalParts = [];

// @ts-ignore
configProd.upl = {};
// @ts-ignore
configProd.upl.uniqueIdOfSplitFiles = "(^[a-zA-z0-9]+-)";
// @ts-ignore
configProd.upl.removeFromSplitFiles = "_[0-9]{4}$";
// @ts-ignore
configProd.upl.subSequence = "[0-9]{4}$";

describe("FlatWork => ", () => {
  test("2_files", () => {
    const result = main(inputProd["flatWork_2_files"].files, inputProd["flatWork_2_files"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(2);
    expect(result).toMatchSnapshot();
  });
});

describe("Booklet => ", () => {
  test("6_files", () => {
    const result = main(inputProd["booklet_6_files"].files, inputProd["booklet_6_files"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(6);
    expect(result).toMatchSnapshot();
  });

  test("6_files_sidecar", () => {
    const result = main(inputProd["booklet_6_files_sidecar"].files, inputProd["booklet_6_files_sidecar"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(6);
    expect(result).toMatchSnapshot();
  });

  test("8_files_sidecar", () => {
    const result = main(inputProd["booklet_8_files_sidecar"].files, inputProd["booklet_8_files_sidecar"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(8);
    expect(result).toMatchSnapshot();
  });

  test("8_files_multiple_sidecar", () => {
    const result = main(inputProd["booklet_8_files_multiple_sidecar"].files, inputProd["booklet_8_files_multiple_sidecar"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(8);
    expect(result).toMatchSnapshot();
  });

  test("8_files_sidecar_negativ", () => {
    const result = main(inputProd["booklet_8_files_sidecar_negativ"].files, inputProd["booklet_8_files_sidecar_negativ"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("10_files", () => {
    const result = main(inputProd["booklet_10_files"].files as any, inputProd["booklet_10_files"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(10);
    expect(result).toMatchSnapshot();
  });

  test("booklet_10_files_2_with_uniqueID", () => {
    const result = main(inputProd["booklet_10_files_2_with_uniqueID"].files as any, inputProd["booklet_10_files_2_with_uniqueID"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(10);
    expect(result).toMatchSnapshot();
  });
});

describe("Book => ", () => {
  test("6_files", () => {
    const result = main(inputProd["book_6_files"].files, inputProd["book_6_files"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence !== "undefined").length).toBe(6);
    expect(result).toMatchSnapshot();
  });
});
