import { main } from "../src";
import inputProd from "./resources/input_unitedPrint.json";
import configProd from "../src/resources/config.json";
import { IParameter } from "../src/lib/definitions/interfaces";

// @ts-ignore
// configProd.mode = "common";
// configProd.ignoreNumbersAbove = -1;

// @ts-ignore
// configProd.regex.subSequence = "[0-9]{4}$";
configProd.regex.removeFromFileName = "";
configProd.regex.productionFiles = "(.*.pdf)|(.*.png)|(.*.jpg)|(.*.jpeg)|(.*.tiff)|(.*.tif)|(.*.gif)|(.*.psd)|(.*.psb)|(.*.ai)";
configProd.regex.sidecarFiles = "jobticket";
configProd.regex.ignorePageCountForFiles = "(.*.psd)";
configProd.regex.spineFiles = [];
configProd.regex.additionalParts = [];

describe("Multipart => ", () => {
  test("2_files_1_cover_1_body", () => {
    const result = main(inputProd["2_files_1_cover_1_body"].files, inputProd["2_files_1_cover_1_body"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("3_files", () => {
    const result = main(inputProd["3_files"].files, inputProd["3_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("24_pages_3_cover_files", () => {
    const result = main(inputProd["24_pages_3_cover_files"].files, inputProd["24_pages_3_cover_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("5_files_1_file_body_4_files_cover", () => {
    const result = main(inputProd["5_files_1_file_body_4_files_cover"].files, inputProd["5_files_1_file_body_4_files_cover"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("6_files_2_file_body_4_files_cover", () => {
    const result = main(inputProd["6_files_2_file_body_4_files_cover"].files, inputProd["6_files_2_file_body_4_files_cover"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("6_files_2_file_body_4_files_cover_should_fail", () => {
    const result = main(
      inputProd["6_files_2_file_body_4_files_cover_should_fail"].files,
      inputProd["6_files_2_file_body_4_files_cover_should_fail"].parameter as IParameter,
      configProd,
    );
    expect(result).toMatchSnapshot();
  });

  test("2_files_cover_by_size", () => {
    const result = main(inputProd["2_files_cover_by_size"].files, inputProd["2_files_cover_by_size"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("2_files_cover_body_by_pages_cover_by_exclusion", () => {
    const result = main(
      inputProd["2_files_cover_body_by_pages_cover_by_exclusion"].files,
      inputProd["2_files_cover_body_by_pages_cover_by_exclusion"].parameter as IParameter,
      configProd,
    );
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });

  test("3_files_2_cover_1_body", () => {
    const result = main(inputProd["3_files_2_cover_1_body"].files, inputProd["3_files_2_cover_1_body"].parameter as IParameter, configProd);
    expect(result.files.filter((file) => typeof file.sequence === "undefined").length).toBe(0);
    expect(result).toMatchSnapshot();
  });
});

describe("Multipage => ", () => {
  test("8_files", () => {
    const result = main(inputProd["8_files"].files, inputProd["8_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("13_files", () => {
    const result = main(inputProd["13_files"].files, inputProd["13_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("8_files_booklet_with_cover_files", () => {
    const result = main(inputProd["8_files_booklet_with_cover_files"].files, inputProd["8_files_booklet_with_cover_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("12_files_with_cover_files", () => {
    const result = main(inputProd["12_files_with_cover_files"].files, inputProd["12_files_with_cover_files"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });

  test("14_files_with_cover_front_back", () => {
    const result = main(inputProd["14_files_with_cover_front_back"].files, inputProd["14_files_with_cover_front_back"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });
});

describe("Flat => ", () => {
  test("2_files_flat", () => {
    const result = main(inputProd["2_files_flat"].files, inputProd["2_files_flat"].parameter as IParameter, configProd);
    expect(result).toMatchSnapshot();
  });
});
