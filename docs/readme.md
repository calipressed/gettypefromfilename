
# Description

The script sets the order and type of files by their name or size.

__On Success:__

- Files have a defined type (body, cover or wildcard)
- Files have a defined sequence (1, 2, 3, etc.)

__On Warning:__

- If the seuqence was not clearly specified
- If the type was not explicitly specified (e.g. cover files found and assumed that others are of type body)
- If expected pages are unequal actual pages and only one input file

__On Error:__

- No production files were found
- Sequence cannot be determined
- If the type of files cannot be determined
- More files than expected
- If type of cover and body are expected, but only cover files are found

# Examples

## FlatWork

