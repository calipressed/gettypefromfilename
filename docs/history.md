
### 2.4.6 (setOrder 0.6.5)

### feat

- adapt functions to set type by the number of pages (also if some files are already assigned as body)

### chore

- adapt function to determine type by the size of the PDF

### 2.4.5

### fix

- first and last files not be assigned to cover even if all files have a sequence
- adapt interfaces

### chore

- update fallback config and tests

### 2.4.4 (setOrder 0.6.4)

### fix

- sequence is not set for multi part product where body is set by name and cover by the procedure of exclusion

### 2.4.3

### fix

- upload portal
  - adapt configuration
  - store unique id of split files
  - ignore letter case for part types

### 2.4.2 (setOrder 0.6.3)

### fix

- error 'cannot read subType of undefined'
- add regex 'march' to config json

### 2.4.1 (setOrder 0.6.1)

#### feat

- add logging name to mask off strings from the original file name if needed
  - notice that if __removeFromFileNameForLogging__ is not defined the whole original name will be displayed in log messages

#### change

- rename package to __gettypefromfilename__

### 2.4.0 (setOrder 0.6.0)

#### fix

- Do not set splitCoverForMergeWithBody on multi part product

#### change

- Handling of flat products with variants
  - run through pairs in strict mode
  - if no match found run through pairs again (assuming front/back by procedure of exclusion)

#### feat

- Support case where cover files are defined as follows
  - Example:
    ```JSON
    "files": [
        {"name":"Cover_01.tif"},
        {"name":"Cover_02.tif"},
        {"name":"xBack_Cover_01.tif"},
        {"name":"xBack_Cover_02.tif"}
      ]
      ```

- Support calendar with 13 pages, where one file has no sequence defined (single and multi part)
  - Example:
  ```JSON
  "files": [
      { "name": "1.tif"},
      { "name": "10.tif"},
      { "name": "11.tif"},
      { "name": "12.tif"},
      { "name": "2.tif"},
      { "name": "3.tif"},
      { "name": "4.tif"},
      { "name": "5.tif"},
      { "name": "6.tif"},
      { "name": "7.tif"},
      { "name": "8.tif"},
      { "name": "9.tif"},
      { "name": "ok__adka.tif"}
    ]
    ```

- Support calendar with 14 pages, where one file has no sequence defined (only works if files have month in their name and for single part products)
  - Example:
  ```JSON
  "files": [
    { "name": "Calendar_00_front_2023.pdf", "numPages": 1 },
    { "name": "Calendar_01_Jan_2023.pdf", "numPages": 1 },
    { "name": "Calendar_02_Feb_2023.pdf", "numPages": 1 },
    { "name": "Calendar_03_Mar_2023.pdf", "numPages": 1 },
    { "name": "Calendar_04_Apr_2023.pdf", "numPages": 1 },
    { "name": "Calendar_05_May_2023.pdf", "numPages": 1 },
    { "name": "Calendar_06_Jun_2023.pdf", "numPages": 1 },
    { "name": "Calendar_07_Jul_2023.pdf", "numPages": 1 },
    { "name": "Calendar_08_Aug_2023.pdf", "numPages": 1 },
    { "name": "Calendar_09_Sep_2023.pdf", "numPages": 1 },
    { "name": "Calendar_10_Oct_2023.pdf", "numPages": 1 },
    { "name": "Calendar_11_Nov_2023.pdf", "numPages": 1 },
    { "name": "Calendar_12_Dez_2023.pdf", "numPages": 1 },
    { "name": "last_page_2023.pdf", "numPages": 1}
  ]
  ```

- Support files without detecting any type at the beginning (assumed that first and last pages are cover => other must be body)
  - Example:
  ```JSON
  "files": [
      { "name": "print24_test_seite3_14_NN02.pdf", "numPages": 12},
      { "name": "print24_test_seite15_26_NN02.pdf", "numPages": 12},
      { "name": "print24_test_seite_1_NN02.jpg", "numPages": 1},
      { "name": "print24_test_seite_27_NN02.pdf", "numPages": 1},
      { "name": "print24_test_seite_28_NN02.jpg", "numPages": 1},
      { "name": "print24_test_seite_2_NN02.pdf", "numPages": 1}
    ]
  ```

### 2.3.7 (setOrder 0.5.8)

#### fix

- exclude spine if sequence for cover is determined by front/back or front/back/inside/outside

### 2.3.6 (setOrder 0.5.7)

#### fix

- Honor pairs for front/back also for 4 cover files (with inside/outside)
- Log warning if inside/outside is assumed by procedure of excluding

### 2.3.4 / 2.3.5 (setOrder 0.5.6)

#### fix

- Always log warning if file needs to be processed afterwards
  - e.g. if any of `splitCoverForMergeWithBody`, `splitCoverFromSingleFile`, `spineIsLastPage` or `duplicatePageWithVariants` is __true__

### 2.3.3 (setOrder 0.5.5)

#### Features

- If sequence defined => set first/last files to type cover (also if 3 cover files are defined)

#### Changes

- For front/back and inside/outside
  - go through all pairs in the first run and if no match found, then go for a second run and determine front/back by procedure of excluding

#### Chores

- Adaption of functions to determine front/back/inside/outside files

### 2.3.1

#### fix

- A message was logged that not all product parts were clearly defined, although this was not the case.
- Calendars:
  - logged warning for all files even if only one did not match front / back
  - not honored pairs
- Multipart:
  - if sequence is defined and first/last pages are set to cover => only step into function if actual cover pages are not equal the expected ones

### 2.3.0

#### Breaking changes

- result of main function changed to:

```JSON
{
  "results": {
    "severity": 2,
    "splitCoverForMergeWithBody": false,
    "splitCoverFromSingleFile": false,
    "spineIsLastPage": false,
    "hasSidecarFiles": false,
    "duplicatePageWithVariants": null,
    "lastErrorMessage": "Assumed that 11_NN01.pdf is of type cover.",
    "lastErrorCode": -124,
    "lastErrorId": "setOrderWarning"
  },
  "parameter": {
    "needsAdditionalPart": false,
    "neededCoverPages": 4,
    "neededBodyPages": 8,
    "neededAdditionalPartPages": 0,
    "allNeededPages": 12,
    "additionalPartType": "",
    "parts": [
      {
        "type": "body",
        "pages": 8,
        "width": 210,
        "height": 297
      },
      {
        "type": "cover",
        "pages": 4,
        "width": 210,
        "height": 297
      }
    ],
    "numVariants": 1,
    "ignoreNumbersAbove": -1,
    "ignoreSequenceForFlat": false,
    "mode": "common"
  },
  "messages": [],
  "files": [],
  "singleFiles": [],
  "sidecarFiles": [],
  "type": "book"
}
```

- config JSON changed
  - `regex.sequence."1"` and `regex.sequence."2"` removed => use front/back expressions defined in `regex.sequence.pairs`

#### Features

- Normalize sequence of files and always start with 2 (e.g. if sequence is 3, 6, 9 => normalized sequence will be 2, 3, 4)
- Add additional function to determine file type by the actual pages of the file
- Support of mode = 'uploadPortal'
  - each pdf page comes in as single file with a certain sub sequence
- Use pairs to determine front/back and inside/outside (flat, single part and multi part)

- Flat:
  - Determine sequence where amount of files = number of variants + 1
- Single part booklet => support files delivered with cover
  - support front and back files for booklets
  - 1 file & pages > 1 => set type to cover and splitCoverForMergeWithBody=true
  - 1 or 2 files or 4 files => set to first and last sequence
- Multipart:
  - if sequence is already defined and actual body pages === expected body + cover pages => assign first/last files to cover

#### Changes

- Change handling to identify spine files
- Ignore capital letter when determining production/sidecar files
- Adapt handling of product type
  - assume type from expected parts
  - if type === booklet and an expected product part cover exists => handle as multi part product
- Adapted configuration file
- Adapt logging
  - set lastErrorMessage for info, warning and error
  - only set single line text to lastErrorMessage

### 1.4.0

- Search for months to determine sequence of files (e.g. calendars)

### 1.3.0

- Renewal of function for determining sequence from file name
- Define numbers above <value> to be ignored for sequence (e.g. all numbers above 999) in config.json

### 1.2.0

- Support to determine spine for multi part products
- Define which part of the file name should be ignored by regex in config.json

### 1.1.8

- Additional logging messages

### 1.0.5

- Change handling of regular expressions to determine sequence of files, add configuration if prefix or postifx must be equal for the expression
